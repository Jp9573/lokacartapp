package com.mobile.ict.cart.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.Master;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class OrderViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private final ImageView mArrowExpandImageView;
    private final TextView orderId;
    private final TextView timeStamp;
    private final TextView total;
    private final TextView paymentStatus,deliveryTimeStamp;

    public OrderViewHolder(View itemView) {
        super(itemView);
        orderId = (TextView) itemView.findViewById(R.id.tvOrderId);
        timeStamp = (TextView) itemView.findViewById(R.id.tvDate);
        total = (TextView) itemView.findViewById(R.id.tvTotal);
        mArrowExpandImageView = (ImageView) itemView.findViewById(R.id.arrow_expand_imageview);
        paymentStatus = (TextView) itemView.findViewById(R.id.tvPaymentStatus);
        deliveryTimeStamp = (TextView) itemView.findViewById(R.id.tvDeliveryDate);

    }

    @SuppressLint("SimpleDateFormat")
    public void bind(Orders orders, String type) {

        orderId.setText("ID: " + orders.getOrder_id());
        Date time = null,deliveryTime=null;
        String deliveryDate;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(orders.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        @SuppressLint("SimpleDateFormat") String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);
        timeStamp.setText("Order Date: "+date);

        DecimalFormat df = new DecimalFormat("0.00");

        int tot = (int)Math.round(orders.getTotalBill());

        tot=tot+orders.getDeliveryCharges();

        //total.setText("Total(with GST): " + "\u20B9" + String.format("%.2f", orders.getTotalBill()));

        total.setText("Total(with GST): " + "\u20B9" + df.format(tot));


        if (type.equals(Master.DELIVEREDORDER)) {
            paymentStatus.setVisibility(View.VISIBLE);
            if (orders.getPaymentStatus().equals("true")) {
                paymentStatus.setText("Paid");
                paymentStatus.setTextColor(Color.GREEN);

            } else {
                paymentStatus.setText("Unpaid");
                paymentStatus.setTextColor(Color.RED);

            }

        } else {
            paymentStatus.setVisibility(View.INVISIBLE);

        }

         if(type.equals(Master.DELIVEREDORDER)||type.equals(Master.PROCESSEDORDER))
        {
            deliveryTimeStamp.setVisibility(View.VISIBLE);

            if(!orders.getDeliveryTimeStamp().equals("Not Applicable"))
            {
                try {
                    deliveryTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(orders.getDeliveryTimeStamp());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                 deliveryDate = new SimpleDateFormat("EEE, MMM d, yyyy").format(deliveryTime);
            }
            else
            {
                deliveryDate=orders.getDeliveryTimeStamp();
            }

            deliveryTimeStamp.setText("Delivery Date: " + deliveryDate);

        }
        else
        {
            deliveryTimeStamp.setVisibility(View.INVISIBLE);

        }

    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (expanded) {
            mArrowExpandImageView.setRotation(ROTATED_POSITION);
        } else {
            mArrowExpandImageView.setRotation(INITIAL_POSITION);
        }
        //Unnecessary check!
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                mArrowExpandImageView.setRotation(ROTATED_POSITION);
            } else {
                mArrowExpandImageView.setRotation(INITIAL_POSITION);
            }
        }*/
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        RotateAnimation rotateAnimation;
        if (expanded) {
            rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        } else {
            rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        }

        rotateAnimation.setDuration(200);
        rotateAnimation.setFillAfter(true);
        mArrowExpandImageView.startAnimation(rotateAnimation);
        //Unnecessary check!
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) {
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else {
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            mArrowExpandImageView.startAnimation(rotateAnimation);
        }*/
    }
}
