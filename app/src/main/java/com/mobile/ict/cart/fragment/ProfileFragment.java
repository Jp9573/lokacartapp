package com.mobile.ict.cart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.mobile.ict.cart.BuildConfig;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Random;


public class ProfileFragment extends Fragment implements View.OnClickListener, GetResponse {


    private View profileFragmentView;
    private View editMobileNumberDialogView;
    private TextInputEditText eFName;
    private TextInputEditText eLName;
    private TextInputEditText eAddress;
    private TextInputEditText ePincode;
    private TextInputEditText eMobileNumber;
    private TextInputEditText eOtp;
    private TextInputEditText eEmail;
    private CardView cardView2;
    private ImageView iVProfile;
    private TextView tMobileNumber;
    private TextView tName;
    private TextView tEmail;
    private TextView tAddress;
    private TextView tPincode;
    private TextView tverify;
    private String localFName;
    private String localLName;
    private String localAddress;
    private String localPincode;
    private String localMobileNumber;
    private String localEmail;
    private AlertDialog changeMobileNumberAlert;
    private OTPCountDownTimer countDownTimer;
    private String otp_check;
    private long session;
    private File file;
    private String tempImage;
    private final int REQUEST_GALLERY = 2;
    private final int REQUEST_CAMERA = 1;
    private DBHelper dbHelper;
    private Button bPositive;
    private Button bNeutral;
    private String optionSelected = "";
    private boolean isVisible = false;
    private String abspath;
    private Context cont;
    private int category;
    private String oldNum;
    private String newNum;
    String selectedImagePath;
    Bitmap bitmap;
    String filePath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle(R.string.title_fragment_profile);

        setHasOptionsMenu(true);

        if (!Master.isNetworkAvailable(getActivity())) {
            profileFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        } else {
            dbHelper = DBHelper.getInstance(getActivity());
            dbHelper.getSignedInProfile();


            profileFragmentView = inflater.inflate(R.layout.fragment_profile, container, false);

            init();
            assign();
        }


        return profileFragmentView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (dbHelper == null) dbHelper = DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void init() {
        dbHelper = DBHelper.getInstance(getActivity());
        cardView2 = (CardView) profileFragmentView.findViewById(R.id.card_view2);
        cardView2.setVisibility(View.GONE);
        eEmail = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileEmail);
        tverify = (TextView) profileFragmentView.findViewById(R.id.tverify);

        eFName = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileFName);
        eLName = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileLName);
        eMobileNumber = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileMobileNumber);
        ePincode = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfilePincode);
        eAddress = (TextInputEditText) profileFragmentView.findViewById(R.id.eEditProfileAddress);


        iVProfile = (ImageView) profileFragmentView.findViewById(R.id.iProdfilePhoto);
        iVProfile.setOnClickListener(this);


        tName = (TextView) profileFragmentView.findViewById(R.id.tProfileName);
        tEmail = (TextView) profileFragmentView.findViewById(R.id.tProfileEmail);
        tAddress = (TextView) profileFragmentView.findViewById(R.id.tProfileAddress);
        tPincode = (TextView) profileFragmentView.findViewById(R.id.tProfilePincode);
        tMobileNumber = (TextView) profileFragmentView.findViewById(R.id.tProfileMobileNumber);

        ImageView ivEdit = (ImageView) profileFragmentView.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(this);

        ImageView ivDone = (ImageView) profileFragmentView.findViewById(R.id.ivDone);
        ivDone.setOnClickListener(this);

        tverify.setOnClickListener(this);

        if (!SharedPreferenceConnector.readString(getActivity(), Master.emailVerified, "1").equals("0") || MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com")) {
            tverify.setVisibility(View.GONE);
        }

        ImageView ivMobileNumberDone = (ImageView) profileFragmentView.findViewById(R.id.ivMobileNumberDone);
        ivMobileNumberDone.setOnClickListener(this);
    }

    private void assign() {
        isVisible = false;

        tName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());

        if (!MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com"))
            tEmail.setText(MemberDetails.getEmail());
        else
            tEmail.setText("");

        tMobileNumber.setText(MemberDetails.getMobileNumber());
        tAddress.setText(MemberDetails.getAddress());
        tPincode.setText(MemberDetails.getPincode());

        eFName.setText(MemberDetails.getFname());
        eLName.setText(MemberDetails.getLname());
        eMobileNumber.setText(MemberDetails.getMobileNumber());
        eAddress.setText(MemberDetails.getAddress());
        ePincode.setText(MemberDetails.getPincode());

        if (!MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com")) {
            eEmail.setText(MemberDetails.getEmail());
            localEmail = MemberDetails.getEmail();

        } else {
            eEmail.setText("");
            localEmail = "";

        }

        localFName = MemberDetails.getFname();
        localLName = MemberDetails.getLname();
        localAddress = MemberDetails.getAddress();
        localPincode = MemberDetails.getPincode();
        localMobileNumber = MemberDetails.getMobileNumber();


        Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .centerCrop()
                .placeholder(R.drawable.profile_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iVProfile);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onClick(View v) {

        Master.hideSoftKeyboard(getActivity());
        switch (v.getId()) {
            case R.id.ivEdit:

                if (isVisible) {
                    isVisible = false;
                    cardView2.setVisibility(View.GONE);
                } else {
                    isVisible = true;
                    cardView2.setVisibility(View.VISIBLE);
                    eMobileNumber.setText(MemberDetails.getMobileNumber());
                }
                break;

            case R.id.ivDone:
                save();
                break;
            case R.id.tverify: {
                new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                        getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "Email_Internet").execute();


                break;
            }
            case R.id.iProdfilePhoto: {

                checkImageUploadPermission();

                break;
            }
            case R.id.ivMobileNumberDone:
                if (eMobileNumber.getText().toString().trim().length() != 10)
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
                else if (eMobileNumber.getText().toString().trim().charAt(0) == '0')
                    Toast.makeText(getActivity(), R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
                else if (localMobileNumber.equals(eMobileNumber.getText().toString().trim()))
                    Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
                else {

                    category = 2;


                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "MobileNumberVerify_Internet").execute();


                }
                break;
        }
    }


    private String getResizedBitmap(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();

        String filepath = "";
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = 300;
            height = (int) (width / bitmapRatio);
        } else {
            height = 300;
            width = (int) (height * bitmapRatio);
        }
        FileOutputStream out = null;
        try {

            String path = Environment.getExternalStorageDirectory().toString();

            long time = System.currentTimeMillis();
            File file = new File(path, MemberDetails.getMobileNumber() + "-" + time + ".png");
            filepath = file.getAbsolutePath();
            if (file.exists()) {
                file.delete();
            }
            out = new FileOutputStream(file);
            Bitmap bitmp = Bitmap.createScaledBitmap(image, width, height, true);
            bitmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return filepath;
    }


    public void checkImageUploadPermission() {

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) + ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {


            SharedPreferenceConnector.writeString(getActivity(), "tag", Master.PROFILE_TAG);

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);


        } else {

            uploadImage();

        }
    }


    private void uploadImage() {

        final CharSequence[] options =
                {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options.equals("Take Photo")) {
                    optionSelected = "camera";
                } else {
                    optionSelected = "gallery";
                }

                DialogClick(dialog, options[item]);


            }
        });
        builder.show();
    }

    private void DialogClick(DialogInterface dialog, CharSequence option) {
        if (option.equals("Take Photo")) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Random randomno = new Random();
            tempImage = "temp" + randomno.nextInt(1000000) + ".jpg";
            File f = new File(android.os.Environment.getExternalStorageDirectory(), tempImage);
            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(intent, REQUEST_CAMERA);
        } else if (option.equals("Choose from Gallery")) {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/jpg");
            startActivityForResult(intent, REQUEST_GALLERY);
        } else {
            dialog.dismiss();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        int countPermission;

        if (requestCode == 1) {

            countPermission = 0;

            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];


                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission);
                    if (!showRationale) {

                        Material.alertDialog(
                                getActivity(),
                                "Permission Denied",
                                "You denied permissions and checked \\\"Don\\'t ask again\\\".You won\\'t access photos, media and files on your device.You can enable them by app settings.",
                                "Ok");


                    } else if (Manifest.permission.CAMERA.equals(permission)) {

                    } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission)) {

                    }
                } else {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {


                        countPermission++;

                        if (countPermission == permissions.length) {
                            uploadImage();
                        }
                    }
                }
            }

        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cont = getContext();
        super.onActivityResult(requestCode, resultCode, data);

        //noinspection AccessStaticViaInstance
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(tempImage)) {
                        f = temp;
                        break;
                    }
                }


                Uri selectedImageURI = Uri.fromFile(f);

                if (selectedImageURI != null) {
                    try {
                        selectedImagePath = f.getAbsolutePath();


                        new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                                getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "UploadImage_Internet").execute();


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.label_toast_unable_to_get_the_file_path), Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.label_toast_unable_to_get_the_file_path), Toast.LENGTH_LONG).show();

                }

            } else if (requestCode == REQUEST_GALLERY) {

                Uri selectedImageURI = data.getData();
                if (selectedImageURI != null) {
                    try {
                        selectedImagePath = getAbsolutePath(getActivity(), selectedImageURI);

                        new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                                getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "UploadImage_Internet").execute();


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.label_toast_unable_to_get_the_file_path), Toast.LENGTH_LONG).show();

                    }
                }
            }
        } else if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
    }


    private Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    private Bitmap rotateImage(final String path) {
        final Bitmap[] b = new Bitmap[1];
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                b[0] = decodeFileFromPath(path);
                try {
                    ExifInterface ei = new ExifInterface(path);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            System.out.println("Case 90   " + ExifInterface.ORIENTATION_ROTATE_90);
                            matrix.postRotate(90);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            System.out.println("Case 90   " + ExifInterface.ORIENTATION_ROTATE_180);
                            matrix.postRotate(180);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            System.out.println("Case 90   " + ExifInterface.ORIENTATION_ROTATE_270);
                            matrix.postRotate(270);
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                        default:
                            b[0] = Bitmap.createBitmap(b[0], 0, 0, b[0].getWidth(), b[0].getHeight(), matrix, true);
                            break;
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }

                FileOutputStream out1 = null;
                File file;
                try {
                    String state = Environment.getExternalStorageState();
                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".jpg");
                    } else {
                        file = new File(cont.getFilesDir(), "image" + new Date().getTime() + ".jpg");
                    }
                    out1 = new FileOutputStream(file);
                    b[0].compress(Bitmap.CompressFormat.JPEG, 90, out1);
                    abspath = file.getAbsolutePath();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        out1.close();
                    } catch (Throwable ignore) {

                    }
                }

            }
        });
        return b[0];
    }

    private Bitmap decodeFileFromPath(String path) {
        Uri uri = getImageUri(path);
        InputStream in = null;
        try {
            try {
                in = cont.getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(in, null, o);
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


            int scale = 1;
            int inSampleSize = 1024;
            if (o.outHeight > inSampleSize || o.outWidth > inSampleSize) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(inSampleSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = cont.getContentResolver().openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            return b;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getAbsolutePath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    private Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }

    @Override
    public void getData(String response, String tag) {
        if (ProfileFragment.this.isAdded()) {
            switch (tag) {

                case "Email_Internet":
                    if (response.equals("success")) {
                        new NetworkAsyncTask
                                (
                                        Master.getCheckVerificationURL(MemberDetails.getMobileNumber()),
                                        "sendVerification",
                                        Master.DIALOG_TRUE,
                                        getString(R.string.pd_sending_data_to_server),
                                        ProfileFragment.this,
                                        null,
                                        Master.GET,
                                        Master.AUTH_TRUE,
                                        MemberDetails.getEmail(),
                                        MemberDetails.getPassword()
                                ).execute();
                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }
                    break;

                case "MobileNumberVerify_Internet":
                    if (response.equals("success")) {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("phonenumber_old", "91" + MemberDetails.getMobileNumber());
                            obj.put("phonenumber_new", "91" + eMobileNumber.getText().toString().trim());

                            new NetworkAsyncTask
                                    (
                                            Master.getNumberVerifyURL(),
                                            "mobileNumberVerify",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_data_to_server),
                                            ProfileFragment.this,
                                            obj,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }
                    break;

                case "ProfileInfo_Internet":
                    if (response.equals("success")) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(Master.FNAME, eFName.getText().toString().trim());
                            jsonObject.put(Master.LNAME, eLName.getText().toString().trim());
                            jsonObject.put(Master.ADDRESS, eAddress.getText().toString().trim());
                            jsonObject.put(Master.PINCODE, ePincode.getText().toString().trim());
                            jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                            jsonObject.put("oldemail", MemberDetails.getEmail());

                            if (eEmail.getText().toString().trim().length() != 0) {
                                jsonObject.put(Master.EMAIL, eEmail.getText().toString().trim());

                            } else {
                                jsonObject.put(Master.EMAIL, "91" + MemberDetails.getMobileNumber() + "@gmail.com");

                            }


                            new NetworkAsyncTask
                                    (
                                            Master.getEditProfileURL(),
                                            "editProfile",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_data_to_server),
                                            ProfileFragment.this,
                                            jsonObject,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();
                        } catch (JSONException e) {
                        }

                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }
                    break;

                case "EditMobileNumber_Internet":
                    if (response.equals("success")) {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phonenumber", "91" + MemberDetails.getMobileNumber());
                            jsonObject.put("phonenumber_new", "91" + eMobileNumber.getText().toString().trim());

                            oldNum = MemberDetails.getMobileNumber();
                            newNum = eMobileNumber.getText().toString().trim();
                            new NetworkAsyncTask
                                    (
                                            Master.getChangeNumberURL(),
                                            "editMobileNumber",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_data_to_server),
                                            ProfileFragment.this,
                                            jsonObject,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();


                        } catch (JSONException e) {
                        }

                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }
                    break;

                case "UploadImage_Internet":
                    if (response.equals("success")) {
                        bitmap = rotateImage(selectedImagePath);
                        bitmap = getCroppedBitmap(bitmap);
                        filePath = getResizedBitmap(bitmap);

                        new UploadImageTask(filePath, bitmap).execute();
                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }
                    break;

                case "sendVerification":

                    if (response.equals("exception")) {
                        Material.alertDialog(getActivity(), getActivity().getString(R.string.toast_we_are_facing_some_technical_problems), "OK");
                    } else {
                        try {
                            JSONObject root = new JSONObject(response);
                            if (root.getString("status").equals("Email Verified Already")) {
                                SharedPreferenceConnector.writeString(getActivity(), Master.emailVerified, "1");
                                Toast.makeText(getActivity(), "Email Verified Already", Toast.LENGTH_LONG).show();
                            } else {
                                SharedPreferenceConnector.writeString(getActivity(), Master.emailVerified, "0");
                                Toast.makeText(getActivity(), "Verification email has been sent", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    break;

                case "mobileNumberVerify":

                {
                    if (response.equals("exception")) {
                        Material.alertDialog(getActivity(), getActivity().getString(R.string.toast_we_are_facing_some_technical_problems), "OK");
                    } else {
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            String otp_val = responseObject.getString("otp");
                            String text = responseObject.getString("text");
                            if (!otp_val.equals("null") && !text.equals("null")) {
                                Toast.makeText(getActivity(), R.string.toast_OTP_has_sent_to_your_mobile_sms, Toast.LENGTH_LONG).show();

                                otp_check = otp_val;
                                session = System.currentTimeMillis();
                                countDownTimer = new OTPCountDownTimer();
                                countDownTimer.start();

                                session = session + 600000;
                                if (category == 2)
                                    editMobileNumber();
                                else {
                                    bNeutral.setVisibility(View.GONE);
                                    bPositive.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (otp_val.equals("null") && text.equals("Phone number entered already exists.")) {
                                    Toast.makeText(getActivity(), R.string.toast_validation_Mobile_Number_exists, Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                        }
                    }
                }
                break;

                case "editProfile":

                {
                    if (response.equals("exception")) {
                        Material.alertDialog(getActivity(), getActivity().getString(R.string.toast_we_are_facing_some_technical_problems), "OK");
                    } else {

                        try {

                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.getString(Master.RESPONSE).equals("Success")) {
                                try {
                                    if (responseObject.getString("emailVerify").equals("0")) {
                                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.emailVerified, "0");
                                        tverify.setVisibility(View.VISIBLE);
                                    } else {
                                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.emailVerified, "1");
                                        tverify.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Toast.makeText(getActivity(), R.string.toast_profile_details_updated_successfully, Toast.LENGTH_LONG).show();

                                MemberDetails.setFname(eFName.getText().toString().trim());
                                MemberDetails.setLname(eLName.getText().toString().trim());
                                MemberDetails.setAddress(eAddress.getText().toString().trim());
                                MemberDetails.setPincode(ePincode.getText().toString().trim());

                                if (eEmail.getText().toString().trim().length() != 0)
                                    MemberDetails.setEmail(eEmail.getText().toString().trim());

                                else

                                    MemberDetails.setEmail("91" + MemberDetails.getMobileNumber() + "@gmail.com");

                                dbHelper.updateProfile(MemberDetails.getMobileNumber());

                                assign();

                                //DashboardActivity.updateNavHeader();
                                UpdateNavData updateNavData = (UpdateNavData) getContext();
                                updateNavData.updateNavHeader();

                            } else if (responseObject.getString(Master.RESPONSE).equals("The Mail ID is already associated with another account already")) {
                                Toast.makeText(getActivity(), "The Mail ID is already associated with another account", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                        }
                    }
                }

                break;

                case "editMobileNumber":

                {
                    if (response.equals("exception")) {
                        Material.alertDialog(getActivity(), getActivity().getString(R.string.toast_we_are_facing_some_technical_problems), "OK");
                    } else {

                        try {
                            JSONObject responseObject = new JSONObject(response);

                            if (responseObject.getString(Master.STATUS).equals("success")) {
                                Toast.makeText(getActivity(), R.string.toast_Your_mobile_number_upadted_successfully, Toast.LENGTH_LONG).show();
                                dbHelper.changeMobileNumber(oldNum, newNum);
                                tMobileNumber.setText(newNum);
                                localMobileNumber = newNum;
                                MemberDetails.setMobileNumber(newNum);

                                //DashboardActivity.updateNavHeader();
                                UpdateNavData updateNavData = (UpdateNavData) getContext();
                                updateNavData.updateNavHeader();

                            } else if (responseObject.getString(Master.STATUS).equals("user phone number not present")) {
                                Toast.makeText(getActivity(), R.string.toast_Mobile_Number_does_not_exists, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                        }
                    }
                }
                break;

                default:
                    break;
            }


        }


    }


    class UploadImageTask extends AsyncTask<Void, String, String> {
        ProgressDialog pd;
        final String path;

        final Bitmap bitmap;
        String imagePath;

        UploadImageTask(String path, Bitmap bitmap) {
            this.bitmap = bitmap;
            this.path = path;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.pd_uploading_image));
            pd.setCancelable(false);
            pd.show();


            try {
                file = new File(path);
            } catch (Exception e) {
                e.printStackTrace();
            }

            imagePath = file.getParent() + "/" + "myImage" + new Random().nextInt(1000000) + ".png";

            File file1 = new File(imagePath);
            try {
                Master.copyFile(file, file1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String response;
            response = Master.okhttpUpload(file, Master.getUploadImageURL(MemberDetails.getMobileNumber()),
                    "91" + MemberDetails.getMobileNumber(), MemberDetails.getPassword());

            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if (pd != null && pd.isShowing())
                pd.dismiss();

            try {
                JSONObject responseObject = new JSONObject(message);
                message = responseObject.optString("response");


                switch (message) {
                    case "Image upload successful":
                        Toast.makeText(getActivity(),
                                R.string.toast_image_has_been_uploaded_successfully, Toast.LENGTH_LONG).show();
                        Master.clearBitmap(bitmap);
                        if (responseObject.has("url")) {
                            String imageUrl = responseObject.getString("url");

                            MemberDetails.setProfileImageUrl(imageUrl);

                            Glide.with(getActivity()).load(MemberDetails.getProfileImageUrl()).asBitmap().placeholder(R.drawable.profile_placeholder).centerCrop().into(new BitmapImageViewTarget(iVProfile) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    iVProfile.setImageDrawable(circularBitmapDrawable);
                                }
                            });
                            dbHelper.addProfile();
                        }


                        break;
                    case "timeout":
                        Toast.makeText(getActivity(), "timeout", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

            }
        }
    }


    private void save() {
        if (eFName.getText().toString().trim().equals(""))
            eFName.setError(getString(R.string.error_required));
        else if (eLName.getText().toString().trim().equals(""))
            eLName.setError(getString(R.string.error_required));
        else if (eAddress.getText().toString().trim().equals(""))
            eAddress.setError(getString(R.string.error_required));
        else if (ePincode.getText().toString().trim().equals(""))
            ePincode.setError(getString(R.string.error_required));

        else if (ePincode.getText().toString().trim().length() != 6)
            Toast.makeText(getActivity(), R.string.toast_please_enter_a_valid_six_digit_pincode, Toast.LENGTH_LONG).show();
        else if (eFName.getText().toString().trim().equals(localFName) && eLName.getText().toString().trim().equals(localLName)
                && eAddress.getText().toString().trim().equals(localAddress) && ePincode.getText().toString().trim().equals(localPincode) && eEmail.getText().toString().equals(localEmail)) {
            Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
        } else if (eEmail.getText().toString().trim().length() != 0 && Validation.isValidEmail(eEmail.getText().toString().trim())) {
            Toast.makeText(getActivity(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();
        } else {

            new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                    getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "ProfileInfo_Internet").execute();


        }
    }

    private void editMobileNumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        editMobileNumberDialogView = getActivity().getLayoutInflater().inflate(R.layout.change_mobile_number_box, null);
        builder.setView(editMobileNumberDialogView);
        builder.setTitle(R.string.title_dialog_change_mobile_number);
        builder.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNeutralButton(R.string.button_get_OTP, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        eOtp = (TextInputEditText) editMobileNumberDialogView.findViewById(R.id.eOtp1);
        changeMobileNumberAlert = builder.create();
        changeMobileNumberAlert.show();

        bPositive = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_POSITIVE);
        Button bNegative = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_NEGATIVE);
        bNeutral = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_NEUTRAL);


        bNeutral.setVisibility(View.GONE);

        bPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long session2 = System.currentTimeMillis();

                if (session2 < session) {
                    if (eOtp.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getActivity(), "Please " + getString(R.string.toast_enter_otp_received), Toast.LENGTH_LONG).show();
                    } else if (eOtp.getText().toString().trim().equals(otp_check)) {
                        changeMobileNumberAlert.dismiss();
                        countDownTimer.cancel();

                        new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                                getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "EditMobileNumber_Internet").execute();


                    } else {
                        Toast.makeText(getActivity(), R.string.toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();
                    }
                } else {
                    eOtp.setText("");
                    Toast.makeText(getActivity(), R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }
            }
        });


        bNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMobileNumberAlert.dismiss();
            }
        });

        bNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category = 1;

                new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                        getString(R.string.pd_chk_internet_connection), ProfileFragment.this, "MobileNumberVerify_Internet").execute();


            }
        });

    }


    public class OTPCountDownTimer extends CountDownTimer {

        public OTPCountDownTimer() {
            super((long) 600000, (long) 1000);
        }

        @Override
        public void onFinish() {

            if (ProfileFragment.this.isAdded()) {
                if (editMobileNumberDialogView != null) {
                    eOtp.setText("");
                    bPositive.setVisibility(View.GONE);
                    bNeutral.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    public interface UpdateNavData {
        void updateNavHeader();
    }


}
