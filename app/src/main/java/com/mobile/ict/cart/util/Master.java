package com.mobile.ict.cart.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.view.inputmethod.InputMethodManager;

import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.container.ProductType;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class Master {

    //private static final String serverURL = "https://www.best-erp.com/ruralict/";

    private static final String serverURL = "http://ruralict.cse.iitb.ac.in/ruralict/";

    public static final String checkInternetURL = "http://www.google.com";

    public static String getLoginURL() {
        return serverURL + "app/login";
    }

    public static String getDetailsFormURL() {
        return serverURL + "app/loginform";
    }

    public static String getEnquiryFormURL() {
        return serverURL + "app/enquiry";
    }

    public static String getEditProfileURL() {
        return serverURL + "api/Test2/manageUsers/editProfile";
    }

    public static String getCheckVerificationURL(String number) {
        return serverURL + "app/emailVerifyLink/" + number;
    }


    public static String getNumberVerifyURL() {
        return serverURL + "app/numberverify";
    }

    public static String getChangeNumberURL() {
        return serverURL + "app/changenumber";
    }

    public static String getLeaveOrganisationURL() {
        return serverURL + "app/delete";
    }

    public static String getProductsURL(String orgAbbr) {
        return serverURL + "api/products/search/byType/mapnew?orgabbr=" + orgAbbr;
    }

    public static String getPlacingOrderURL() {
        return serverURL + "api/orders/add";
    }

    public static String getPaymentGatwayPlacingOrderURL() {
        return serverURL + "api/orders/addPG";
    }

    public static String getConfirmPGOrderURL() {
        return serverURL + "api/orders/confirmPG";
    }

    public static String getDeletePGOrderURL() {
        return serverURL + "api/orders/deletePG";
    }


    public static String getOrgInfoURL(String orgAbbr) {
        return serverURL + "api/" + orgAbbr + "/orginfo";
    }

    public static String getPlacedOrderURL(String orgAbbr, String mobileNumber) {
        return serverURL + "api/savedorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }

    public static String getCancellingOrderURL(String orderID) {
        return serverURL + "api/orders/update/" + orderID;
    }

    public static String getProcessedOrderURL(String orgAbbr, String mobileNumber) {

        return serverURL + "api/processedorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }


    public static String getDeliveredOrderURL(String orgAbbr, String mobileNumber) {

        return serverURL + "api/deliveredorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }

    public static String getRegisteredUserMobileVerifyURL() {
        return serverURL + "app/recoverotp";
    }

    public static String getRegisteredUserDetailsURL() {
        return serverURL + "app/recoverdetails";
    }

    public static String getReferCodeURL() {
        return serverURL + "app/referCode";
    }


    public static String getVersionCheckNewURL() {
        return serverURL + "app/versioncheckapp";
    }


    public static String getSendCommentURL() {
        return serverURL + "api/feedback";
    }

    public static String getSendReferURL() {
        return serverURL + "/app/refer";
    }

    public static String sendGCMTokenUrl() {
        return serverURL + "/app/registertoken";
    }

    public static String getAcceptReferralURL() {
        return serverURL + "app/acceptreferral";
    }


    public static String getUploadImageURL(String number) {

        return serverURL + "/api/profpicupload?phonenumber=" + "91" + number;
    }

    public static final String UpdateRequiredPref = "updateRequired";

    public static final String showAddToCartDialogPref = "showaddtocartdialog";

    public static final String showEmailDialogPref = "showemaildialog";


    public static final String AcceptReferralFailedPref = "acceptReferralFailed";

    public static final String ReferralCodePref = "referralCode";

    public static final String PRODUCT_DRAWER_ALERT_TAG = "productDrawer";

    public static final String VERSION_CHECK_NEW_PHONE_NUMBER_TAG = "phonenumber";
    public static final String VERSION_CHECK_NEW_VERSION_TAG = "version";

    public static final String VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG = "update";


    public static final String VERSION_CHECK_NEW_RESPONSE_FLAG_TAG = "flag";


    public static final String ACCEPT_REFERRAL_RESPONSE_TAG = "response";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG = "organization";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG = "abbr";
    public static final String ACCEPT_REFERRAL_USER_NAME_TAG = "name";
    public static final String ACCEPT_REFERRAL_USER_PROFILE_PIC_URL = "profilepic";
    public static final String ACCEPT_REFERRAL_USER_EMAIL_TAG = "email";
    public static final String ACCEPT_REFERRAL_USER_LAST_NAME_TAG = "lastname";
    public static final String ACCEPT_REFERRAL_PINCODE_TAG = "pincode";
    public static final String ACCEPT_REFERRAL_USER_NUMBER_TAG = "phonenumber";
    public static final String ACCEPT_REFERRAL_USER_ADDRESS_TAG = "address";
    public static final String ACCEPT_REFERRAL_UPDATE_MANDATORY_TAG = "update";
    public static final String ACCEPT_REFERRAL_DETAILS_PRESENT = "flag";


    private static final String IMAGE_FILE_TYPE = "image/jpg";


    public static final String
            PRODUCT_TAG = "product_fragment";
    public static final String PLACED_ORDER_TAG = "placed_order_fragment";
    public static final String PROCESSED_ORDER_TAG = "processed_order_fragment";
    public static final String DELIVERED_ORDER_TAG = "delivered_order_fragment";

    public static final String PROFILE_TAG = "profile_fragment";
    public static final String REFERRALS_TAG = "referrals_fragment";
    public static final String CHANGE_ORGANISATION_TAG = "organisation_fragment";
    public static final String FEEDBACKS_TAG = "feedbacks_fragment";
    public static final String TERMS_AND_CONDITIONS_TAG = "terms_and_conditions_fragment";
    public static final String FAQ_TAG = "faq_fragment";
    public static final String ABOUT_US_TAG = "about_us_fragment";

    public static int CART_ITEM_COUNT = 0;

    public static int PAYTM_STATUS = 0;

    public static int DELIVERY_CHARGES = 0;

    public static String BANKNAME, ACCOUNTNO, IFSC, MICR, ACCOUNTNAME, BRANCH, ACCOUNTTYPE;

    public static final String
            PRODUCT_NAME = "pname",
            PRICE = "price",
            TOTAL = "total",
            QUANTITY = "quantity",
            ID = "id",
            IMAGE_URL = "imageurl";

    public static final String
            STOCK_QUANTITY = "stockquantity";

    public static final String
            FNAME = "firstname",
            LNAME = "lastname",
            EMAIL = "email",
            ADDRESS = "address",
            MOBILENUMBER = "phonenumber",
            PASSWORD = "password",
            PINCODE = "pincode",
            STEPPER = "stepper",
            INTRO = "intro",
            RESPONSE = "response",
            STATUS = "status",
            ORGANISATIONS = "organizations",
            ORG_NAME = "name",
            ORG_ABBR = "abbr",
            ORG_CONTACT = "contact",
            SELECTED_ORG_NAME = "selectedOrgName",
            SELECTED_ORG_ABBR = "selectedOrgAbbr",
            STOCK_MANAGEMENT_STATUS = "stockManagement",
            emailVerified = "emailVerified",
            image = "image",
            link = "link",
            GST = "gst";

    public static final String feedbackText = "content";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";

    public static final String
            POST = "POST",
            GET = "GET";


    public static final String

            DEFAULT_LOGIN_JSON = "",
            DEFAULT_ORG_ABBR = "abbr";

    public static final Boolean
            DEFAULT_STEPPER = true;
    public static final Boolean DEFAULT_INTRO = true;
    public static final Boolean DIALOG_TRUE = true;
    public static final Boolean DIALOG_FALSE = false;
    public static final Boolean AUTH_TRUE = true;
    public static final Boolean AUTH_FALSE = false;

    public static final String
            LOGIN = "login",
            LOGIN_JSON = "loginJSON";

    public static final String
            PLACEDORDER = "placed";
    public static final String PROCESSEDORDER = "processed";
    public static final String DELIVEREDORDER = "delivered";


    public static void initialise(Context context) {

        Master.getMemberDetails(context);
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }


    public static boolean isMember;
    public static boolean isProductClicked;
    public static GetJSON getJSON;
    public static String response;


    public static ArrayList<Product> productList;
    public static ArrayList<Product> cartList;
    public static ArrayList<ProductType> productTypeList;


    public static void getMemberDetails(Context context) {

        DBHelper dbHelper = DBHelper.getInstance(context);

        dbHelper.getSignedInProfile();

    }


    public static void hideSoftKeyboard(Context context) {
        if (((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }


    public static String okhttpUpload(File file, String serverURL, String email, String password) {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);

        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse(Master.IMAGE_FILE_TYPE), file))
                .build();

        Request request = new Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .addHeader("authorization", "Basic " + new String(Base64.encode((email + ":" + password).getBytes(), Base64.NO_WRAP)))
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return "{ response: \"timeout\"}";
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception ex) {
            return null;
        }
    }


    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();

    }


    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }

        CartIconDrawable badge;


        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CartIconDrawable) {
            badge = (CartIconDrawable) reuse;
        } else {
            badge = new CartIconDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }


    public static void updateProductList() {
        for (int i = 0; i < Master.cartList.size(); ++i) {
            for (int j = 0; j < Master.productList.size(); ++j) {
                if (Master.productList.get(j).getID().equals(Master.cartList.get(i).getID())) {
                    Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    Master.productList.get(j).setUnitPrice(Master.cartList.get(i).getUnitPrice());
                    break;
                }
            }
        }
    }


    public static String version(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }


    public static void clearBitmap(Bitmap bm) {
        bm.recycle();
        System.gc();
    }


}
