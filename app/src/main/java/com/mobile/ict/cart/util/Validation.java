package com.mobile.ict.cart.util;

import android.content.Context;
import android.widget.EditText;

import com.mobile.ict.cart.R;


public class Validation {

    public static boolean isValidEmail(String target) {
        return target == null || !android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public static void hasText(Context ctx, EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        if (text.length() == 0) {
            editText.setError(ctx.getString(R.string.label_validation_required));
            editText.setText("");
        }

    }

}
