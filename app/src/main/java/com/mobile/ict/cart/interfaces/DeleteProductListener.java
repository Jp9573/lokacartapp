package com.mobile.ict.cart.interfaces;


public interface DeleteProductListener {
    void deleteProduct(int position, String productID);
}
