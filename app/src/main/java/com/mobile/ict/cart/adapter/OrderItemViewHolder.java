package com.mobile.ict.cart.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.util.Master;


import java.util.ArrayList;


public class OrderItemViewHolder extends ChildViewHolder {


    private final ListView itemListView;
    private final Context ctx;
    private final ImageView delete;
    private final EditDeletePlacedOrderInterface callback;
    private final TextView tdeliveryCharges;

    public OrderItemViewHolder(Context context, View itemView, EditDeletePlacedOrderInterface editDeletePlacedOrderInterface, String type) {
        super(itemView);
        ctx = context;
        callback = editDeletePlacedOrderInterface;
        itemListView = (ListView) itemView.findViewById(R.id.listView);
        delete = (ImageView) itemView.findViewById(R.id.delete);
        tdeliveryCharges = (TextView) itemView.findViewById(R.id.tvDeliveryCharges);
        TextView tdelete = (TextView) itemView.findViewById(R.id.tvDelete);


        RelativeLayout rlFooter = (RelativeLayout) itemView.findViewById(R.id.footer);

        /*if (type.equals(Master.PROCESSEDORDER) || type.equals(Master.DELIVEREDORDER)) {
            rlFooter.setVisibility(View.GONE);


        } else if (type.equals(Master.PLACEDORDER)) {
            rlFooter.setVisibility(View.VISIBLE);

        }*/

        if (type.equals(Master.PROCESSEDORDER) || type.equals(Master.DELIVEREDORDER))
        {
            delete.setVisibility(View.GONE);
            tdelete.setVisibility(View.GONE);


        } else if (type.equals(Master.PLACEDORDER)) {
            delete.setVisibility(View.VISIBLE);
            tdelete.setVisibility(View.VISIBLE);
        }



        itemListView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    }

    public void bind(ArrayList<String[]> list, String type,int deliveryCharges) {
        Resources r = ctx.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, r.getDisplayMetrics());


        LinearLayout.LayoutParams mParam;

        if (list.size() == 1) {

            mParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } else {
            mParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) px);

        }
        itemListView.setLayoutParams(mParam);



        itemListView.setAdapter(new DataAdapter(ctx, list));


        tdeliveryCharges.setText(ctx.getResources().getString(R.string.textview_delivery_charges)+" "+"\u20B9"+deliveryCharges);



        if (type.equals(Master.PLACEDORDER))
        {
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.deletePlacedOrder();
                }
            });

        }

    }


    public class DataAdapter extends BaseAdapter {
        public final ArrayList<String[]> list;
        final Context context;

        public DataAdapter(Context ctx, ArrayList<String[]> list) {
            super();
            context = ctx;
            this.list = list;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        private class ViewHolder {
            TextView productName, quantity, total;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.order_list_item, parent, false);
                holder = new ViewHolder();
                holder.productName = (TextView) convertView.findViewById(R.id.placed_order_quantityName);
                holder.quantity = (TextView) convertView.findViewById(R.id.placed_order_quantity);
                holder.total = (TextView) convertView.findViewById(R.id.placed_order_quantityTotalPrice);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String[] list = (String[]) getItem(position);

/*
            holder.productName.setText(list[0]);
            holder.quantity.setText(list[2]);
            holder.total.setText(list[3]);*/


            holder.productName.setText(list[0]);
            holder.quantity.setText(list[2]);
            holder.total.setText(list[3]);

            return convertView;
        }

    }
}
