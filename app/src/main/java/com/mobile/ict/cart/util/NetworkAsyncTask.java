package com.mobile.ict.cart.util;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import com.mobile.ict.cart.interfaces.GetResponse;
import org.json.JSONObject;

public class NetworkAsyncTask extends AsyncTask<Void, String, String> {

    public String emailid;
    public String password;
    public String requestMethod;
    public String dialogMessage;
    public String tagMsg, response;
    public JSONObject jsonObject;
    public String serverUrl;
    public boolean dialogE = true;
    public boolean auth;
    int flag, ctxFlag;
    private GetResponse getResponse = null;
    private Activity activity;
    private Fragment fragment;


    public NetworkAsyncTask(String url, String tag, boolean dialog, String dialogMsg, Activity act, JSONObject object, String method, boolean authentication, String email, String pwd) {
        serverUrl = url;
        dialogE = dialog;
        activity = act;
        emailid = email;
        password = pwd;
        requestMethod = method;
        dialogMessage = dialogMsg;
        jsonObject = object;
        auth = authentication;
        getResponse = (GetResponse) act;
        tagMsg = tag;
        flag = 1;
        ctxFlag = 1;
    }

    public NetworkAsyncTask(String url, String tag, boolean dialog, String dialogMsg, android.support.v4.app.Fragment frg, JSONObject object, String method, boolean authentication, String email, String pwd) {
        serverUrl = url;
        dialogE = dialog;
        fragment = frg;
        emailid = email;
        password = pwd;
        requestMethod = method;
        dialogMessage = dialogMsg;
        jsonObject = object;
        auth = authentication;
        getResponse = (GetResponse) frg;
        tagMsg = tag;
        flag = 1;
        ctxFlag = 0;
    }

    public NetworkAsyncTask(String url, boolean dialog, String dialogMsg, Activity act, String tag) {
        serverUrl = url;
        activity = act;
        getResponse = (GetResponse) act;
        tagMsg = tag;
        dialogE = dialog;
        dialogMessage = dialogMsg;
        flag = 0;
        ctxFlag = 1;
    }

    public NetworkAsyncTask(String url, boolean dialog, String dialogMsg, android.support.v4.app.Fragment frg, String tag) {
        serverUrl = url;
        fragment = frg;
        getResponse = (GetResponse) frg;
        tagMsg = tag;
        dialogE = dialog;
        dialogMessage = dialogMsg;
        flag = 0;
        ctxFlag = 0;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (dialogE && activity != null && !activity.isFinishing()) {
            Context ctx = activity;
            Material.circularProgressDialog(ctx, dialogMessage);

        } else {
            if (dialogE && fragment.getActivity() != null && !fragment.getActivity().isFinishing()) {

                Material.circularProgressDialog(fragment.getActivity(), dialogMessage);
            }

        }

    }

    @Override
    protected String doInBackground(Void... voids) {
        Master.getJSON = GetJSON.getInstance();

        if (flag != 0)
            return Master.getJSON.getJSONFromUrl(serverUrl, jsonObject, requestMethod, auth, emailid, password);
        else
            return Master.getJSON.getInternetActiveStatus(serverUrl);

    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        if (ctxFlag == 1) {
            if (activity != null && !activity.isFinishing()) {

                this.response = response;

                sendData();

            }
        } else if (ctxFlag == 0) {
            if (fragment.getActivity() != null && !fragment.getActivity().isFinishing()) {
                this.response = response;

                sendData();

            }
        }

    }

    public void sendData() {
        if (dialogE) {
            if (Material.circularProgressDialog != null && Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
        }
        getResponse.getData(response, tagMsg);
    }

}