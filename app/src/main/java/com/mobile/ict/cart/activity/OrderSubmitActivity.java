package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.SharedPreferenceConnector;


public class OrderSubmitActivity extends Activity implements View.OnClickListener{

    private Button submit;
   private DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();
       // LokacartApplication application = (LokacartApplication) getApplication();
       // Tracker mTracker = application.getDefaultTracker();
        String name = "OrderSubmit";
      //  mTracker.setScreenName("Screen~" + name);
      //  mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setContentView(R.layout.activity_order_submit);

        String orderId = getIntent().getExtras().getString("orderid");
        TextView orderid = (TextView) findViewById(R.id.message2);
        orderid.setText(getResources().getString(R.string.label_activity_ordersubmit_orderid) + ": " + orderId);
        submit = (Button) findViewById(R.id.button_order_submit_done);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view== submit)
        {
            SharedPreferenceConnector.writeString(this, "refresh", "refresh");
            Intent i = new Intent(this,DashboardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finishOrderSubmitActivity();
        }
    }

    private void finishOrderSubmitActivity() {
        OrderSubmitActivity.this.finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferenceConnector.writeString(this, "refresh", "refresh");

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();
    }
}
