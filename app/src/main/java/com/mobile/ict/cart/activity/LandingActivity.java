package com.mobile.ict.cart.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.BackgroundBitmapDrawable;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jp.wasabeef.blurry.Blurry;

public class LandingActivity extends AppCompatActivity implements GetResponse {

    private static int from;
    private DBHelper dbHelper;
    private static String goTo;
    private OTPCountDownTimer countDownTimer;
    private String otp_check;
    private long session;
    private EditText eMobileNumber, eReferralMobileNumber, eReferralCode;
    private EditText eOtp;
    private Button proceed;
    private Button verify;
    private ImageView imgVerify;
    private TextView verifyText;
    private Dialog authenticationDialogView, referralDialogView;
    Handler handler;
    private RadioGroup radioGroup;
    private RadioButton account, noAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        // LokacartApplication application = (LokacartApplication) getApplication();
        //  Tracker mTracker = application.getDefaultTracker();
        String name = "Landing";
        //  mTracker.setScreenName("Screen~" + name);
        //  mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        dbHelper = DBHelper.getInstance(this);


        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }

        });

        account = (RadioButton) findViewById(R.id.account);
        noAccount = (RadioButton) findViewById(R.id.no_account);


        Button continueButton = (Button) findViewById(R.id.continue_button);
        continueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();

                if (selectedId == account.getId()) {
                    openAuthenticationDialog();

                } else if (selectedId == noAccount.getId()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(LandingActivity.this);
                    builder.setMessage(getResources().getString(R.string.do_you_have_referral_code));
                    builder.setCancelable(false);
                    builder.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    openReferralDialog();
                                }
                            });

                    builder.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    startActivity(new Intent(LandingActivity.this, TalkToUsActivity.class));
                                    LandingActivity.this.finish();
                                }
                            });
                    builder.setNeutralButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();

                }
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.talk_to_us_bg);
            Drawable drawable = new BackgroundBitmapDrawable(getResources(), bitmap);
            ((View) findViewById(R.id.content)).setBackground(drawable);
        } else
            ((ImageView) findViewById(R.id.bgImage)).setImageDrawable(getResources().getDrawable(R.mipmap.talk_to_us_bg));


        final ImageView imageView = ((ImageView) findViewById(R.id.logoImage));
        final ImageView circleView = ((ImageView) findViewById(R.id.logoCircle));

        handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                Log.d(getString(R.string.app_name), "on Runnable");

                Animation fadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    circleView.setImageDrawable(getDrawable(R.drawable.circle));
                    imageView.setImageDrawable(getDrawable(R.mipmap.s_logo));
                    Blurry.with(LandingActivity.this)
                            .radius(20)
                            .sampling(2)
                            .async()
                            .animate(2000)
                            .onto((ViewGroup) findViewById(R.id.content));
                } else {
                    circleView.setImageDrawable(getResources().getDrawable(R.drawable.circle));
                    imageView.setImageDrawable(getResources().getDrawable(R.mipmap.s_logo));
                    Blurry.with(LandingActivity.this)
                            .radius(20)
                            .sampling(2)
                            .async()
                            .animate(2000)
                            .capture((ImageView) findViewById(R.id.bgImage))
                            .into((ImageView) findViewById(R.id.bgImage));
                }
                circleView.startAnimation(fadeInAnimation);
                imageView.startAnimation(fadeInAnimation);

            }
        };
        handler.postDelayed(r, 500);

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
    }

    private void storeUserDetails(JSONObject rootJson) {


        String name, address, lastname, pincode, phonenumber, email, abbr, organisation, password, profilePicUrl;
        abbr = organisation = profilePicUrl = "null";


        try {
            name = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NAME_TAG);
            address = rootJson.getString(Master.ACCEPT_REFERRAL_USER_ADDRESS_TAG);
            lastname = rootJson.getString(Master.ACCEPT_REFERRAL_USER_LAST_NAME_TAG);
            pincode = rootJson.getString(Master.ACCEPT_REFERRAL_PINCODE_TAG);
            phonenumber = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NUMBER_TAG);
            password = rootJson.getString("token");
            email = rootJson.getString(Master.ACCEPT_REFERRAL_USER_EMAIL_TAG);
            if (rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG) && rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG)) {
                abbr = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG);
                organisation = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
            }

            if (rootJson.has(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL)) {
                profilePicUrl = rootJson.getString(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL);

            }
            MemberDetails.setProfileImageUrl(profilePicUrl);
            MemberDetails.setFname(name);
            MemberDetails.setAddress(address);
            MemberDetails.setLname(lastname);
            MemberDetails.setPincode(pincode);
            MemberDetails.setMobileNumber(phonenumber.substring(2));

            if (email.equals("null"))
                MemberDetails.setEmail(phonenumber + "@gmail.com");
            else
                MemberDetails.setEmail(email);

            MemberDetails.setSelectedOrgAbbr(abbr);
            MemberDetails.setSelectedOrgName(organisation);
            MemberDetails.setPassword(password);


            //call db helper to store it to database


            //check if selected org is already present
            String selectedOrg[] = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

            //add the details about the user to database
            dbHelper.addProfile();


            if (selectedOrg == null || selectedOrg[0].equals("null")) {

                String organisation2Name = "";
                String organisation2Abbr = "";

                //in the new version check api if we give the number of user the response will
                // contain list of organisations he is part of
                //Jsonarray memberships will only be present if we call the new version check api with user mobile number
                if (rootJson.has("orglist")) {
                    JSONArray organisationsList = rootJson.getJSONArray("orglist");
                    JSONObject org = organisationsList.getJSONObject(0);
                    organisation2Name = org.getString("organization");
                    organisation2Abbr = org.getString("abbr");

                }

                if (organisation2Name.equals("") || organisation2Name.equals("null")
                        || organisation2Abbr.equals("") || organisation2Abbr.equals("null")) {
                    dbHelper.setSelectedOrg(phonenumber, organisation, abbr);
                } else {
                    dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(), organisation2Name, organisation2Abbr);
                }


            } else {
                dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(), selectedOrg[1], selectedOrg[0]);


            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void forceUpdateDialog() {


        //storing the update required in shared pref , if it is set to 2 ,
        // we show a forceupdate dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 2);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    //this will open play store and show lokacart app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    //if opening in play store fails open play store in browser
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
            }
        });

        builder.setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showRecommendUpdateDialog() {
        from = 2;

        //storing the update required in shared pref , if it is set to 1 ,
        // we show a recommend dialog even if net is not available
        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 1);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //ignore update dialog and continue with the app

                if (from == 2) {
                    ShowStepper(goTo);

                }


            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void ShowStepper(@NonNull String nextPage) {
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

            Intent i = new Intent(getApplicationContext(), StepperActivity.class);
            i.putExtra("redirectto", nextPage);
            switch (nextPage) {
                case "profile": {
                    i.putExtra("showintro", "true");

                    break;
                }
                case "organisation": {
                    i.putExtra("showintro", "true");
                    break;
                }
                case "dashboard": {
                    i.putExtra("showintro", "true");
                    break;
                }
                case "talktous": {

                    break;
                }


            }

            startActivity(i);
            finish();
        } else {
            goToActivity(nextPage);
        }
    }

    private void goToActivity(@NonNull String nextPage) {
        authenticationDialogView.dismiss();
        authenticationDialogView = null;

        Intent i;
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.INTRO, Master.DEFAULT_INTRO)) {
            switch (nextPage) {
                case "profile": {
                    i = new Intent(LandingActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation": {

                    i = new Intent(LandingActivity.this, DemoLists.class);
                    i.putExtra("redirectto", "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard": {
                    i = new Intent(LandingActivity.this, DemoLists.class);
                    i.putExtra("redirectto", "dashboard");
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous": {
                    i = new Intent(LandingActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }

        } else {
            switch (nextPage) {
                case "profile": {
                    i = new Intent(LandingActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation": {

                    i = new Intent(LandingActivity.this, DashboardActivity.class);
                    i.putExtra("redirectto", "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard": {
                    i = new Intent(LandingActivity.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous": {
                    i = new Intent(LandingActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
            }
        }

    }


    private void openAuthenticationDialog() {

        authenticationDialogView = new Dialog(LandingActivity.this);
        authenticationDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        authenticationDialogView.setContentView(R.layout.user_authentication);
        authenticationDialogView.setCancelable(false);

        eMobileNumber = (EditText) authenticationDialogView.findViewById(R.id.etMobileNumber);
        eOtp = (EditText) authenticationDialogView.findViewById(R.id.etOtp);
        verifyText = (TextView) authenticationDialogView.findViewById(R.id.tvVerifyText);

        verifyText.setVisibility(View.INVISIBLE);


        Button cancel = (Button) authenticationDialogView.findViewById(R.id.bCancel);
        proceed = (Button) authenticationDialogView.findViewById(R.id.bProceed);
        verify = (Button) authenticationDialogView.findViewById(R.id.bVerifyMobileNumber);
        imgVerify = (ImageView) authenticationDialogView.findViewById(R.id.imgVerify);
        imgVerify.setImageResource(R.drawable.verify_tick);
        imgVerify.setEnabled(false);
        proceed.setEnabled(false);
        verify.setEnabled(true);
        eMobileNumber.setEnabled(true);
        eMobileNumber.setText("");
        eOtp.setText("");
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Master.isNetworkAvailable(LandingActivity.this)) {

                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection), LandingActivity.this, "MobileVerification_Internet").execute();


                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                }
            }
        });


        imgVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long session2 = System.currentTimeMillis();

                if (session2 < session) {
                    if (eOtp.getText().toString().trim().isEmpty()) {
                        imgVerify.setImageResource(R.drawable.verify_tick);
                        imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                        imgVerify.setEnabled(true);
                        proceed.setEnabled(false);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.INVISIBLE);
                        Toast.makeText(LandingActivity.this, R.string.toast_enter_otp_received, Toast.LENGTH_LONG).show();

                    } else if (eOtp.getText().toString().trim().equals(otp_check)) {
                        countDownTimer.cancel();
                        imgVerify.setEnabled(false);
                        imgVerify.setImageResource(R.drawable.verify_tick);
                        imgVerify.setColorFilter(Color.argb(255, 90, 193, 67));
                        proceed.setEnabled(true);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.VISIBLE);
                        verifyText.setText(getString(R.string.textview_verify_tick_text));
                    } else {
                        imgVerify.setEnabled(true);
                        imgVerify.setImageResource(R.drawable.verify_wrong);
                        imgVerify.setColorFilter(Color.argb(255, 228, 8, 41));
                        proceed.setEnabled(false);
                        verify.setEnabled(false);
                        verifyText.setVisibility(View.VISIBLE);
                        verifyText.setText(getString(R.string.textview_verify_wrong_text));
                        Toast.makeText(LandingActivity.this, R.string.toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();

                    }
                } else {
                    eOtp.setText("");
                    imgVerify.setImageResource(R.drawable.verify_tick);
                    imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                    imgVerify.setEnabled(true);
                    proceed.setEnabled(false);
                    verify.setEnabled(true);
                    verifyText.setVisibility(View.INVISIBLE);

                    Toast.makeText(LandingActivity.this, R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
                }

            }
        });


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Master.isNetworkAvailable(LandingActivity.this)) {

                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection), LandingActivity.this, "RecoverDetail_Internet").execute();

                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (countDownTimer != null)
                    countDownTimer.cancel();
                authenticationDialogView.dismiss();
                authenticationDialogView = null;

            }
        });


        authenticationDialogView.show();
    }


    private void openReferralDialog() {
        referralDialogView = new Dialog(LandingActivity.this);
        referralDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialogView.setContentView(R.layout.dialog_referral_code);
        referralDialogView.setCancelable(false);
        eReferralMobileNumber = (EditText) referralDialogView.findViewById(R.id.eReferralMobileNumber);
        eReferralCode = (EditText) referralDialogView.findViewById(R.id.eReferralCode);
        Button referralCancel = (Button) referralDialogView.findViewById(R.id.bReferralCancel);
        Button submit = (Button) referralDialogView.findViewById(R.id.bReferralSubmit);

        referralCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialogView.dismiss();
                referralDialogView = null;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Master.isNetworkAvailable(LandingActivity.this)) {

                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection), LandingActivity.this, "CheckReferralCode_Internet").execute();


                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                }

            }
        });

        referralDialogView.show();
    }


    @Override
    public void getData(String object, String tag) {

        switch (tag) {

            case "MobileVerification_Internet":

                if (object.equals("success")) {

                    if (eMobileNumber.getText().toString().trim().length() != 10)
                        Toast.makeText(LandingActivity.this, R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
                    else if (eMobileNumber.getText().toString().trim().charAt(0) == '0')
                        Toast.makeText(LandingActivity.this, R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
                    else {
                        eMobileNumber.setEnabled(false);
                        verify.setEnabled(false);

                        JSONObject obj = new JSONObject();
                        try {

                            obj.put("phonenumber", "91" + eMobileNumber.getText().toString().trim());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        new NetworkAsyncTask(
                                Master.getRegisteredUserMobileVerifyURL(),
                                "mobileverifcation",
                                Master.DIALOG_TRUE,
                                getString(R.string.pd_sending_data_to_server),
                                LandingActivity.this,
                                obj,
                                Master.POST,
                                Master.AUTH_FALSE,
                                null,
                                null
                        ).execute();
                    }
                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }
                break;

            case "mobileverifcation":

                if (object.equals("exception")) {
                    Material.alertDialog(LandingActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems_landing_page), "OK");
                    eMobileNumber.setEnabled(true);
                    verify.setEnabled(true);

                } else {
                    try {
                        JSONObject responseObject = new JSONObject(object);
                        String otp_val = responseObject.getString("otp");
                        String text = responseObject.getString("text");
                        imgVerify.setImageResource(R.drawable.verify_tick);
                        imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                        verifyText.setVisibility(View.INVISIBLE);

                        if (!otp_val.equals("null") && !text.equals("null")) {

                            Toast.makeText(LandingActivity.this, R.string.toast_OTP_has_sent_to_your_mobile_sms, Toast.LENGTH_LONG).show();
                            verify.setEnabled(false);
                            otp_check = otp_val;
                            session = System.currentTimeMillis();
                            countDownTimer = new OTPCountDownTimer();
                            countDownTimer.start();
                            imgVerify.setEnabled(true);

                            session = session + 600000; // 10 minutes

                        } else if (otp_val.equals("null") && text.equals("User does not exist. Please get a referral")) {

                            eMobileNumber.setEnabled(true);
                            verify.setEnabled(true);


                            Toast.makeText(LandingActivity.this, R.string.toast_user_does_not_exists, Toast.LENGTH_LONG).show();
                        } else if (otp_val.equals("null") && text.equals("User is an administrator. Please use a consumer account.")) {

                            eMobileNumber.setEnabled(true);
                            verify.setEnabled(true);


                            Toast.makeText(LandingActivity.this, R.string.toast_error_cannot_refer_admin, Toast.LENGTH_LONG).show();
                        }


                    } catch (JSONException e) {
                        eMobileNumber.setEnabled(true);
                        verify.setEnabled(true);
                        Toast.makeText(LandingActivity.this, R.string.toast_we_are_facing_some_technical_problems_landing_page, Toast.LENGTH_LONG).show();

                    }
                }


                break;


            case "RecoverDetail_Internet":
                if (object.equals("success")) {


                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("phonenumber", "91" + eMobileNumber.getText().toString().trim());
                        jsonObject.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Master.version(getApplicationContext()));

                        new NetworkAsyncTask(
                                Master.getRegisteredUserDetailsURL(),
                                "registeredUserDetails",
                                Master.DIALOG_TRUE,
                                getString(R.string.pd_sending_data_to_server),
                                LandingActivity.this,
                                jsonObject,
                                Master.POST,
                                Master.AUTH_FALSE,
                                null,
                                null
                        ).execute();

                    } catch (JSONException e) {
                    }

                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }
                break;


            case "CheckReferralCode_Internet":

                if (object.equals("success")) {

                    if (eReferralMobileNumber.getText().toString().trim().equals("") && eReferralCode.getText().toString().trim().equals(""))

                        Toast.makeText(LandingActivity.this, R.string.toast_Please_enter_mobile_referralcode, Toast.LENGTH_LONG).show();

                    else if (eReferralMobileNumber.getText().toString().trim().length() != 10 && !eReferralCode.getText().toString().trim().equals(""))

                        Toast.makeText(LandingActivity.this, R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();

                    else if (eReferralMobileNumber.getText().toString().trim().length() == 10 && eReferralCode.getText().toString().trim().equals(""))

                        Toast.makeText(LandingActivity.this, R.string.toast_Please_enter_referral_code, Toast.LENGTH_LONG).show();

                    else {

                        eReferralMobileNumber.setEnabled(false);
                        eReferralCode.setEnabled(false);

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("phonenumber", "91" + eReferralMobileNumber.getText().toString().trim());
                            jsonObject.put("refCode", eReferralCode.getText().toString().trim());

                            new NetworkAsyncTask
                                    (
                                            Master.getReferCodeURL(),
                                            "referralCode",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_data_to_server),
                                            LandingActivity.this,
                                            jsonObject,
                                            Master.POST,
                                            Master.AUTH_FALSE,
                                            null,
                                            null
                                    ).execute();

                        } catch (JSONException e) {
                        }
                    }


                } else {
                    Toast.makeText(LandingActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }

                break;


            case "registeredUserDetails":


                if (object.equals("exception")) {
                    Material.alertDialog(LandingActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems_landing_page), "OK");
                } else {
                    try {

                        try {
                            JSONObject responseJson = new JSONObject(object);

                            //store user details if present
                            storeUserDetails(responseJson);

                            //either  0,1 or 2
                            //0 -> no update required
                            //1 -> update recommended
                            //2 -> update necessary
                            String updateValue = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG);

                            switch (updateValue) {
                                case "0": {
                                    // case -> no update needed
                                    //storing the update required in shared pref , if it is set to 0 , no need to update
                                    SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 0);


                                    //response will contain details present flag only if we give the user mobile number if the api call
                                    if (responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {


                                        //check if all details are present
                                        String flag;
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        // all details present
                                        if (flag.equals("0")) {
                                            ShowStepper("dashboard");
                                        } else {
                                            //all details are not present , get details from user
                                            ShowStepper("profile");

                                        }
                                    }
                                    break;
                                }
                                case "1": {
                                    // case ->  update recommended

                                    //if all details are prenent or not will only be
                                    // returned if we give the mobile number to the api call
                                    if (responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {

                                        //check if all details are present
                                        String flag;
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        // all details present
                                        if (flag.equals("0")) {


                                            goTo = "dashboard";

                                        } else {
                                            //all details are not present , get details from user
                                            goTo = "profile";
                                        }

                                        showRecommendUpdateDialog();
                                    }

                                    break;
                                }
                                case "2": {
                                    // case -> update compulsory
                                    forceUpdateDialog();
                                    break;

                                }

                            }


                        } catch (JSONException e) {
                            Toast.makeText(LandingActivity.this, R.string.toast_we_are_facing_some_technical_problems_landing_page, Toast.LENGTH_LONG).show();

                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        Toast.makeText(LandingActivity.this, R.string.toast_we_are_facing_some_technical_problems_landing_page, Toast.LENGTH_LONG).show();

                    }
                }


                break;


            case "referralCode":

                if (object.equals("exception")) {
                    eReferralMobileNumber.setEnabled(true);
                    eReferralCode.setEnabled(true);
                    Material.alertDialog(LandingActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems_landing_page), "OK");
                } else {

                    try {
                        eReferralMobileNumber.setEnabled(true);
                        eReferralCode.setEnabled(true);

                        JSONObject obj = new JSONObject(object);
                        String status = obj.getString("response");

                        switch (status) {
                            case "success":
                                referralDialogView.dismiss();

                                Toast.makeText(LandingActivity.this, R.string.toast_get_sms_referral, Toast.LENGTH_LONG).show();

                                break;

                            case "Already a member":
                                Toast.makeText(LandingActivity.this, R.string.toast_already_member_organizations, Toast.LENGTH_LONG).show();

                                break;


                            case "admin":
                                Toast.makeText(LandingActivity.this, R.string.toast_error_cannot_refer_admin, Toast.LENGTH_LONG).show();

                                break;

                            case "No such code":
                                Toast.makeText(LandingActivity.this, R.string.toast_referral_mismatch, Toast.LENGTH_LONG).show();

                                break;

                            default:
                                break;
                        }
                    } catch (Exception e) {
                        Toast.makeText(LandingActivity.this, R.string.toast_we_are_facing_some_technical_problems_landing_page, Toast.LENGTH_LONG).show();

                    }

                }
                break;


            default:
                break;

        }

    }


    public class OTPCountDownTimer extends CountDownTimer {

        public OTPCountDownTimer() {
            super((long) 600000, (long) 1000);
        }

        @Override
        public void onFinish() {
            if (authenticationDialogView != null) {
                eOtp.setText("");
                imgVerify.setImageResource(R.drawable.verify_tick);
                imgVerify.setColorFilter(Color.argb(255, 255, 255, 255));
                imgVerify.setEnabled(false);
                verify.setEnabled(true);
                proceed.setEnabled(false);
                verifyText.setVisibility(View.INVISIBLE);

                Toast.makeText(LandingActivity.this, R.string.toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!(authenticationDialogView != null && authenticationDialogView.isShowing())) {
            finish();
        }
    }
}