package com.mobile.ict.cart.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


public class Material {

    public static ProgressDialog circularProgressDialog;

    public static void circularProgressDialog(Context context, String message) {
        circularProgressDialog = new ProgressDialog(context);
        circularProgressDialog.setCancelable(false);
        circularProgressDialog.setMessage(message);
        circularProgressDialog.show();
    }

    public static void alertDialog(Context context, String message, String button_text) //for showing the messages on an alertbox
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                button_text,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder.create();
        alert11.show();
    }

    public static void alertDialog(Context context, String title, String message, String button_text) //for showing the messages on an alertbox
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                button_text,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder.create();
        alert11.show();
    }
}
