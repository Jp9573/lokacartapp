package com.mobile.ict.cart.container;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.util.Master;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;


public class Orders implements ParentListItem {

    private List<ArrayList<String[]>> items;

    @Override
    public List<?> getChildItemList() {
        return items;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }


    private int order_id;

    public int getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(int deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    private int deliveryCharges;
    private String timeStamp,deliveryTimeStamp;

    public String getDeliveryTimeStamp() {
        return deliveryTimeStamp;
    }

    public void setDeliveryTimeStamp(String deliveryTimeStamp) {
        this.deliveryTimeStamp = deliveryTimeStamp;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    private double totalBill;
    private String paymentStatus;

    public double getTotalBill() {
        return totalBill;
    }

    private TreeMap<String, String[]> placedOrderItemLists;
    private TreeMap<String, String[]> processedOrderItemLists;
    private TreeMap<String, String[]> deliveredOrderItemLists;


    public Orders(JSONObject order, int pos, String type) {

        switch (type) {
            case Master.PLACEDORDER:

                try {
                    this.totalBill = 0.0;
                    this.order_id = order.getInt("orderId");
                    this.deliveryCharges=order.getInt("delCharges");
                    this.timeStamp = order.getString("registeredTime");
                    JSONArray orderItems = order.getJSONArray("orderItems");
                    this.placedOrderItemLists = new TreeMap<>();
                    HashSet<String> set = new HashSet<>();
                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");
                        String[] temp = new String[3];

                        temp[0] = item.getString("unitrate");
                        int quantity = Integer.parseInt(item.getString("quantity"));
                        if (!set.contains(name)) {
                            set.add(name);
                            temp[1] = item.getString("quantity");
                        } else {
                            temp[1] = String.valueOf(Integer.parseInt(this.placedOrderItemLists.get(pos + name)[1]) + Integer.parseInt(item.getString("quantity")));
                        }

                        temp[2] = item.getString("stockquantity");
                        this.placedOrderItemLists.put(pos + name, temp);
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[0]) * quantity))));

                    }
                    /*String[] temp = new String[4];

                    temp[0]=String.valueOf(order.getInt("delCharges"));

                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");

                        temp[1] = item.getString("unitrate");
                        int quantity = Integer.parseInt(item.getString("quantity"));
                        if (!set.contains(name)) {
                            set.add(name);
                            temp[2] = item.getString("quantity");
                        } else {
                            temp[2] = String.valueOf(Integer.parseInt(this.placedOrderItemLists.get(pos + name)[2]) + Integer.parseInt(item.getString("quantity")));
                        }

                        temp[3] = item.getString("stockquantity");
                        this.placedOrderItemLists.put(pos + name, temp);
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[0]) * quantity))));

                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case Master.PROCESSEDORDER:

                try {
                    this.totalBill = 0.0;
                    this.order_id = order.getInt("orderId");
                    this.deliveryCharges=order.getInt("delCharges");

                    this.timeStamp = order.getString("registeredTime");

                    if(order.has("delivery"))
                        this.deliveryTimeStamp=order.getString("delivery");
                    else
                        this.deliveryTimeStamp="Not Applicable";

                    JSONArray orderItems = order.getJSONArray("orderItems");
                    this.processedOrderItemLists = new TreeMap<>();
                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");
                        String[] temp = new String[2];
                        temp[0] = item.getString("unitrate");
                        temp[1] = item.getString("quantity");
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[0]) * Integer.parseInt(temp[1])))));
                        this.processedOrderItemLists.put(pos + name, temp);


                    }

                   /* String[] temp = new String[3];

                    temp[0]=String.valueOf(order.getInt("delCharges"));

                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");
                        temp[1] = item.getString("unitrate");
                        temp[2] = item.getString("quantity");
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[1]) * Integer.parseInt(temp[2])))));
                        this.processedOrderItemLists.put(pos + name, temp);


                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            default:

                try {
                    this.totalBill = 0.0;
                    this.order_id = order.getInt("orderId");
                    this.deliveryCharges=order.getInt("delCharges");

                    this.timeStamp = order.getString("registeredTime");

                    if(order.has("delivery"))
                        this.deliveryTimeStamp=order.getString("delivery");
                    else
                        this.deliveryTimeStamp="Not Applicable";

                    this.paymentStatus = order.getString("payment");
                    JSONArray orderItems = order.getJSONArray("orderItems");
                    this.deliveredOrderItemLists = new TreeMap<>();
                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");
                        String[] temp = new String[2];
                        temp[0] = item.getString("unitrate");
                        temp[1] = item.getString("quantity");
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[0]) * Integer.parseInt(temp[1])))));
                        this.deliveredOrderItemLists.put(pos + name, temp);


                    }

                    /*String[] temp = new String[3];

                    temp[0]=String.valueOf(order.getInt("delCharges"));

                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject item = (JSONObject) orderItems.get(i);
                        String name = item.getString("productname");
                        temp[1] = item.getString("unitrate");
                        temp[2] = item.getString("quantity");
                        this.totalBill = this.totalBill + (Double.parseDouble(String.format("%.2f", (Double.parseDouble(temp[1]) * Integer.parseInt(temp[2])))));
                        this.deliveredOrderItemLists.put(pos + name, temp);


                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
        }

    }


    public int getOrder_id() {
        return order_id;
    }


    public String getTimeStamp() {
        return timeStamp;
    }


    public void getPlacedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res;

        int posLen = ("" + pos).length();


        for (String itemName : placedOrderItemLists.keySet()) {

            res = new String[5];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = placedOrderItemLists.get(itemName)[0];
            res[2] = placedOrderItemLists.get(itemName)[1];

            double rate = Double.parseDouble(placedOrderItemLists.get(itemName)[0]);
            int quantity = Integer.parseInt(placedOrderItemLists.get(itemName)[1]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));
            res[3] = "" + total;
            res[4] = placedOrderItemLists.get(itemName)[2];


            itemlist.add(res);
        }


       /* for (String itemName : placedOrderItemLists.keySet()) {

            res = new String[6];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = placedOrderItemLists.get(itemName)[0];
            res[2] = placedOrderItemLists.get(itemName)[1];
            res[3] = placedOrderItemLists.get(itemName)[2];

            double rate = Double.parseDouble(placedOrderItemLists.get(itemName)[1]);
            int quantity = Integer.parseInt(placedOrderItemLists.get(itemName)[2]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));
            res[4] = "" + total;
            res[5] = placedOrderItemLists.get(itemName)[3];


            itemlist.add(res);
        }*/
        this.items = Collections.singletonList(itemlist);

    }


    public void getProcessedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res;

        int posLen = ("" + pos).length();


        for (String itemName : processedOrderItemLists.keySet()) {

            res = new String[4];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = processedOrderItemLists.get(itemName)[0];
            res[2] = processedOrderItemLists.get(itemName)[1];
            double rate = Double.parseDouble(processedOrderItemLists.get(itemName)[0]);
            int quantity = Integer.parseInt(processedOrderItemLists.get(itemName)[1]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));
            res[3] = "" + total;
            itemlist.add(res);
        }

       /* for (String itemName : processedOrderItemLists.keySet()) {

            res = new String[5];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = processedOrderItemLists.get(itemName)[0];
            res[2] = processedOrderItemLists.get(itemName)[1];
            res[3] = processedOrderItemLists.get(itemName)[2];

            double rate = Double.parseDouble(processedOrderItemLists.get(itemName)[1]);
            int quantity = Integer.parseInt(processedOrderItemLists.get(itemName)[2]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));
            res[4] = "" + total;
            itemlist.add(res);
        }*/
        this.items = Collections.singletonList(itemlist);

    }


    public void getDeliveredOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res;

        int posLen = ("" + pos).length();


        for (String itemName : deliveredOrderItemLists.keySet()) {

            res = new String[4];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = deliveredOrderItemLists.get(itemName)[0];
            res[2] = deliveredOrderItemLists.get(itemName)[1];

            double rate = Double.parseDouble(deliveredOrderItemLists.get(itemName)[0]);
            int quantity = Integer.parseInt(deliveredOrderItemLists.get(itemName)[1]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));

            res[3] = "" + total;

            itemlist.add(res);
        }

       /* for (String itemName : deliveredOrderItemLists.keySet()) {

            res = new String[5];
            String name = itemName.substring(posLen);
            res[0] = name;
            res[1] = deliveredOrderItemLists.get(itemName)[0];
            res[2] = deliveredOrderItemLists.get(itemName)[1];
            res[3] = deliveredOrderItemLists.get(itemName)[2];


            double rate = Double.parseDouble(deliveredOrderItemLists.get(itemName)[1]);
            int quantity = Integer.parseInt(deliveredOrderItemLists.get(itemName)[2]);
            double total = Double.parseDouble(String.format("%.2f", (quantity * rate)));

            res[4] = "" + total;

            itemlist.add(res);
        }
*/
        this.items = Collections.singletonList(itemlist);
    }


}
