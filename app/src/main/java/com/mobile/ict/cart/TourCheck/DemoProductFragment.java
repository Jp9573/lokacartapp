package com.mobile.ict.cart.TourCheck;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.activity.ProfileActivity;

import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import java.util.ArrayList;


import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;


public class DemoProductFragment extends Fragment {
    private ImageView btnClickMe1;
    private TextView btnClickMe2;
    private TextView btnClickMe3;
    private RelativeLayout relativeLayout;
    private Drawer result;
    private Drawer result1;
    private ImageView swipe;
    private ImageView swipe2;
    private TourGuide mTourGuideHandler;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //LokacartApplication application = (LokacartApplication) getActivity().getApplication();
       // Tracker mTracker = application.getDefaultTracker();
        String name = "DemoProduct";
       // mTracker.setScreenName("Screen~" + name);
      //  mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setHasOptionsMenu(true);
        Master.isProductClicked = false;
        View productFragmentView = inflater.inflate(R.layout.demo_fragment_product, container, false);

        relativeLayout = (RelativeLayout) productFragmentView.findViewById(R.id.relativeLayout);

        Button gone_swipe = (Button) productFragmentView.findViewById(R.id.button);
        Button gone_swipe2 = (Button) productFragmentView.findViewById(R.id.button2);
        ImageView ivImage1 = (ImageView) productFragmentView.findViewById(R.id.ivProduct1);
        ImageView ivImage2 = (ImageView) productFragmentView.findViewById(R.id.ivProduct2);
        ImageView ivImage3 = (ImageView) productFragmentView.findViewById(R.id.ivProduct3);
        ImageView ivImage4 = (ImageView) productFragmentView.findViewById(R.id.ivProduct4);

        if (Master.isNetworkAvailable(getActivity())) {
            Glide.with(getContext())
                    .load("https://www.best-erp.com/extras/images/silly_veggie_1.jpg")
                    .centerCrop()
                    .into(ivImage1);
            Glide.with(getContext())
                    .load("https://www.best-erp.com/extras/images/silly_veggie_2.jpg")
                    .centerCrop()
                    .into(ivImage2);
            Glide.with(getContext())
                    .load("https://www.best-erp.com/extras/images/silly_veggie_3.jpg")
                    .centerCrop()
                    .into(ivImage3);
            Glide.with(getContext())
                    .load("https://www.best-erp.com/extras/images/silly_veggie_4.jpg")
                    .centerCrop()
                    .into(ivImage4);
        } else {
            Glide.with(getContext())
                    .load("null")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage1);
            Glide.with(getContext())
                    .load("null")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage2);
            Glide.with(getContext())
                    .load("null")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage3);
            Glide.with(getContext())
                    .load("null")
                    .placeholder(R.drawable.placeholder_products)
                    .centerCrop()
                    .into(ivImage4);
        }


        btnClickMe1 = (ImageView) productFragmentView.findViewById(R.id.ivBuy1);
        btnClickMe2 = (TextView) productFragmentView.findViewById(R.id.tQuantity1);
        btnClickMe3 = (TextView) productFragmentView.findViewById(R.id.tPrice1);
        swipe = (ImageView) productFragmentView.findViewById(R.id.imageView1);
        swipe2 = (ImageView) productFragmentView.findViewById(R.id.imageView2);


        result1 = new DrawerBuilder()
                .withActivity(getActivity())
                .withDisplayBelowStatusBar(true)
                .withTranslucentStatusBar(false)
                .withHasStableIds(true).addDrawerItems(
                        new PrimaryDrawerItem().withName("").withIdentifier(12),
                        new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                        new ExpandableDrawerItem().withName(R.string.menu_orders).withIcon(R.drawable.orders).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.menu_placed_orders).withLevel(5).withIdentifier(1),
                                new SecondaryDrawerItem().withName(R.string.menu_processed_orders).withLevel(5).withIdentifier(2)
                        ),
                        new PrimaryDrawerItem().withName(R.string.menu_profile).withIcon(R.drawable.profile).withIdentifier(11),
                        new PrimaryDrawerItem().withName(R.string.menu_change_organisation).withIcon(R.drawable.organisation).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.menu_referrals).withIcon(R.drawable.referrals).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.menu_about_us).withIcon(R.drawable.about_us).withIdentifier(5),
                        new ExpandableDrawerItem().withName(R.string.menu_help).withIcon(R.drawable.help).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.menu_feedback).withLevel(5).withIdentifier(6),
                                new SecondaryDrawerItem().withName(R.string.menu_contact_us).withLevel(5).withIdentifier(7),
                                new SecondaryDrawerItem().withName(R.string.menu_terms_and_conditions).withLevel(5).withIdentifier(8)))

                .buildForFragment();


        mTourGuideHandler = TourGuide.init(getActivity()).with(TourGuide.Technique.Click)
                .setPointer(new Pointer().setColor(Color.RED))
                .setToolTip(
                        new ToolTip()
                                .setTitle("Swipe Right to Left to change products Type ")
                                .setDescription("")
                                .setGravity(Gravity.LEFT)
                                .setBackgroundColor(Color.parseColor("#4b4b4b"))
                                .setShadow(true)
                )
                .setOverlay(new Overlay().disableClickThroughHole(false))
                .playOn(gone_swipe);
        swipe.setVisibility(View.VISIBLE);


        btnClickMe1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickMe2(v);
            }
        });
        btnClickMe2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickMe3(v);

            }
        });
        btnClickMe3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mTourGuideHandler.cleanUp();


                SharedPreferenceConnector.writeBoolean(getActivity(), Master.INTRO, false);


                if (getActivity().getIntent().hasExtra("redirectto")) {
                    String redirectto = getActivity().getIntent().getStringExtra("redirectto");
                    Intent i;
                    switch (redirectto) {

                        case "profile": {
                            i = new Intent(getActivity(), ProfileActivity.class);
                            startActivity(i);
                            getActivity().finish();
                            break;
                        }
                        case "organisation": {
                            i = new Intent(getActivity(), DashboardActivity.class);
                            i.putExtra("redirectto", "organisation");
                            startActivity(i);
                            getActivity().finish();
                            break;
                        }
                        case "dashboard": {
                            i = new Intent(getActivity(), DashboardActivity.class);
                            i.putExtra("redirectto", redirectto);
                            startActivity(i);
                            getActivity().finish();
                            break;
                        }
                    }


                } else {
                    startActivity(new Intent(getActivity(), DashboardActivity.class));
                    getActivity().finish();
                }


            }
        });
        gone_swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mTourGuideHandler.cleanUp();
                swipe.setVisibility(View.GONE);
                result = new DrawerBuilder()
                        .withActivity(getActivity())
                        .addDrawerItems(
                                new PrimaryDrawerItem().withName("Indian Vegetables ( Kg )").withIdentifier(0),
                                new PrimaryDrawerItem().withName("Grocery ( Kg )").withIdentifier(0),
                                new PrimaryDrawerItem().withName("Dairy Products ( Kg )").withIdentifier(0),
                                new PrimaryDrawerItem().withName("Fruit ( Dozen )").withIdentifier(0),
                                new PrimaryDrawerItem().withName("Leafy Vegetables ( Judi / Numbers )").withIdentifier(0)


                        )
                        .withActivity(getActivity())
                        .withDrawerGravity(Gravity.END)
                        .withRootView(relativeLayout)
                        .buildForFragment();

                result.getDrawerLayout().setFitsSystemWindows(true);
                result.getSlider().setFitsSystemWindows(true);


                result.openDrawer();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        result.closeDrawer();


                    }
                }, 2000);


                mTourGuideHandler = TourGuide.init(getActivity()).with(TourGuide.Technique.Click)
                        .setPointer(new Pointer().setColor(Color.RED))
                        .setToolTip(
                                new ToolTip()
                                        .setTitle("Click to buy")
                                        .setDescription("Add to Cart!")
                                        .setBackgroundColor(Color.parseColor("#4b4b4b"))
                                        .setShadow(true)
                        )
                        .setOverlay(new Overlay().disableClickThroughHole(false).setHoleRadius(150))
                        .playOn(btnClickMe1);

            }
        });


        gone_swipe2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTourGuideHandler.cleanUp();


                result1.openDrawer();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        SharedPreferenceConnector.writeBoolean(getActivity(), Master.INTRO, false);


                        if (getActivity().getIntent().hasExtra("redirectto")) {
                            String redirectto = getActivity().getIntent().getStringExtra("redirectto");
                            Intent i;
                            switch (redirectto) {

                                case "profile": {
                                    i = new Intent(getActivity(), ProfileActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                    break;
                                }
                                case "organisation": {

                                    i = new Intent(getActivity(), DashboardActivity.class);
                                    i.putExtra("redirectto", "organisation");
                                    startActivity(i);
                                    getActivity().finish();
                                    break;
                                }
                                case "dashboard": {
                                    i = new Intent(getActivity(), DashboardActivity.class);
                                    i.putExtra("redirectto", redirectto);
                                    startActivity(i);
                                    getActivity().finish();
                                    break;
                                }
                            }


                        } else {
                            startActivity(new Intent(getActivity(), DashboardActivity.class));
                            getActivity().finish();
                        }

                    }
                }, 1000);

                swipe2.setVisibility(View.GONE);


            }
        });


        Master.cartList = new ArrayList<>();

        setHasOptionsMenu(true);


        Material.alertDialog(getActivity(), getActivity().getString(R.string.alert_tutorial_alert), "OK");

        return productFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


    }

    @Override
    public void onResume() {
        super.onResume();

        Master.isProductClicked = false;


    }


    private void clickMe2(View view) {
        mTourGuideHandler.cleanUp();

        mTourGuideHandler.setPointer(new Pointer().setColor(Color.RED))
                .setToolTip(new ToolTip()
                                .setTitle("Quantity")
                                .setDescription("Product quantity added by customer")
                                .setBackgroundColor(Color.parseColor("#4b4b4b"))
                )
                .playOn(btnClickMe2);
    }

    private void clickMe3(View view) {
        mTourGuideHandler.cleanUp();

        new Pointer().setColor(Color.RED).setGravity(Gravity.RIGHT);
        mTourGuideHandler
                .setToolTip(new ToolTip()
                                .setTitle("Price")
                                .setDescription("Per unit cost of product")
                                .setGravity(Gravity.LEFT)
                                .setBackgroundColor(Color.parseColor("#4b4b4b"))
                )
                .playOn(btnClickMe3);

    }
}