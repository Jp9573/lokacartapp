package com.mobile.ict.cart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.FAQsAdapter;
import com.mobile.ict.cart.interfaces.RecyclerViewItemClickListener;

public class FAQFragment extends Fragment {

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_faq, container, false);
        getActivity().setTitle(R.string.title_fragment_faq);

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String[] FAQsCategory = getResources().getStringArray(R.array.category_faqs_titles);

        RecyclerView faqsRecyclerView = (RecyclerView) view.findViewById(R.id.faqs_recycler_view);

        RecyclerView.LayoutManager faqsLayoutManager = new LinearLayoutManager(getActivity());
        faqsRecyclerView.setLayoutManager(faqsLayoutManager);

        RecyclerView.Adapter faqsAdapter = new FAQsAdapter(FAQsCategory);
        faqsRecyclerView.setAdapter(faqsAdapter);

        faqsRecyclerView.addOnItemTouchListener(
                new RecyclerViewItemClickListener(getActivity(), faqsRecyclerView,new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(int position) {

                        ChangeFAQsDetail changeFAQsDetail = (ChangeFAQsDetail) getActivity();
                        changeFAQsDetail.openFAQDetailFragment(position);

                    }
                })
        );
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public interface ChangeFAQsDetail {
        void openFAQDetailFragment(int position);
    }
}
