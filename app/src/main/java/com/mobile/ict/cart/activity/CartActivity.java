package com.mobile.ict.cart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.CartAdapter;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.DeleteProductListener;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartActivity extends AppCompatActivity implements DeleteProductListener, GetResponse {

    private RecyclerView recyclerView;
    private LinearLayout cartAmountPayableLinearLayout, cartDeliveryChargesLinearLayout, cartItemTotalLinearLayout;
    private Button btnCheckout;
    private RelativeLayout emptyCartLinearLayout;
    private CartAdapter cartAdapter;
    private TextView cartTotal, deliveryCharges, amountPayable;
    private DBHelper dbHelper;
    private double sum = 0.0;
    private String response, comment;
    private ArrayList<Product> changedProductsList;
    private EditText placeOrderComment;
    private TextInputLayout txtInputComment;
    private AlertDialog paymentDialog;
    CharSequence[] values = {"Cash on delivery", "Online Payment - Paytm",};
    int paymentPosition = -1;
    double amount;
    String orderID, merchantID, customerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //AnalyticsActivity();

        Master.cartList = new ArrayList<>();

        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        cartAmountPayableLinearLayout = (LinearLayout) findViewById(R.id.cartAmountPayableLinearLayout);
        cartDeliveryChargesLinearLayout = (LinearLayout) findViewById(R.id.cartDeliveryChargesLinearLayout);
        cartItemTotalLinearLayout = (LinearLayout) findViewById(R.id.cartItemTotalLinearLayout);
        btnCheckout = (Button) findViewById(R.id.btnCheckout);


        emptyCartLinearLayout = (RelativeLayout) findViewById(R.id.cartEmptyRelativeLayout);
        emptyCartLinearLayout.setVisibility(View.GONE);
        RelativeLayout noUpdateRelativeLayout = (RelativeLayout) findViewById(R.id.noUpdateRelativeLayout);
        noUpdateRelativeLayout.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.rvCart);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);

        cartTotal = (TextView) findViewById(R.id.tcartTotal);
        deliveryCharges = (TextView) findViewById(R.id.tDeliveryCharges);
        amountPayable = (TextView) findViewById(R.id.tAmountPayable);

        placeOrderComment = (EditText) findViewById(R.id.etComment);

        txtInputComment = (TextInputLayout) findViewById(R.id.txtComment);


        dbHelper = DBHelper.getInstance(this);


        Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());


        if (Master.cartList.isEmpty()) {
            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();

            cartAmountPayableLinearLayout.setVisibility(View.GONE);
            cartDeliveryChargesLinearLayout.setVisibility(View.GONE);
            cartItemTotalLinearLayout.setVisibility(View.GONE);
            btnCheckout.setVisibility(View.GONE);
            emptyCartLinearLayout.setVisibility(View.VISIBLE);
            txtInputComment.setVisibility(View.GONE);
            placeOrderComment.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);

        } else {

            emptyCartLinearLayout.setVisibility(View.GONE);
            txtInputComment.setVisibility(View.VISIBLE);
            placeOrderComment.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            cartAmountPayableLinearLayout.setVisibility(View.VISIBLE);
            cartDeliveryChargesLinearLayout.setVisibility(View.VISIBLE);
            cartItemTotalLinearLayout.setVisibility(View.VISIBLE);
            btnCheckout.setVisibility(View.VISIBLE);

            Master.CART_ITEM_COUNT = Master.cartList.size();
            comment = SharedPreferenceConnector.readString(this, MemberDetails.getSelectedOrgAbbr() + " comment", "");

            placeOrderComment.setText(comment);

            invalidateOptionsMenu();

            for (int i = 0; i < Master.cartList.size(); ++i) {
                sum = sum + Master.cartList.get(i).getTotal();
            }

            DecimalFormat df = new DecimalFormat("0.00");

            int tot = (int) Math.round(sum);


            cartTotal.setText("" + df.format(tot));

            deliveryCharges.setText("" + Master.DELIVERY_CHARGES);


            tot = tot + Master.DELIVERY_CHARGES;


            amountPayable.setText("" + df.format(tot));

        }


        cartAdapter = new CartAdapter(this, cartTotal, deliveryCharges, amountPayable);
        recyclerView.setAdapter(cartAdapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    /*private void AnalyticsActivity() {
        LokacartApplication application = (LokacartApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        String name = "Cart";
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void AnalyticsEvent(String categoryId, String actionId) {
        Tracker t = ((LokacartApplication) getApplication()).getTracker(LokacartApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .build());
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        saveCart();
        finish();
        getSupportFragmentManager().popBackStack();
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        saveCart();
    }

    private void saveCart() {
        comment = placeOrderComment.getText().toString().trim();

        SharedPreferenceConnector.writeString(this, MemberDetails.getSelectedOrgAbbr() + " comment", comment);

        for (int i = 0; i < Master.cartList.size(); i++) {
            if (Master.cartList.get(i).getQuantity() == 0) {
                dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.cartList.get(i).getID());

                Master.CART_ITEM_COUNT--;


                for (int j = 0; j < Master.productList.size(); ++j) {
                    if (Master.productList.get(j).getID().equals(Master.cartList.get(i).getID())) {
                        Master.productList.get(j).setQuantity(0);
                        break;
                    }
                }
            } else {
                dbHelper.updateProduct
                        (
                                String.valueOf(Master.cartList.get(i).getUnitPrice()),
                                String.valueOf(Master.cartList.get(i).getQuantity()),
                                String.valueOf(Master.cartList.get(i).getTotal()),
                                String.valueOf(Master.cartList.get(i).getName()),
                                MemberDetails.getMobileNumber(),
                                MemberDetails.getSelectedOrgAbbr(),
                                String.valueOf(Master.cartList.get(i).getID()),
                                String.valueOf(Master.cartList.get(i).getImageUrl()),
                                String.valueOf(Master.cartList.get(i).getStockQuantity()),
                                Master.cartList.get(i).getStockEnabledStatus(),
                                String.valueOf(Master.cartList.get(i).getGst())
                        );

                for (int j = 0; j < Master.productList.size(); ++j) {
                    if (Master.productList.get(j).getID().equals(Master.cartList.get(i).getID())) {
                        Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                        break;
                    }
                }
            }
        }
    }


    private void showVerifiedEmailDialog() {
        String ver = SharedPreferenceConnector.readString(CartActivity.this.getApplicationContext(), Master.emailVerified, "1");
        if (ver.equals("0")) {
            if (SharedPreferenceConnector.readBoolean(this, Master.showEmailDialogPref, true)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.alert_email_verified));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.dialog_do_not_show_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferenceConnector.writeBoolean(CartActivity.this, Master.showEmailDialogPref, false);
                        checkOutFunc();

                    }
                });
                builder.setNegativeButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkOutFunc();

                    }
                });


                AlertDialog alert11 = builder.create();
                alert11.show();
            } else {
                checkOutFunc();

            }


        } else {

            checkOutFunc();


        }
    }

    private void checkOutFunc() {


        comment = placeOrderComment.getText().toString().trim();
        SharedPreferenceConnector.writeString(this, MemberDetails.getSelectedOrgAbbr() + " comment", comment);

        if (Master.PAYTM_STATUS == 1 || Master.PAYTM_STATUS == 2) {

            View paymentDialogView = getLayoutInflater().inflate(R.layout.dialog_payment, null);

            AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
            builder.setTitle(getResources().getString(R.string.dialog_title));
            builder.setView(paymentDialogView);
            builder.setCancelable(false);


            final TextView bankDetails = (TextView) paymentDialogView.findViewById(R.id.tbankdetails);


            final ScrollView bankDetailsLinearLayout = (ScrollView) paymentDialogView.findViewById(R.id.bankDetailsScrollView);

            final TextView tBankName = (TextView) paymentDialogView.findViewById(R.id.tBankName);
            final TextView tAccountNo = (TextView) paymentDialogView.findViewById(R.id.tAccountNo);

            final TextView tAccountName = (TextView) paymentDialogView.findViewById(R.id.tAccountName);

            final TextView tBankIfsc = (TextView) paymentDialogView.findViewById(R.id.tBankIfsc);

            final TextView tBankMicr = (TextView) paymentDialogView.findViewById(R.id.tBankMicr);

            final TextView tAccountType = (TextView) paymentDialogView.findViewById(R.id.tAccountType);

            final TextView tBankBranch = (TextView) paymentDialogView.findViewById(R.id.tBankBranch);


            RadioGroup radioGroupPayment = (RadioGroup) paymentDialogView.findViewById(R.id.rbgPayment);

            final RadioButton radioButtonCod = (RadioButton) paymentDialogView.findViewById(R.id.rbCod);

            if (Master.PAYTM_STATUS == 2)
                radioButtonCod.setText(getResources().getString(R.string.label_cod_bank));
            else
                radioButtonCod.setText(getResources().getString(R.string.label_cod));


            bankDetails.setVisibility(View.GONE);
            bankDetailsLinearLayout.setVisibility(View.GONE);

            radioGroupPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                    if (checkedId == R.id.rbCod) {
                        paymentPosition = 0;

                        if (Master.PAYTM_STATUS == 2) {
                            bankDetails.setVisibility(View.VISIBLE);
                            bankDetailsLinearLayout.setVisibility(View.VISIBLE);

                            tBankName.setText(Master.BANKNAME);
                            tAccountNo.setText(Master.ACCOUNTNO);
                            tAccountName.setText(Master.ACCOUNTNAME);
                            tBankIfsc.setText(Master.IFSC);
                            tBankMicr.setText(Master.MICR);
                            tAccountType.setText(Master.ACCOUNTTYPE);
                            tBankBranch.setText(Master.BRANCH);
                        } else {
                            bankDetails.setVisibility(View.GONE);
                            bankDetailsLinearLayout.setVisibility(View.GONE);

                        }
                    } else {
                        paymentPosition = 1;

                        if (Master.PAYTM_STATUS == 2) {
                            radioButtonCod.setText(getResources().getString(R.string.label_cod_bank));
                            bankDetails.setVisibility(View.GONE);
                            bankDetailsLinearLayout.setVisibility(View.GONE);
                        }


                    }
                }
            });


            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (paymentPosition != -1)

                    {

                        if (Master.isNetworkAvailable(CartActivity.this)) {
                            new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE, getString(R.string.pd_chk_internet_connection), CartActivity.this, "PlacingOrder_Internet").execute();

                        } else {
                            paymentPosition = -1;
                            Material.alertDialog(CartActivity.this, getString(R.string.toast_Please_check_internet_connection), getString(R.string.dialog_ok));
                        }

                    } else

                    {
                        paymentPosition = -1;
                        Toast.makeText(CartActivity.this, "Please select payment mode", Toast.LENGTH_LONG).show();
                    }

                }
            });


            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    paymentPosition = -1;
                    paymentDialog.dismiss();
                }
            });

            paymentDialog = builder.create();
            paymentDialog.show();

        } else {

            if (Master.isNetworkAvailable(CartActivity.this)) {
                new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE, getString(R.string.pd_chk_internet_connection), CartActivity.this, "PlacingOrder_Internet").execute();


            } else {
                Material.alertDialog(CartActivity.this, getString(R.string.toast_Please_check_internet_connection), getString(R.string.dialog_ok));

            }


        }
    }


    public void createOrder() {

        try {

            boolean qty = false;
            JSONObject order = new JSONObject();
            order.put("orgabbr", MemberDetails.getSelectedOrgAbbr());
            order.put("groupname", "Parent Group");

            if (comment.equals(""))
                order.put("comment", "NA");
            else
                order.put("comment", comment);

            JSONArray products = new JSONArray();
            JSONObject object;

            amount = 0.0;

            for (int i = 0; i < Master.cartList.size(); i++) {
                object = new JSONObject();
                object.put("name", Master.cartList.get(i).getName());
                object.put(Master.PRICE, Master.cartList.get(i).getUnitPrice());

                object.put("gst", Master.cartList.get(i).getGst());

                double itemTotal = Master.cartList.get(i).getTotal();


                if (itemTotal == 0.0) {
                    qty = true;
                    break;
                } else {
                    amount = amount + itemTotal;
                }

                object.put("quantity", Master.cartList.get(i).getQuantity());

                products.put(object);


            }

            amount = amount + Master.DELIVERY_CHARGES;

            order.put("orderItems", products);
            order.put("delCharges", Master.DELIVERY_CHARGES);


            if (!qty) {

                if (Master.PAYTM_STATUS == 1 || Master.PAYTM_STATUS == 2) {
                    switch (paymentPosition) {
                        case 0:

                            if (Master.PAYTM_STATUS == 1)
                                order.put("payMode", 0);
                            else
                                order.put("payMode", 1);

                            new NetworkAsyncTask
                                    (
                                            Master.getPlacingOrderURL(),
                                            "placingOrder",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_placing_your_order),
                                            this,
                                            order,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();
                            break;

                        case 1:
                            new NetworkAsyncTask
                                    (
                                            Master.getPaymentGatwayPlacingOrderURL(),
                                            "placingOrder",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_placing_your_order),
                                            this,
                                            order,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();

                            break;

                        default:
                            break;
                    }
                } else {

                    order.put("payMode", 0);

                    new NetworkAsyncTask
                            (
                                    Master.getPlacingOrderURL(),
                                    "placingOrder",
                                    Master.DIALOG_TRUE,
                                    getString(R.string.pd_placing_your_order),
                                    this,
                                    order,
                                    Master.POST,
                                    Master.AUTH_TRUE,
                                    MemberDetails.getEmail(),
                                    MemberDetails.getPassword()
                            ).execute();
                }


            } else {
                paymentPosition = -1;
                Material.alertDialog(this, getString(R.string.alert_please_insert_quantity), getString(R.string.dialog_ok));
            }

        } catch (Exception e) {
            paymentPosition = -1;

            e.printStackTrace();
        }

    }


    public void onlinePayment() {


        DecimalFormat df = new DecimalFormat("0.00");

        int tot = (int) Math.round(amount);


        PaytmPGService Service = PaytmPGService.getProductionService();

        Map<String, String> paramMap = new HashMap<String, String>();


        paramMap.put("ORDER_ID", orderID);
        paramMap.put("MID", merchantID);
        paramMap.put("CUST_ID", customerID);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail109");
        paramMap.put("WEBSITE", "ITAAKAWAP");


        paramMap.put("TXN_AMOUNT", "" + df.format(tot));
        paramMap.put("THEME", "merchant");
        paramMap.put("EMAIL", MemberDetails.getEmail());
        paramMap.put("MOBILE_NO", MemberDetails.getMobileNumber());
        PaytmOrder Order = new PaytmOrder(paramMap);


        PaytmMerchant Merchant = new PaytmMerchant(
                "https://best-erp.com/extras/paytm/paytmCheckSumGenerator.jsp",
                "https://best-erp.com/extras/paytm/paytmCheckSumVerify.jsp");

        Service.initialize(Order, Merchant, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        // Some UI Error Occurred in Payment Gateway Activity.
                        // // This may be due to initialization of views in
                        // Payment Gateway Activity or may be due to //
                        // initialization of webview. // Error Message details
                        // the error occurred.


                        Toast.makeText(getApplicationContext(), inErrorMessage, Toast.LENGTH_LONG).show();

                        deletePGOrder();

                    }

                    @Override
                    public void onTransactionSuccess(Bundle inResponse) {
                        // After successful transaction this method gets called.
                        // // Response bundle contains the merchant response
                        // parameters.


                        Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();

                        onConfirmPG(inResponse);
                    }

                    @Override
                    public void onTransactionFailure(String inErrorMessage,
                                                     Bundle inResponse) {
                        // This method gets called if transaction failed. //
                        // Here in this case transaction is completed, but with
                        // a failure. // Error Message describes the reason for
                        // failure. // Response bundle contains the merchant
                        // response parameters.


                        Toast.makeText(getApplicationContext(), inErrorMessage, Toast.LENGTH_LONG).show();
                        deletePGOrder();

                    }

                    @Override
                    public void networkNotAvailable() { // If network is not
                        // available, then this
                        // method gets called.
                        Toast.makeText(getApplicationContext(), "Network not available", Toast.LENGTH_LONG).show();

                        deletePGOrder();


                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.


                        Toast.makeText(getApplicationContext(), inErrorMessage, Toast.LENGTH_LONG).show();

                        deletePGOrder();

                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {


                        Toast.makeText(getApplicationContext(), inErrorMessage, Toast.LENGTH_LONG).show();

                        deletePGOrder();

                    }

                    @Override
                    public void onBackPressedCancelTransaction() {

                        // TODO Auto-generated method stub
                        deletePGOrder();
                    }

                });

    }


    public void onConfirmPG(Bundle bundle) {
        try {
            String net = bundle.getString("TXNAMOUNT");
            String txnId = bundle.getString("TXNID");
            JSONObject order = new JSONObject();
            order.put("orderId", "" + orderID);
            order.put("net", "" + net);
            order.put("txnId", txnId);

            new NetworkAsyncTask
                    (
                            Master.getConfirmPGOrderURL(),
                            "getConfirmPGOrder",
                            Master.DIALOG_TRUE,
                            getString(R.string.pd_sending_data_to_server),
                            this,
                            order,
                            Master.POST,
                            Master.AUTH_TRUE,
                            MemberDetails.getEmail(),
                            MemberDetails.getPassword()
                    ).execute();

        } catch (Exception e) {

        }


    }


    public void deletePGOrder() {

        try {
            JSONObject delObj = new JSONObject();
            delObj.put("org", MemberDetails.getSelectedOrgAbbr());
            delObj.put("orderId", "" + orderID);

            new NetworkAsyncTask
                    (
                            Master.getDeletePGOrderURL(),
                            "getDeletePGOrder",
                            Master.DIALOG_FALSE,
                            null,
                            this,
                            delObj,
                            Master.POST,
                            Master.AUTH_TRUE,
                            MemberDetails.getEmail(),
                            MemberDetails.getPassword()
                    ).execute();

        } catch (Exception e) {

        }

    }


    public void removeCartItems(String orderid) {
        SharedPreferenceConnector.writeString(this, MemberDetails.getSelectedOrgAbbr() + " comment", "");

        paymentPosition = -1;

        try {
            /*for (int i = 0; i < Master.cartList.size(); i++)
            {
                AnalyticsOrder(Master.cartList.get(i).getName(), Master.cartList.get(i).getUnitPrice(), Master.cartList.get(i).getQuantity());

                AnalyticsEvent("Conversion", "Checkout");

            }*/

            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();


            for (int i = 0; i < Master.cartList.size(); i++) {

                for (int j = 0; j < Master.productList.size(); ++j) {
                    if (Master.productList.get(j).getID().equals(Master.cartList.get(i).getID())) {
                        Master.productList.get(j).setQuantity(0);

                        break;
                    }
                }
            }

            for (int k = 0; k < Master.cartList.size(); ++k) {
                for (int i = 0; i < Master.productTypeList.size(); ++i) {
                    for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j) {
                        if (Master.productTypeList.get(i).productItems.get(j).getID().equals(Master.cartList.get(k).getID())) {
                            Master.productTypeList.get(i).productItems.get(j).setQuantity(0);

                            break;
                        }
                    }
                }
            }

            Master.cartList.clear();

            dbHelper.deleteCart(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());

            Intent i = new Intent(CartActivity.this, OrderSubmitActivity.class);
            i.putExtra("orderid", orderid);
            startActivity(i);
            finish();

        } catch (Exception e) {

        }


    }


    public void onCheckOut(View v) {
        showVerifiedEmailDialog();

    }

   /* private void AnalyticsOrder(String analyticsProductName, double analyticsProductPrice, int analyticsProductQuantity)
    {
        com.google.android.gms.analytics.ecommerce.Product analyticsProduct = new com.google.android.gms.analytics.ecommerce.Product()
                .setName(analyticsProductName)
                .setPrice(analyticsProductPrice)
                .setQuantity(analyticsProductQuantity);

        ProductAction productAction = new ProductAction(ProductAction.ACTION_CLICK)
                .setProductActionList("CartActivity");
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addProduct(analyticsProduct)
                .setProductAction(productAction);

        Tracker t = ((LokacartApplication) getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        t.setScreenName("Screen~CartActivity");
        t.enableAdvertisingIdCollection(true);
        t.send(builder.build());

        ProductAction productAction_1 = new ProductAction(ProductAction.ACTION_CHECKOUT)
                .setCheckoutStep(2)
                .setCheckoutOptions("Checkout Cart");
        HitBuilders.ScreenViewBuilder builder_1 = new HitBuilders.ScreenViewBuilder()
                .addProduct(analyticsProduct)
                .setProductAction(productAction_1);

        Tracker t1 = ((LokacartApplication) getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        t1.setScreenName("checkoutStep3");
        t1.send(builder_1.build());
    }*/

    @Override
    public void deleteProduct(final int position, final String productID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.alert_do_you_really_want_to_remove));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        for (int j = 0; j < Master.productList.size(); ++j) {
                            if (Master.productList.get(j).getID().equals(productID)) {
                                Master.productList.get(j).setQuantity(0);
                                //   AnalyticsEvent("Impression", "ProductDeleted");
                                break;
                            }
                        }

                        Master.cartList.remove(position);
                        cartAdapter.notifyItemRemoved(position);
                        cartAdapter.notifyDataSetChanged();
                        dialog.dismiss();


                        dbHelper.deleteProduct(MemberDetails.getMobileNumber(), productID);

                        if (Master.cartList.isEmpty()) {
                            Master.CART_ITEM_COUNT = 0;
                            cartAmountPayableLinearLayout.setVisibility(View.GONE);
                            cartDeliveryChargesLinearLayout.setVisibility(View.GONE);
                            cartItemTotalLinearLayout.setVisibility(View.GONE);

                            btnCheckout.setVisibility(View.GONE);


                            emptyCartLinearLayout.setVisibility(View.VISIBLE);

                            txtInputComment.setVisibility(View.GONE);
                            placeOrderComment.setVisibility(View.GONE);

                            recyclerView.setVisibility(View.GONE);

                        } else {
                            Master.CART_ITEM_COUNT--;

                            sum = 0.0;
                            for (int i = 0; i < Master.cartList.size(); ++i) {
                                sum = sum + Master.cartList.get(i).getTotal();
                            }

                            DecimalFormat df = new DecimalFormat("0.00");

                            int tot = (int) Math.round(sum);

                            tot = tot + Master.DELIVERY_CHARGES;


                            cartTotal.setText("" + df.format(tot));


                            deliveryCharges.setText("" + Master.DELIVERY_CHARGES);

                            tot = tot + Master.DELIVERY_CHARGES;


                            amountPayable.setText("" + df.format(tot));


                        }
                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sum = 0.0;
                        for (int i = 0; i < Master.cartList.size(); ++i) {
                            sum = sum + Master.cartList.get(i).getTotal();
                        }

                        DecimalFormat df = new DecimalFormat("0.00");

                        int tot = (int) Math.round(sum);

                        cartTotal.setText("" + df.format(tot));

                        deliveryCharges.setText("" + Master.DELIVERY_CHARGES);


                        tot = tot + Master.DELIVERY_CHARGES;


                        amountPayable.setText("" + df.format(tot));


                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();


    }

    @Override
    protected void onResume() {
        super.onResume();

        //AnalyticsActivity();

        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

    }


    @Override
    public void getData(String s, String tag) {
        switch (tag) {
            case "PlacingOrder_Internet":

                if (s.equals("success")) {


                    createOrder();


                } else {
                    paymentPosition = -1;
                    Material.alertDialog(this, getString(R.string.toast_Please_check_internet_connection), getString(R.string.dialog_ok));

                }

                break;

            case "placingOrder":


                if (s.equals("exception")) {
                    paymentPosition = -1;
                    Material.alertDialog(CartActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), getString(R.string.dialog_ok));

                } else {

                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(s);
                        String val = jsonObj.getString("status");

                        switch (val) {

                            case "Success":

                                if (Master.PAYTM_STATUS == 1 || Master.PAYTM_STATUS == 2) {
                                    switch (paymentPosition) {
                                        case 0:
                                            orderID = jsonObj.getString("orderId");
                                            removeCartItems(orderID);
                                            break;

                                        case 1:

                                            paymentPosition = -1;

                                            orderID = jsonObj.getString("orderId");
                                            merchantID = jsonObj.getString("merchantId");
                                            customerID = jsonObj.getString("userId");


                                            onlinePayment();
                                            break;

                                        default:
                                            break;
                                    }
                                } else {
                                    orderID = jsonObj.getString("orderId");
                                    removeCartItems(orderID);
                                }

                                break;
                            case "Failure": {
                                paymentPosition = -1;
                                String error[] = {jsonObj.getString("error")};
                                if (error[0].equals("You are no longer a member")) {

                                    AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                                    builder.setCancelable(false);
                                    builder.setMessage(error[0] + " of " + MemberDetails.getSelectedOrgName());
                                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                                    AlertDialog changeMobileNumberAlert = builder.create();
                                    changeMobileNumberAlert.show();

                                    Button bPositive = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_POSITIVE);

                                    bPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Master.isMember = false;
                                            dbHelper.deleteCart(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
                                            finish();
                                        }
                                    });
                                } else {
                                    Material.alertDialog(CartActivity.this, error[0], getString(R.string.button_ok));
                                }
                                break;
                            }
                            case "GST/price changed":
                                paymentPosition = -1;
                                parseChangedProducts(jsonObj);
                                cartAdapter.notifyDataSetChanged();
                                Material.alertDialog(CartActivity.this, val + ". " + getString(R.string.toast_price_of_some_products_has_changed), getString(R.string.dialog_ok));
                                break;


                            case "del changed":
                                paymentPosition = -1;

                                Master.DELIVERY_CHARGES = Integer.parseInt(jsonObj.getString("delCharges"));

                                cartAdapter.notifyDataSetChanged();
                                Material.alertDialog(CartActivity.this, getString(R.string.toast_delivery_charges_changed) + ". " + getString(R.string.toast_price_of_some_products_has_changed), getString(R.string.dialog_ok));
                                break;


                            case "products has been disabled": {
                                paymentPosition = -1;

                                String error[] = {jsonObj.getString("error")};

                                Material.alertDialog(CartActivity.this, error[0], getString(R.string.button_ok));

                                break;
                            }
                            default:
                                paymentPosition = -1;
                                Toast.makeText(CartActivity.this, R.string.toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        paymentPosition = -1;

                        e.printStackTrace();
                    }
                }
                break;


            case "getMerchantDetails":

                if (s.equals("exception")) {
                    Material.alertDialog(CartActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), getString(R.string.dialog_ok));

                } else {
                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(s);
                        String val = jsonObj.getString("status");

                        switch (val) {

                            case "Success":

                                orderID = jsonObj.getString("orderId");
                                merchantID = jsonObj.getString("merchantId");
                                customerID = jsonObj.getString("userId");


                                onlinePayment();


                                break;
                            case "Failure": {
                                String error[] = {jsonObj.getString("error")};
                                if (error[0].equals("You are no longer a member")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                                    builder.setCancelable(false);
                                    builder.setMessage(error[0] + " of " + MemberDetails.getSelectedOrgName());
                                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                                    AlertDialog changeMobileNumberAlert = builder.create();
                                    changeMobileNumberAlert.show();

                                    Button bPositive = changeMobileNumberAlert.getButton(AlertDialog.BUTTON_POSITIVE);
                                    bPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Master.isMember = false;
                                            dbHelper.deleteCart(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
                                            finish();
                                        }
                                    });
                                } else {
                                    Material.alertDialog(CartActivity.this, error[0], getString(R.string.button_ok));
                                }
                                break;
                            }
                            case "GST/price changed":
                                paymentPosition = -1;
                                parseChangedProducts(jsonObj);
                                cartAdapter.notifyDataSetChanged();
                                Material.alertDialog(CartActivity.this, val + ". " + getString(R.string.toast_price_of_some_products_has_changed), getString(R.string.dialog_ok));
                                break;


                            case "del changed":
                                paymentPosition = -1;

                                Master.DELIVERY_CHARGES = Integer.parseInt(jsonObj.getString("delCharges"));

                                cartAdapter.notifyDataSetChanged();
                                Material.alertDialog(CartActivity.this, getString(R.string.toast_delivery_charges_changed) + ". " + getString(R.string.toast_price_of_some_products_has_changed), getString(R.string.dialog_ok));
                                break;

                            case "products has been disabled": {
                                String error[] = {jsonObj.getString("error")};

                                Material.alertDialog(CartActivity.this, error[0], getString(R.string.button_ok));

                                break;
                            }
                            default:
                                Toast.makeText(CartActivity.this, R.string.toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                break;


            case "getConfirmPGOrder":


                if (s.equals("exception")) {
                    Material.alertDialog(CartActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), getString(R.string.dialog_ok));

                } else {
                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(s);
                        String val = jsonObj.getString("status");

                        switch (val) {

                            case "Success":

                                removeCartItems(orderID);

                                break;

                            default:
                                Toast.makeText(CartActivity.this, R.string.toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                break;


            case "getDeletePGOrder":


                if (s.equals("exception")) {
                    Material.alertDialog(CartActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), getString(R.string.dialog_ok));

                } else {


                    JSONObject jsonObj;
                    try {
                        jsonObj = new JSONObject(s);
                        String val = jsonObj.getString("status");

                        switch (val) {

                            case "Success":
                                break;

                            default:
                                Toast.makeText(CartActivity.this, R.string.toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            default:
                break;
        }

    }

    private void parseChangedProducts(JSONObject object) {
        changedProductsList = new ArrayList<>();
        Product product;
        try {


            JSONArray array = object.getJSONArray("products");

            for (int x = 0; x < array.length(); ++x) {
                JSONObject productObject = array.getJSONObject(x);

                if (productObject.has("imageUrl") && productObject.has("audioUrl")) {
                    if (productObject.getString("imageUrl") == null &&
                            productObject.getString("audioUrl") == null) {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"), "null", "null",
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    } else if (productObject.getString("imageUrl") == null) {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                "null",
                                productObject.getString("audioUrl"),
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    } else if (productObject.getString("audioUrl") == null) {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                productObject.getString("imageUrl"),
                                "null",
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    } else {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                productObject.getString("imageUrl"),
                                productObject.getString("audioUrl"),
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    }
                } else if (productObject.has("imageUrl")) {
                    if (productObject.getString("imageUrl") == null) {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                "null",
                                "null",
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    } else {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                productObject.getString("imageUrl"),
                                "null",
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    }
                } else if (productObject.has("audioUrl")) {
                    if (productObject.getString("audioUrl") == null) {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                "null",
                                "null",
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    } else {
                        product = new Product(productObject.getString("name"),
                                Double.parseDouble(productObject.getString("unitRate")),
                                Integer.parseInt(productObject.getString("quantity")),
                                productObject.getString("stockManagement"),
                                "null",
                                productObject.getString("audioUrl"),
                                productObject.getString("id"),
                                productObject.getString("description"),
                                Integer.parseInt(productObject.getString("gst")));
                    }
                } else {
                    product = new Product(productObject.getString("name"),
                            Double.parseDouble(productObject.getString("unitRate")),
                            Integer.parseInt(productObject.getString("quantity")),
                            productObject.getString("stockManagement"),
                            "null",
                            "null",
                            productObject.getString("id"),
                            productObject.getString("description"),
                            Integer.parseInt(productObject.getString("gst")));
                }
                changedProductsList.add(product);
            }

            updateCart();
            Master.updateProductList();
        } catch (JSONException ignored) {
        }
    }

    private void updateCart() {
        for (int i = 0; i < changedProductsList.size(); ++i) {
            for (int j = 0; j < Master.cartList.size(); ++j) {
                if (Master.cartList.get(j).getID().equals(changedProductsList.get(i).getID())) {
                    Master.cartList.get(j).setName(changedProductsList.get(i).getName());
                    Master.cartList.get(j).setUnitPrice(changedProductsList.get(i).getUnitPrice());
                    Master.cartList.get(j).setImageUrl(changedProductsList.get(i).getImageUrl());
                    Master.cartList.get(j).setStockQuantity(changedProductsList.get(i).getStockQuantity());
                    Master.cartList.get(j).setGst(changedProductsList.get(i).getGst());

                    Master.cartList.get(j).setStockEnabledStatus(changedProductsList.get(i).getStockEnabledStatus());

                    String total = String.format("%.2f", changedProductsList.get(i).getUnitPrice() * Master.cartList.get(j).getQuantity());

                    Master.cartList.get(j).setTotal(Double.parseDouble(total));

                    dbHelper.updateProduct(
                            String.valueOf(Master.cartList.get(j).getUnitPrice()),
                            String.valueOf(Master.cartList.get(j).getQuantity()),
                            String.valueOf(Master.cartList.get(j).getTotal()),
                            String.valueOf(Master.cartList.get(j).getName()),
                            MemberDetails.getMobileNumber(),
                            MemberDetails.getSelectedOrgAbbr(),
                            String.valueOf(Master.cartList.get(j).getID()),
                            String.valueOf(Master.cartList.get(j).getImageUrl()),
                            String.valueOf(Master.cartList.get(j).getStockQuantity()),
                            Master.cartList.get(j).getStockEnabledStatus(),
                            String.valueOf(Master.cartList.get(i).getGst())
                    );
                    break;
                }
            }
        }
    }
}
