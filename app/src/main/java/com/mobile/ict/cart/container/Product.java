package com.mobile.ict.cart.container;



public class Product {

    private String audioUrl;
    private double unitPrice;
    private double total;
    private int quantity,gst;
    private String stockEnabledStatus;
    private int stockQuantity;
    private String name;

    public int getGst() {
        return gst;
    }

    public void setGst(int gst) {
        this.gst = gst;
    }

    private String imageUrl;
    private String ID;
    private String desc;

    public String getDesc() {
        return desc;
    }


    @SuppressWarnings("EmptyMethod")
    public void setOrgAbbr(String orgAbbr) {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAudioUrl() {
        return audioUrl;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStockEnabledStatus() {
        return stockEnabledStatus;
    }

    public void setStockEnabledStatus(String stockEnabledStatus) {
        this.stockEnabledStatus = stockEnabledStatus;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product(String name, double unitPrice, int stockQuantity, String stockEnabledStatus, String imageUrl, String audioUrl, String ID, String desc,int gst) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = 0.0;
        this.quantity = (int) (0.0 / unitPrice);
        this.stockQuantity = stockQuantity;
        this.stockEnabledStatus = stockEnabledStatus;
        this.imageUrl = imageUrl;
        this.audioUrl = audioUrl;
        this.ID = ID;
        this.desc = desc;
        this.gst= gst;
    }

    public Product() {

    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
