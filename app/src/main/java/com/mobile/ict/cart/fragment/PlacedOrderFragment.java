package com.mobile.ict.cart.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.PlacedOrderAdapter;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class PlacedOrderFragment extends Fragment implements EditDeletePlacedOrderInterface , GetResponse{

    private View placedOrderFragmentView;
    private SwipeRefreshLayout swipeContainer;
    private LinearLayout  emptyCartLinearLayout;
    private List<Orders> ordersList;
    private AlertDialog feedbackDialog;
    private RecyclerView mRecyclerView;
    private PlacedOrderAdapter mAdapter;
    private RelativeLayout noDataRelativeLayout,noInternetRelativeLayout;
    private int optionIndex=-1;
    private EditText otherReason;
    private String options="";
    private String comments;
    private String otherReasonText="";

    private int pos;
    private int orderId;
    private int lastpos=0;
    private DBHelper dbHelper;

    LayoutInflater _inflater;
    ViewGroup _container;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.title_fragment_placed_order);
        _inflater=inflater;
        _container=container;

        if(!Master.isNetworkAvailable(getActivity()))
        {
            placedOrderFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        }
        else
        {
            dbHelper = DBHelper.getInstance(getActivity());

            placedOrderFragmentView = inflater.inflate(R.layout.fragment_placed_order, container, false);

            mRecyclerView = (RecyclerView) placedOrderFragmentView.findViewById(R.id.rvOrder);
            emptyCartLinearLayout = (LinearLayout) placedOrderFragmentView.findViewById(R.id.cartEmptyLinearLayout);
            emptyCartLinearLayout.setVisibility(View.GONE);
            noDataRelativeLayout = (RelativeLayout) placedOrderFragmentView.findViewById(R.id.noDataRelativeLayout);
            noDataRelativeLayout.setVisibility(View.GONE);
            noInternetRelativeLayout = (RelativeLayout) placedOrderFragmentView.findViewById(R.id.noInternetRelativeLayout);
            noInternetRelativeLayout.setVisibility(View.GONE);

            swipeContainer = (SwipeRefreshLayout) placedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
            {
                @Override
                public void onRefresh()
                {

                    new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE, getString(R.string.pd_chk_internet_connection),PlacedOrderFragment.this,"PlacedOrder_Internet" ).execute();


                }
            });
            swipeContainer.setColorSchemeResources(
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light,
                    android.R.color.holo_blue_bright);

        }


        return placedOrderFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(Master.isNetworkAvailable(getActivity()))
        {
            if(mRecyclerView != null)
            {

                new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,getString(R.string.pd_chk_internet_connection),PlacedOrderFragment.this,"PlacedOrder_Internet" ).execute();



            }
        }
    }



    private List<Orders> getOrders(JSONArray jsonArray) {

        ordersList = new ArrayList<>();

        int size= jsonArray.length();

        if(size!=0)

        {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyCartLinearLayout.setVisibility(View.GONE);
            noDataRelativeLayout.setVisibility(View.GONE);
            noInternetRelativeLayout.setVisibility(View.GONE);

            for(int i=0;i<size;i++)
            {

                try
                {
                     Orders orders = new Orders((JSONObject) jsonArray.get(i), i, Master.PLACEDORDER);
                     orders.getPlacedOrderItemsList(i);
                    ordersList.add(orders);
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }


            }

        }
        else
        {

            emptyCartLinearLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            noDataRelativeLayout.setVisibility(View.GONE);
            noInternetRelativeLayout.setVisibility(View.GONE);


        }

        return ordersList;



    }


    @Override
    public void deletePlacedOrder()
    {

        openFeedBackDialog(orderId);
    }




    private void openFeedBackDialog(final int orderId)
    {
        options="";
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.cancel_placed_order_feedback, (ViewGroup)placedOrderFragmentView. findViewById(R.id.cancel_order_feedback), false);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(Layout);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        feedbackDialog = builder.create();

        feedbackDialog.show();

        final ListView lv = (ListView)Layout.findViewById(R.id.lv1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        otherReason =(EditText)Layout.findViewById(R.id.comments);
        otherReason.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals(""))
                    otherReasonText=s.toString();
                else
                    otherReasonText="";
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        final ArrayList<HashMap<String, Object>> m_data = new ArrayList<>();

        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("option",getResources().getString(R.string.label_cancelorder_feedback_form_option1));
        m_data.add(map1);

        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option2));// no small text of this item!
        m_data.add(map2);

        HashMap<String, Object> map3 = new HashMap<>();
        map3.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option3));
        m_data.add(map3);

        HashMap<String, Object> map4 = new HashMap<>();
        map4.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option4));
        m_data.add(map4);

        HashMap<String, Object> map5 = new HashMap<>();
        map5.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option5));
        m_data.add(map5);

        for (HashMap<String, Object> m :m_data)
            m.put("checked", false);

        final SimpleAdapter adapter = new SimpleAdapter(getActivity(),
                m_data,
                R.layout.cancel_placed_order_feedback_item,
                new String[] {"option","checked"},
                new int[] {R.id.options, R.id.rb_choice});

        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {

                view.setVisibility(View.VISIBLE);
                return false;
            }

        });


        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
                RadioButton rb = (RadioButton) v.findViewById(R.id.rb_choice);

                if (m_data.get(arg2).get("option").equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5))) {
                    otherReason.setVisibility(View.VISIBLE);
                } else {
                    if (otherReason.isShown()) {
                        otherReason.setVisibility(View.GONE);
                        otherReason.setError(null);

                    }

                }

                options = m_data.get(arg2).get("option").toString();

                optionIndex = arg2;

                if (!rb.isChecked()) {
                    for (HashMap<String, Object> m : m_data)
                        m.put("checked", false);
                    m_data.get(arg2).put("checked", true);
                    adapter.notifyDataSetChanged();
                }
            }
        });


        feedbackDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (options.equals("")) {
                    Toast.makeText(getActivity(),R.string.label_toast_cancel_order_feedback_form, Toast.LENGTH_LONG).show();

                } else {
                    if(options.equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5)))
                    {
                        if(!otherReason.getText().toString().trim().equals(""))
                        {
                            comments=otherReason.getText().toString().replaceAll("\\n", " ");
                            otherReasonText=comments;
                            new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                                    getString(R.string.pd_chk_internet_connection),PlacedOrderFragment.this,"CancelOrder_Internet" ).execute();

                        }
                        else
                        {
                            Validation.hasText(getActivity(), otherReason);
                        }
                    }
                    else
                    {
                        comments=options;


                        new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                                getString(R.string.pd_chk_internet_connection),PlacedOrderFragment.this,"CancelOrder_Internet" ).execute();


                    }
                }
            }
        });

        feedbackDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherReasonText = "";
                feedbackDialog.dismiss();
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (ordersList != null)
        {

            ordersList.clear();
        }



    }

    @Override
    public void getData(String response, String tag) {

        if(PlacedOrderFragment.this.isAdded())
        {

            switch(tag)
            {
                case "PlacedOrder_Internet":
                    if(response.equals("success"))
                    {

                        new NetworkAsyncTask
                                (
                                        Master.getPlacedOrderURL(MemberDetails.getSelectedOrgAbbr(), MemberDetails.getMobileNumber()),
                                        "getPlacedOrders",
                                        Master.DIALOG_TRUE,
                                        getString(R.string.pd_fetching_orders),
                                        PlacedOrderFragment.this,
                                        null,
                                        Master.GET,
                                        Master.AUTH_TRUE,
                                        MemberDetails.getEmail(),
                                        MemberDetails.getPassword()
                                ).execute();

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                return false;
                            }
                        });

                        mRecyclerView.setLayoutManager(mLayoutManager);
                    }
                    else
                    {

                        noInternetRelativeLayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        noDataRelativeLayout.setVisibility(View.GONE);
                        swipeContainer.setRefreshing(false);


                    }
                    break;


                case "CancelOrder_Internet":
                    if(response.equals("success"))
                    {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("status","cancelled");
                            obj.put("comments",comments);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        feedbackDialog.dismiss();

                        new NetworkAsyncTask
                                (
                                        Master.getCancellingOrderURL(String.valueOf(orderId)),
                                        "cancelOrders",
                                        Master.DIALOG_TRUE,
                                        getString(R.string.pd_fetching_orders),
                                        PlacedOrderFragment.this,
                                        obj,
                                        Master.POST,
                                        Master.AUTH_TRUE,
                                        MemberDetails.getEmail(),
                                        MemberDetails.getPassword()
                                ).execute();

                     /*   Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                                LokacartApplication.TrackerName.APP_TRACKER);
                        t.send(new HitBuilders.EventBuilder()
                                .setCategory("Cancellation")
                                .setAction("OrderCanceled")
                                .build());*/
                    }
                    else
                    {
                        noInternetRelativeLayout.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        noDataRelativeLayout.setVisibility(View.GONE);
                        swipeContainer.setRefreshing(false);

                    }
                    break;



                case "getPlacedOrders":

                    {
                        if(response.equals("exception"))
                        {
                            mRecyclerView.setVisibility(View.GONE);
                            emptyCartLinearLayout.setVisibility(View.GONE);
                            noInternetRelativeLayout.setVisibility(View.GONE);

                            noDataRelativeLayout.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            try {

                                JSONObject orderList = new JSONObject(response);
                                JSONArray ordersArray = orderList.getJSONArray("orders");


                                ordersList = getOrders(ordersArray);
                                mAdapter= new PlacedOrderAdapter(getActivity(), ordersList,PlacedOrderFragment.this,Master.PLACEDORDER);
                                mRecyclerView.setHasFixedSize(true);
                                mRecyclerView.setAdapter(mAdapter);

                                mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {

                                    @Override
                                    public void onListItemExpanded(int position) {

                                        List<? extends ParentListItem> parentItemList = mAdapter.getParentItemList();
                                        if(lastpos!=position)
                                        {
                                            mAdapter.collapseParent(lastpos);
                                        }
                                        mRecyclerView.smoothScrollToPosition(position + parentItemList.get(position).getChildItemList().size());
                                        lastpos=position;
                                        pos=position;
                                        orderId= ordersList.get(position).getOrder_id();

                                    }

                                    @Override
                                    public void onListItemCollapsed(int position) {

                                    }
                                });


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                mRecyclerView.setVisibility(View.GONE);
                                emptyCartLinearLayout.setVisibility(View.GONE);
                                noDataRelativeLayout.setVisibility(View.VISIBLE);

                            }
                            swipeContainer.setRefreshing(false);
                        }
                    }

                    break;

                case "cancelOrders":

                    {
                        if(response.equals("exception"))
                        {
                            mRecyclerView.setVisibility(View.GONE);
                            emptyCartLinearLayout.setVisibility(View.GONE);
                            noDataRelativeLayout.setVisibility(View.VISIBLE);
                            noInternetRelativeLayout.setVisibility(View.GONE);

                        }
                        else {
                            JSONObject jsonObj;
                            String val;
                            try {
                                jsonObj = new JSONObject(response);
                                val=jsonObj.getString("status");

                                switch (val) {
                                    case "Success":

                                        Toast.makeText(getActivity(), R.string.label_toast_Your_order_has_been_cancelled_successfully, Toast.LENGTH_LONG).show();

                                        mAdapter.collapseParent(pos);
                                        ordersList.remove(pos);
                                        mAdapter.notifyParentItemRemoved(pos);
                                        mAdapter.notifyDataSetChanged();


                                        if (ordersList.isEmpty()) {
                                            emptyCartLinearLayout.setVisibility(View.VISIBLE);
                                            mRecyclerView.setVisibility(View.GONE);
                                            noInternetRelativeLayout.setVisibility(View.GONE);

                                            noDataRelativeLayout.setVisibility(View.GONE);
                                        }


                                        break;
                                    case "error":
                                        String error[] = {jsonObj.getString("error")};
                                        Toast.makeText(getActivity(), error[0], Toast.LENGTH_LONG).show();
                                        break;
                                    default:
                                        Toast.makeText(getActivity(), R.string.toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_LONG).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }

                    break;

                default : break;
            }

        }


    }



}
