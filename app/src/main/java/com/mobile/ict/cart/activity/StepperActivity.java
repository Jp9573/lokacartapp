package com.mobile.ict.cart.activity;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.fragment.SampleSlide;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;


public class StepperActivity extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(SampleSlide.newInstance(R.layout.stepper_1));
        addSlide(SampleSlide.newInstance(R.layout.stepper_2));
        addSlide(SampleSlide.newInstance(R.layout.stepper_3));

        setProgressButtonEnabled(true);

        setVibrate(false);
        setVibrateIntensity(30);
    }

    @Override
    public void onDonePressed() {
        SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);
        String redirectto = getIntent().getStringExtra("redirectto");
        Bundle extras = getIntent().getExtras();
        if(extras.containsKey("showintro")){


            if(extras.getString("showintro").equals("true")&&!redirectto.equals("profile"))
            {
                Intent i = new Intent(this, DemoLists.class);
                i.putExtra("redirectto" , redirectto);
                startActivity(i);
                finish();


            }else

            {
                goToActivity(redirectto);

            }

        }else {
            goToActivity(redirectto);
        }
    }

    private void goToActivity(String nextPage) {
        Intent i;
        switch(nextPage){
            case "profile":{

                i = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case "organisation":{

                i = new Intent(getApplicationContext(), DashboardActivity.class);
                i.putExtra("redirectto" , "organisation");
                startActivity(i);
                finish();
                break;
            }
            case "dashboard":{
                i = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(i);
                finish();
                break;
            }
            case "talktous":{
                i = new Intent(getApplicationContext(), LandingActivity.class);
                startActivity(i);
                finish();
                break;
            }

        }
    }
    @Override
    public void onSlideChanged() {
    }

    @Override
    public void onNextPressed() {
    }

}
