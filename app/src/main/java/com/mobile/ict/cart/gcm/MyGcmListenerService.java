package com.mobile.ict.cart.gcm;

import android.app.NotificationManager;
import android.content.Context;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;




public class MyGcmListenerService extends GcmListenerService {

    public static final String ACTION = "update";
    private static int broadcastId = 100;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(("Message"));
        String title = data.getString(("Title"));
        int id = Integer.parseInt(data.getString("id"));

        if (id == 8)
        {
            SharedPreferenceConnector.writeString(getApplicationContext(), Master.emailVerified, "1");

        } else if (id == 4) {

            sendProcessedOrderNotification(message, title);

        } else if (id == 5) {

            sendBroadCastNotification(message, title);


        } else if (id == 6) {

            sendDeliveredNotification(message, title);


        } else if (id == 7) {

            sendChangedOrderNotification(message, title);


        } else if (id == 10) {

            sendDeleteOrderNotification(message, title);


        }


    }

    private void sendProcessedOrderNotification(String message, String title) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("processed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(4, notificationBuilder.build());
    }


    private void sendBroadCastNotification(String message, String title) {

        broadcastId++;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setGroup("broadcast_messages");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(broadcastId, notificationBuilder.build());
    }


    private void sendDeliveredNotification(String message, String title) {


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("delivered_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(6, notificationBuilder.build());
    }

    private void sendChangedOrderNotification(String message, String title) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("changed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(7, notificationBuilder.build());
    }


    private void sendDeleteOrderNotification(String message, String title) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cart_white)
                .setContentTitle("Lokacart - " + title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setGroup("delete_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(10, notificationBuilder.build());
    }


}