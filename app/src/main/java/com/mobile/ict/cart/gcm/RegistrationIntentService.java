package com.mobile.ict.cart.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            sendRegistrationToServer(token);
            sharedPreferences.edit().putBoolean(Master.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (Exception e) {
            sharedPreferences.edit().putBoolean(Master.SENT_TOKEN_TO_SERVER, false).apply();
        }
    }

    private void sendRegistrationToServer(String token) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", token);
            obj.put("number", "91" + MemberDetails.getMobileNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SendTokenTask task = new SendTokenTask(this.getBaseContext());
        task.execute(obj);
    }

    static class SendTokenTask extends AsyncTask<JSONObject, String, Void> {
        final Context context;

        SendTokenTask(Context context) {
            this.context = context;
        }

        protected Void doInBackground(JSONObject... params) {
            Master.getJSON = GetJSON.getInstance();
            Master.getJSON.getJSONFromUrl(Master.sendGCMTokenUrl(), params[0], "POST", false, null, null);

            return null;
        }
    }



    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

}