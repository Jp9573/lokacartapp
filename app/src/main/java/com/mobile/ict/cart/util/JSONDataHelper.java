package com.mobile.ict.cart.util;

import android.content.Context;
import android.widget.Toast;

import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONDataHelper {
    public static ArrayList<Organisations> getOrganisationListFromJson(Context context, String JSON) {
        ArrayList<Organisations> list = new ArrayList<>();
        if (JSON.equals("")) {
            Toast.makeText(context, R.string.alert_unable_to_connect_server, Toast.LENGTH_LONG).show();

        } else {

            try {
                JSONObject jsonObject = new JSONObject(JSON);
                JSONArray jsonArray = jsonObject.getJSONArray(Master.ORGANISATIONS);
                if (jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject tempObj = jsonArray.getJSONObject(i);
                        Organisations organisations = new Organisations(tempObj);
                        list.add(organisations);
                    }
                }
            } catch (JSONException e) {
                Toast.makeText(context, R.string.alert_unable_to_connect_server, Toast.LENGTH_LONG).show();

            }
        }
        return list;
    }
}
