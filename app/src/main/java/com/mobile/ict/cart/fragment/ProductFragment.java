package com.mobile.ict.cart.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.adapter.ProductAdapter;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.container.ProductType;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


public class ProductFragment extends Fragment implements GetResponse {

    private RelativeLayout relativeLayout;
    private Drawer result;
    private RecyclerView recyclerView;
    private DrawerBuilder drawerBuilder;
    private DBHelper dbHelper;
    private ImageView ivNoProduct, ivNoInternet;
    private ImageView ivNoData;
    private TextView tNoProduct, tvOops, tvNoInternet;
    private TextView tNoData;
    private Menu searchMenu;
    private boolean isInternet = true;
    private Bundle savedInstanceState;
    private Dialog adDialogView;
    private StaggeredGridLayoutManager layoutManager;

    private static int selectedProductTypePosition = 0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a;

        if (context instanceof Activity) {
            a = (Activity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //AnalyticsFragment();
        setHasOptionsMenu(true);
        View productFragmentView;
        if (!Master.isNetworkAvailable(getActivity())) {
            productFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
            getActivity().setTitle(R.string.menu_products);
            isInternet = false;
            setHasOptionsMenu(true);
        } else {

            productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);
            getActivity().setTitle(R.string.title_fragment_product);
            relativeLayout = (RelativeLayout) productFragmentView.findViewById(R.id.relativeLayout);

            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

            recyclerView = (RecyclerView) productFragmentView.findViewById(R.id.rvProduct);
            recyclerView.setHasFixedSize(true);

            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setMotionEventSplittingEnabled(false);

            ivNoProduct = (ImageView) productFragmentView.findViewById(R.id.ivNoProduct);
            tNoProduct = (TextView) productFragmentView.findViewById(R.id.tNoProduct);

            ivNoProduct.setVisibility(View.GONE);
            tNoProduct.setVisibility(View.GONE);

            ivNoData = (ImageView) productFragmentView.findViewById(R.id.ivNoData);
            tNoData = (TextView) productFragmentView.findViewById(R.id.tNoData);

            ivNoData.setVisibility(View.GONE);
            tNoData.setVisibility(View.GONE);

            ivNoInternet = (ImageView) productFragmentView.findViewById(R.id.ivNoInternet);
            tvOops = (TextView) productFragmentView.findViewById(R.id.tvOops);
            tvNoInternet = (TextView) productFragmentView.findViewById(R.id.tvNoInternet);


            ivNoInternet.setVisibility(View.GONE);
            tvOops.setVisibility(View.GONE);
            tvNoInternet.setVisibility(View.GONE);

            setHasOptionsMenu(true);

            this.savedInstanceState = savedInstanceState;


            new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                    getString(R.string.pd_chk_internet_connection), ProductFragment.this, "Product_Internet").execute();

        }

        return productFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        searchMenu = menu;
        if (!isInternet) menu.clear();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.productTypeDrawer:
                if (result != null)
                    result.openDrawer();
                return true;


            default:

                return super.onOptionsItemSelected(item);

        }
    }



    /*private void AnalyticsFragment() {
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        Tracker mTracker = application.getDefaultTracker();
        String name = "AboutUs";
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }*/

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Master.productTypeList != null) Master.productTypeList.clear();

        if (Master.productList != null) Master.productList.clear();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (dbHelper == null) dbHelper = DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();

        // AnalyticsFragment();


        Master.isProductClicked = false;

        if (recyclerView != null) {
            isInternet = true;
            ProductAdapter rcAdapter = new ProductAdapter(getActivity());
            recyclerView.swapAdapter(rcAdapter, false);


            if (Master.productTypeList != null && !Master.productTypeList.isEmpty())
                updateCart();


        } else {
            isInternet = false;
        }

        if (result != null) {
            result.setSelectionAtPosition(selectedProductTypePosition);
        }


        if (!isInternet && searchMenu != null)
            searchMenu.clear();

        getActivity().invalidateOptionsMenu();


    }


    private void createDialog() {

        adDialogView = new Dialog(getActivity());
        adDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        adDialogView.setContentView(R.layout.newsad);
        adDialogView.setCancelable(true);
        adDialogView.setCanceledOnTouchOutside(true);


        ImageView liv = (ImageView) adDialogView.findViewById(R.id.adImageView);


        Button close = (Button) adDialogView.findViewById(R.id.adCloseButton);


        String image = SharedPreferenceConnector.readString(getActivity(), Master.image, "");
        final String link = SharedPreferenceConnector.readString(getActivity(), Master.link, "");


        Glide.with(getActivity())
                .load(image)
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.mipmap.organisation_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(liv);


        liv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferenceConnector.writeString(getActivity(), "showAdsDialog", "0");
                Intent intent;
                intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(link));
                startActivity(intent);
                adDialogView.dismiss();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceConnector.writeString(getActivity(), "showAdsDialog", "0");

                adDialogView.dismiss();

            }
        });

        adDialogView.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                SharedPreferenceConnector.writeString(getActivity(), "showAdsDialog", "0");

            }
        });

    }


    @Override
    public void getData(String response, String tag) {

        if (ProductFragment.this.isAdded()) {
            switch (tag) {
                case "Product_Internet":


                    if (response.equals("success")) {

                        if (SharedPreferenceConnector.readString(getActivity(), "showAdsDialog", "0").equals("1")) {
                            createDialog();
                            adDialogView.show();

                        }


                        isInternet = true;
                        selectedProductTypePosition = 0;
                        Master.isProductClicked = false;

                        dbHelper = DBHelper.getInstance(getActivity());


                        drawerBuilder = new DrawerBuilder();

                        Master.cartList = new ArrayList<>();


                        String url = Master.getProductsURL(dbHelper.getSelectedOrg(MemberDetails.getMobileNumber())[0]);


                        new NetworkAsyncTask
                                (url,
                                        "getProducts",
                                        Master.DIALOG_TRUE,
                                        getString(R.string.pd_fetching_products),
                                        ProductFragment.this,
                                        null,
                                        Master.GET,
                                        Master.AUTH_TRUE,
                                        MemberDetails.getEmail(),
                                        MemberDetails.getPassword()
                                ).execute();

                    } else {
                        ivNoInternet.setVisibility(View.VISIBLE);
                        tvOops.setVisibility(View.VISIBLE);
                        tvNoInternet.setVisibility(View.VISIBLE);
                        if (searchMenu != null)
                            searchMenu.clear();
                    }

                    break;

                case "getProducts":


                    if (!response.equals("exception")) {
                        try {
                            JSONObject object = new JSONObject(response);
                            Master.PAYTM_STATUS = object.getInt("paytm");
                            Master.DELIVERY_CHARGES = object.getInt("delCharges");


                            if (Master.PAYTM_STATUS == 2) {
                                Master.BANKNAME = object.getString("bankName");
                                Master.ACCOUNTNO = object.getString("accNo");
                                Master.IFSC = object.getString("ifsc");
                                Master.MICR = object.getString("micr");
                                Master.ACCOUNTNAME = object.getString("accName");
                                Master.BRANCH = object.getString("branch");
                                String accType = object.getString("accType");
                                if (accType.equals("0"))
                                    Master.ACCOUNTTYPE = "Current Account";
                                else
                                    Master.ACCOUNTTYPE = "Savings Account";
                            }

                            JSONArray array = object.getJSONArray("products");
                            Master.productTypeList = new ArrayList<>();
                            if (array.length() > 0) {
                                for (int x = 0; x < array.length(); ++x) {
                                    JSONObject prodTypeObj = array.getJSONObject(x);
                                    Iterator<String> keysItr = prodTypeObj.keys();
                                    while (keysItr.hasNext()) {
                                        String key = keysItr.next();

                                        ProductType productType = new ProductType(key);
                                        Object category = prodTypeObj.get(key);

                                        if (category instanceof JSONArray) {
                                            int size = ((JSONArray) category).length();
                                            for (int i = 0; i < size; i++) {

                                                Product product;
                                                if (((JSONArray) category).getJSONObject(i).has("imageUrl") && ((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                    if (

                                                            (
                                                                    (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) ||
                                                                            (((JSONArray) category).getJSONObject(i).getString("imageUrl").equals(""))
                                                            )
                                                                    &&
                                                                    (
                                                                            (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) ||
                                                                                    (((JSONArray) category).getJSONObject(i).getString("audioUrl").equals(""))

                                                                    )
                                                            ) {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"), "null", "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    } else if (
                                                            (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) ||
                                                                    (((JSONArray) category).getJSONObject(i).getString("imageUrl").equals(""))
                                                            ) {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    } else if (
                                                            (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) ||
                                                                    (((JSONArray) category).getJSONObject(i).getString("audioUrl").equals(""))
                                                            ) {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    } else {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                                ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    }
                                                } else if (((JSONArray) category).getJSONObject(i).has("imageUrl")) {
                                                    if (
                                                            (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) ||
                                                                    (((JSONArray) category).getJSONObject(i).getString("imageUrl").equals(""))
                                                            ) {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                "null",
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    } else {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    }
                                                } else if (((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                    if (
                                                            (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) ||
                                                                    (((JSONArray) category).getJSONObject(i).getString("audioUrl").equals(""))
                                                            ) {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                "null",
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    } else {
                                                        product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                                Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                                ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                                "null",
                                                                ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                                ((JSONArray) category).getJSONObject(i).getString("id"),
                                                                ((JSONArray) category).getJSONObject(i).getString("description"),
                                                                Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                    }
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            "null",
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("gst")));
                                                }


                                               /* com.google.android.gms.analytics.ecommerce.Product analyticsProduct = new com.google.android.gms.analytics.ecommerce.Product()
                                                        .setId(((JSONArray) category).getJSONObject(i).getString("id"))
                                                        .setName(((JSONArray) category).getJSONObject(i).getString("name"))

                                                        .setPrice(Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")))
                                                        .setQuantity(Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")));
                                                ProductAction productAction = new ProductAction(ProductAction.ACTION_CHECKOUT)
                                                        .setCheckoutStep(1)
                                                        .setCheckoutOptions("View Product");
                                                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                                                        .addProduct(analyticsProduct)
                                                        .setProductAction(productAction);

                                                Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                                                        LokacartApplication.TrackerName.APP_TRACKER);
                                                t.setScreenName("checkoutStep1");
                                                t.send(builder.build());*/

                                                productType.productItems.add(product);
                                            }
                                        }

                                        int i = 0; // to set the selection dynamically afterwards
                                        if (productType.productItems.size() > 0) {
                                            //noinspection UnusedAssignment
                                            drawerBuilder.addDrawerItems(new PrimaryDrawerItem().withName(productType.getName()).withIdentifier(i++));
                                            Master.productTypeList.add(productType);
                                        }
                                    }
                                }

                                if (Master.productTypeList.size() != 0) {


                                    DashboardActivity.updateSearchAdapter();

                                    result = drawerBuilder
                                            .withActivity(getActivity())
                                            .withRootView(relativeLayout)
                                            .withDisplayBelowStatusBar(false)
                                            .withSavedInstance(savedInstanceState)
                                            .withDrawerGravity(Gravity.END)
                                            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                                                @Override

                                                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                                                    drawerItemClick(position);
                                                    return true;
                                                }

                                            })
                                            .buildForFragment();


                                    result.getDrawerLayout().setFitsSystemWindows(true);
                                    result.getSlider().setFitsSystemWindows(true);


                                    Master.productList = Master.productTypeList.get(0).productItems;

                                    Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
                                    updateCart();
                                    Master.updateProductList();

                                    ProductAdapter rcAdapter = new ProductAdapter(getActivity());
                                    recyclerView.setAdapter(rcAdapter);
                                    getActivity().setTitle(Master.productTypeList.get(0).getName());

                                    if (!SharedPreferenceConnector.readBoolean(getActivity(), Master.PRODUCT_DRAWER_ALERT_TAG, false)) {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setCancelable(false);
                                        builder.setMessage(getActivity().getString(R.string.alert_right_drawer_alert));
                                        builder.setPositiveButton(R.string.button_dont_show_again, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                SharedPreferenceConnector.writeBoolean(getActivity(), Master.PRODUCT_DRAWER_ALERT_TAG, true);
                                                result.openDrawer();
                                            }
                                        });
                                        builder.setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                result.openDrawer();

                                            }
                                        });
                                        builder.show();
                                    }
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    ivNoProduct.setVisibility(View.VISIBLE);
                                    tNoProduct.setVisibility(View.VISIBLE);

                                    if (searchMenu != null)
                                        searchMenu.clear();
                                }

                            } else {
                                recyclerView.setVisibility(View.GONE);
                                ivNoProduct.setVisibility(View.VISIBLE);
                                tNoProduct.setVisibility(View.VISIBLE);

                                if (searchMenu != null)
                                    searchMenu.clear();

                            }
                        } catch (JSONException ignored) {
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        ivNoData.setVisibility(View.VISIBLE);
                        tNoData.setVisibility(View.VISIBLE);
                        try {
                            searchMenu.clear();

                        } catch (Exception ignored) {

                        }

                    }

                    break;

                default:
                    break;
            }


        }
    }

    public void setSelection(int position) {
        selectedProductTypePosition = position;
    }

    private void drawerItemClick(int position) {
        result.closeDrawer();

        selectedProductTypePosition = position;
        Master.productList = Master.productTypeList.get(position).productItems;

        Master.updateProductList();

        ProductAdapter swapAdapter = new ProductAdapter(getActivity());
        recyclerView.swapAdapter(swapAdapter, false);
        layoutManager.scrollToPosition(0);
        getActivity().setTitle(Master.productTypeList.get(position).getName());
    }

    private void updateCart() {
        for (int i = 0; i < Master.productTypeList.size(); ++i) {
            for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j) {
                for (int z = 0; z < Master.cartList.size(); z++) {
                    if (Master.cartList.get(z).getID().equals(Master.productTypeList.get(i).productItems.get(j).getID())) {
                        Master.cartList.get(z).setName(Master.productTypeList.get(i).productItems.get(j).getName());
                        Master.cartList.get(z).setUnitPrice(Master.productTypeList.get(i).productItems.get(j).getUnitPrice());
                        Master.cartList.get(z).setImageUrl(Master.productTypeList.get(i).productItems.get(j).getImageUrl());
                        Master.cartList.get(z).setStockQuantity(Master.productTypeList.get(i).productItems.get(j).getStockQuantity());
                        Master.cartList.get(z).setStockEnabledStatus(Master.productTypeList.get(i).productItems.get(j).getStockEnabledStatus());
                        Master.cartList.get(z).setGst(Master.productTypeList.get(i).productItems.get(j).getGst());

                        String total = String.format("%.2f", Master.productTypeList.get(i).productItems.get(j).getUnitPrice() * Master.cartList.get(z).getQuantity());

                        Master.cartList.get(z).setTotal(Double.parseDouble(total));

                        dbHelper.updateProduct(
                                String.valueOf(Master.cartList.get(z).getUnitPrice()),
                                String.valueOf(Master.cartList.get(z).getQuantity()),
                                String.valueOf(Master.cartList.get(z).getTotal()),
                                String.valueOf(Master.cartList.get(z).getName()),
                                MemberDetails.getMobileNumber(),
                                MemberDetails.getSelectedOrgAbbr(),
                                String.valueOf(Master.cartList.get(z).getID()),
                                String.valueOf(Master.cartList.get(z).getImageUrl()),
                                String.valueOf(Master.cartList.get(z).getStockQuantity()),
                                Master.cartList.get(z).getStockEnabledStatus(),
                                String.valueOf(Master.cartList.get(z).getGst())
                        );
                    }
                }
            }
        }
    }
}
