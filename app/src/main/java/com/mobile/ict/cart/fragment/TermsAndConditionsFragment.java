package com.mobile.ict.cart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.ict.cart.R;

public class TermsAndConditionsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View termsAndConditionsFragmentView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        getActivity().setTitle(R.string.title_fragment_terms_and_conditions);

        setHasOptionsMenu(true);

        return termsAndConditionsFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
