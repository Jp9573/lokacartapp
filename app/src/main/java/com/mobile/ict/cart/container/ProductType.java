package com.mobile.ict.cart.container;

import java.util.ArrayList;


public class ProductType {

    private String name;
    public final ArrayList<Product> productItems = new ArrayList<>();

    public ProductType(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
