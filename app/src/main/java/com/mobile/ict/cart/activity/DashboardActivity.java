package com.mobile.ict.cart.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.fragment.AboutUsFragment;
import com.mobile.ict.cart.fragment.DeliveredOrderFragment;
import com.mobile.ict.cart.fragment.FAQDetailFragment;
import com.mobile.ict.cart.fragment.OrganisationFragment;
import com.mobile.ict.cart.fragment.ContactUsFragment;
import com.mobile.ict.cart.fragment.FAQFragment;
import com.mobile.ict.cart.fragment.FeedbackFragment;
import com.mobile.ict.cart.fragment.PlacedOrderFragment;
import com.mobile.ict.cart.fragment.ProcessedOrderFragment;
import com.mobile.ict.cart.fragment.ProductFragment;
import com.mobile.ict.cart.fragment.ProfileFragment;
import com.mobile.ict.cart.fragment.ReferralFragment;
import com.mobile.ict.cart.fragment.TermsAndConditionsFragment;
import com.mobile.ict.cart.gcm.RegistrationIntentService;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity
        implements GetResponse,
        ProfileFragment.UpdateNavData,
        FeedbackFragment.UpdateDrawer,
        FAQFragment.ChangeFAQsDetail,
        FAQDetailFragment.ShowHamburger {

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed = 0;
    private FragmentManager fragmentManager;
    private TextView drawerName;
    private TextView drawerEmail;
    private TextView drawerMobileNumber;
    private View headerView;
    private Drawer result;
    private DBHelper dbHelper;
    private Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static ArrayAdapter<String> itemsAdapter;

    private static ArrayList<String> newProducts;

    private static ArrayList<String> products;
    private static ArrayList<String> products_id;

    private String query_entered = null;

    private ListView listView;

    Menu optionMenu;
    int crash = 0;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("crash", crash);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (savedInstanceState != null) {
            crash = 1;

        }


        Master.isMember = true;
        // AnalyticsActivity();
        //  AnalyticsEvent();

        dbHelper = DBHelper.getInstance(this);

        dbHelper.getProfile();


        Master.productList = new ArrayList<>();

        products = new ArrayList<>();
        products_id = new ArrayList<>();

        setContentView(R.layout.activity_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.dashboardRelativeLayout);

        fragmentManager = getSupportFragmentManager();

        headerView = getLayoutInflater().inflate(R.layout.nav_header_dashboard, relativeLayout, false);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withHeader(headerView)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.menu_products).withIcon(R.drawable.products).withIdentifier(0),
                        new ExpandableDrawerItem().withName(R.string.menu_orders).withIcon(R.drawable.orders).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.menu_placed_orders).withLevel(5).withIdentifier(1),
                                new SecondaryDrawerItem().withName(R.string.menu_processed_orders).withLevel(5).withIdentifier(2),
                                new SecondaryDrawerItem().withName(R.string.menu_delivered_orders).withLevel(5).withIdentifier(3)

                        ),

                        new PrimaryDrawerItem().withName(R.string.menu_profile).withIcon(R.drawable.profile).withIdentifier(11),
                        new PrimaryDrawerItem().withName(R.string.menu_change_organisation).withIcon(R.drawable.organisation).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.menu_referrals).withIcon(R.drawable.referrals).withIdentifier(5),
                        new PrimaryDrawerItem().withName(R.string.menu_video).withIcon(R.drawable.video).withSelectable(false).withIdentifier(12),
                        new PrimaryDrawerItem().withName(R.string.menu_about_us).withIcon(R.drawable.about_us).withIdentifier(6),
                        new ExpandableDrawerItem().withName(R.string.menu_help).withIcon(R.drawable.help).withSelectable(false).withSubItems(
                                new SecondaryDrawerItem().withName(R.string.title_fragment_feedback).withLevel(5).withIdentifier(7),
                                new SecondaryDrawerItem().withName(R.string.menu_contact_us).withLevel(5).withIdentifier(8),
                                new SecondaryDrawerItem().withName(R.string.menu_terms_and_conditions).withLevel(5).withIdentifier(9),
                                new SecondaryDrawerItem().withName(R.string.menu_faqs).withLevel(5).withIdentifier(10)
                        )
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, final IDrawerItem drawerItem) {
                        dashBoardClickListener(drawerItem.getIdentifier());
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
                        DashboardActivity.super.onBackPressed();
                        //return true if we have consumed the event
                        return true;
                    }
                }).build();

        drawerName = (TextView) headerView.findViewById(R.id.tDashboardName);
        drawerName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());

        drawerEmail = (TextView) headerView.findViewById(R.id.tDashboardEmail);

        if (!MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com"))
            drawerEmail.setText(MemberDetails.getEmail());
        else
            drawerEmail.setText("");


        drawerMobileNumber = (TextView) headerView.findViewById(R.id.tDashboardMobileNumber);
        drawerMobileNumber.setText(MemberDetails.getMobileNumber());

        ImageView ivEdit = (ImageView) headerView.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment(), Master.PROFILE_TAG).commit();
                result.setSelection(11);
                result.closeDrawer();
            }
        });


        try {

            if (getIntent().getStringExtra("redirectto").equals("processed")) {
                result.setSelection(2);


            } else if (getIntent().getStringExtra("redirectto").equals("delivered")) {
                result.setSelection(3);

            } else if (getIntent().getStringExtra("redirectto").equals("placed")) {
                result.setSelection(1);


            } else if (getIntent().getStringExtra("redirectto").equals("product")) {
                result.setSelection(0);


            } else if (getIntent().getStringExtra("redirectto").equals("organisation")) {
                result.setSelection(4);


            } else {


                result.setSelection(0);
            }
        } catch (Exception e) {

            if (crash == 1) {
                Intent i;
                i = new Intent(DashboardActivity.this, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            } else {

                result.setSelection(0);


            }
        }

        result.closeDrawer();


        Intent intent = new Intent(this, RegistrationIntentService.class);
        if (checkPlayServices()) {
            startService(intent);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        String tag = SharedPreferenceConnector.readString(this, "tag", "");

        if (tag.equals(Master.PROFILE_TAG)) {
            ProfileFragment profileFragment = (ProfileFragment) fragmentManager.findFragmentByTag(Master.PROFILE_TAG);
            profileFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void updateDrawerPosition() {
        result.setSelection(0);
    }


    private void dashBoardClickListener(long position) {
        Master.hideSoftKeyboard(DashboardActivity.this);

        if (position == 0) {
            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();
        } else if (position == 1) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new PlacedOrderFragment(), Master.PLACED_ORDER_TAG).commit();
        } else if (position == 2) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProcessedOrderFragment(), Master.PROCESSED_ORDER_TAG).commit();
        } else if (position == 3) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new DeliveredOrderFragment(), Master.DELIVERED_ORDER_TAG).commit();
        } else if (position == 4) {

            result.getAdapter().collapse();

            fragmentManager.beginTransaction().replace(R.id.content_frame, new OrganisationFragment(), Master.CHANGE_ORGANISATION_TAG).commit();
        } else if (position == 5) {

            result.getAdapter().collapse();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ReferralFragment(), Master.REFERRALS_TAG).commit();
        } else if (position == 6) {

            result.getAdapter().collapse();

            fragmentManager.beginTransaction().replace(R.id.content_frame, new AboutUsFragment(), Master.ABOUT_US_TAG).commit();
        } else if (position == 7) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new FeedbackFragment(), Master.FEEDBACKS_TAG).commit();
        } else if (position == 8) {

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            ft.commit();

            FragmentManager fm = this.getSupportFragmentManager();
            ContactUsFragment newFragment = new ContactUsFragment();
            newFragment.show(fm, "dialog");
        } else if (position == 9) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new TermsAndConditionsFragment(), Master.TERMS_AND_CONDITIONS_TAG).commit();
        } else if (position == 10) {

            fragmentManager.beginTransaction().replace(R.id.content_frame, new FAQFragment(), Master.FAQ_TAG).commit();
        } else if (position == 11) {

            result.getAdapter().collapse();
            ProfileFragment profileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag(Master.PROFILE_TAG);
            if (profileFragment == null)
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment(), Master.PROFILE_TAG).commit();
        } else if (position == 12) {

            result.getAdapter().collapse();
            Intent intent;
            intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.setData(Uri.parse("https://lvweb.lokavidya.com/"));
            startActivity(intent);
        }
    }

    public static void updateSearchAdapter() {
        if (Master.productTypeList != null) {
            products = new ArrayList<>();
            products_id = new ArrayList<>();
            for (int i = 0; i < Master.productTypeList.size(); ++i) {
                for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j) {
                    products.add(Master.productTypeList.get(i).productItems.get(j).getName());
                    products_id.add(Master.productTypeList.get(i).productItems.get(j).getID());
                }
                if (itemsAdapter != null)
                    itemsAdapter.notifyDataSetChanged();
            }
        }
    }

    private int getPositionFromName(String name) {
        for (int i = 0; i < Master.productTypeList.size(); ++i) {
            for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j) {
                if (Master.productTypeList.get(i).productItems.get(j).getName().equals(name)) {
                    Master.productList = Master.productTypeList.get(i).productItems;
                    setTitle(Master.productTypeList.get(i).getName());
                    ProductFragment productFragment = (ProductFragment) getSupportFragmentManager().findFragmentByTag(Master.PRODUCT_TAG);
                    if (productFragment != null)
                        productFragment.setSelection(i);

                    return j;
                }
            }
        }
        return -1;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        optionMenu = menu;
        products = new ArrayList<>();
        itemsAdapter = new ArrayAdapter<>(this, R.layout.search_list_item, R.id.product_name, products);
        listView = (ListView) findViewById(R.id.search_list_view);
        updateSearchAdapter();
        listView.setAdapter(itemsAdapter);
        getMenuInflater().inflate(R.menu.cart_menu_white, menu);
        MenuItem item = menu.findItem(R.id.action_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchMenuItem.getActionView();

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                FrameLayout layout = (FrameLayout) findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);

                return true;
            }
        });

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {

                query_entered = newText;

                if (TextUtils.isEmpty(newText)) {
                    listView.clearTextFilter();
                    itemsAdapter.getFilter().filter("");
                    listView.setAdapter(itemsAdapter);
                } else {


                    listView.setAdapter(null);


                    newProducts = new ArrayList<>();
                    ArrayAdapter<String> newitemsAdapter = new ArrayAdapter<>(DashboardActivity.this, R.layout.search_list_item, R.id.product_name, newProducts);

                    for (int i = 0; i < products.size(); i++) {
                        String item = products.get(i).toLowerCase();
                        String qr = newText.toLowerCase();

                        if (item.contains(qr)) {
                            newitemsAdapter.add(item);
                        }
                    }

                    listView.setAdapter(newitemsAdapter);

                }

                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if (!queryTextFocused) {
                    searchMenuItem.collapseActionView();
                    searchView.setQuery("", false);
                }
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                ArrayList<Integer> newPosition = new ArrayList<>();
                for (int i = 0; i < products.size(); i++) {
                    String item = products.get(i).toLowerCase();
                    String qr = query_entered.toLowerCase();

                    if (item.contains(qr)) {
                        newPosition.add(i);
                    }
                }

                position = newPosition.get(position);


                FrameLayout layout = (FrameLayout) findViewById(R.id.content_frame);
                layout.setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);


                searchMenuItem.collapseActionView();
                searchView.setQuery("", false);


                Intent i = new Intent(DashboardActivity.this, ProductDetailActivity.class);

                i.putExtra("position", getPositionFromName(products.get(position)));
                startActivity(i);
            }
        });


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.action_cart:
                startActivity(new Intent(DashboardActivity.this, CartActivity.class));
                return true;

            case R.id.search:
                FrameLayout layout = (FrameLayout) findViewById(R.id.content_frame);
                layout.setVisibility(View.GONE);
                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.VISIBLE);

                return true;
            case R.id.productTypeDrawer:
                return false;

            default:

                return super.onOptionsItemSelected(item);

        }
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU)
        {

            optionMenu.performIdentifierAction(R.id.productTypeDrawer,0);
            return true;
        }
        return  super.onKeyDown(keyCode, event);
    }*/

  /*  private void AnalyticsActivity() {
        LokacartApplication application = (LokacartApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();

        String name = "Dashboard";
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void AnalyticsEvent() {
        Tracker t = ((LokacartApplication) getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);

        t.enableAdvertisingIdCollection(true);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Membership")
                .setAction("CreateAccount")
                .build());
    }
*/

    @Override
    protected void onResume() {
        super.onResume();

        //AnalyticsActivity();
        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        Master.CART_ITEM_COUNT = dbHelper.getCartItemsCount(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());


        if (listView != null) {


            if (itemsAdapter == null) {
                itemsAdapter = new ArrayAdapter<>(this, R.layout.search_list_item, R.id.product_name, products);
                listView.setAdapter(itemsAdapter);

            }

        }

        if (drawerEmail != null && drawerName != null && drawerMobileNumber != null) {
            drawerName = (TextView) headerView.findViewById(R.id.tDashboardName);
            drawerName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());
            drawerEmail = (TextView) headerView.findViewById(R.id.tDashboardEmail);


            if (!MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com"))
                drawerEmail.setText(MemberDetails.getEmail());
            else
                drawerEmail.setText("");

            drawerMobileNumber = (TextView) headerView.findViewById(R.id.tDashboardMobileNumber);
            drawerMobileNumber.setText(MemberDetails.getMobileNumber());
        }

        invalidateOptionsMenu();

        if (!Master.isMember) {
            result.setSelection(3);
        }
        try {
            if (SharedPreferenceConnector.readString(this, "refresh", "").equals("refresh")) {
                SharedPreferenceConnector.writeString(this, "refresh", "");
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment(), Master.PRODUCT_TAG).commit();

            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onBackPressed() {
        if (result.isDrawerOpen()) {
            result.closeDrawer();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {

            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                System.exit(0);
                return;
            } else {
                Toast.makeText(getBaseContext(), getString(R.string.back_button_to_exit), Toast.LENGTH_SHORT).show();
            }

            mBackPressed = System.currentTimeMillis();
        } else {
            getSupportFragmentManager().popBackStack();

        }

    }

    @Override
    public void updateNavHeader() {
        drawerName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());

        if (!MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com"))
            drawerEmail.setText(MemberDetails.getEmail());
        else
            drawerEmail.setText("");

        drawerMobileNumber.setText(MemberDetails.getMobileNumber());
    }

    @Override
    public void getData(String objects, String tag) {

    }

    @Override
    public void openFAQDetailFragment(int position) {
        Bundle b = new Bundle();
        b.putInt("position", position);

        /*setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);*/

        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FAQDetailFragment faQsFragment2 = new FAQDetailFragment();
        faQsFragment2.setArguments(b);

        fragmentManager.beginTransaction().replace(R.id.content_frame, faQsFragment2, Master.FAQ_TAG).addToBackStack(null).commit();
    }

    @Override
    public void showHamburgerIcon() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
    }
}
