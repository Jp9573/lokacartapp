package com.mobile.ict.cart.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

public class OrgInfoActivity extends AppCompatActivity implements View.OnClickListener, GetResponse {

    private TextView tvOrginfoAddress;
    private TextView tvInfoOrgAbout;
    private ImageView iVInfoOrgLogo;
    private Dialog authenticationDialogView;
    private String logoUrl = "";
    private String number = "";
    private String orgAbbr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //LokacartApplication application = (LokacartApplication) getApplication();
        //Tracker mTracker = application.getDefaultTracker();
        String name = "OrgInfo";
        // mTracker.setScreenName("Screen~" + name);
        // mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setContentView(R.layout.activity_org_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.title_activity_Org_info));
        tvOrginfoAddress = (TextView) findViewById(R.id.tvOrginfoAddress);
        tvInfoOrgAbout = (TextView) findViewById(R.id.tvOrginfoabout);
        TextView tvInfoOrgName = (TextView) findViewById(R.id.tvOrginfoName);
        iVInfoOrgLogo = (ImageView) findViewById(R.id.ivOrginfoLogo);

        iVInfoOrgLogo.setOnClickListener(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.orginfofab);
        fab.setOnClickListener(this);
        number = getIntent().getStringExtra("contact");
        orgAbbr = getIntent().getStringExtra("abbr");
        tvInfoOrgName.setText(getIntent().getStringExtra("name"));

        if (Master.isNetworkAvailable(OrgInfoActivity.this))

        {

            new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                    getString(R.string.pd_chk_internet_connection), OrgInfoActivity.this, "OrgInfo_Internet").execute();

        } else {
            Toast.makeText(OrgInfoActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

        }


    }


    private void createDialog() {
        authenticationDialogView = new Dialog(this);
        authenticationDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        authenticationDialogView.setContentView(R.layout.large_product_image);
        authenticationDialogView.setCancelable(true);
        authenticationDialogView.setCanceledOnTouchOutside(true);
        ImageView liv = (ImageView) authenticationDialogView.findViewById(R.id.largeImageView);

        Glide.with(OrgInfoActivity.this).load(logoUrl)
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.mipmap.organisation_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(liv);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivOrginfoLogo: {

                authenticationDialogView.show();

                break;
            }

            case R.id.orginfofab: {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+91" + number));
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void getData(String objects, String tag) {

        switch (tag) {

            case "OrgInfo_Internet":
                if (objects.equals("success")) {

                    JSONObject params = new JSONObject();
                    try {
                        params.put("r", "e");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    new NetworkAsyncTask(
                            Master.getOrgInfoURL(orgAbbr),
                            "orginfo",
                            Master.DIALOG_TRUE,
                            getString(R.string.pd_getting_org_info),
                            this,
                            params,
                            Master.GET,
                            Master.AUTH_TRUE,
                            MemberDetails.getEmail(),
                            MemberDetails.getPassword()
                    ).execute();
                } else {
                    Material.alertDialog(this, getString(R.string.toast_Please_check_internet_connection), getString(R.string.dialog_ok));

                }
                break;


            case "orginfo":

                if (!objects.equals("exception")) {
                    try {
                        JSONObject root = new JSONObject(objects);
                        tvInfoOrgAbout.setText(root.getString("description"));
                        tvOrginfoAddress.setText(root.getString("address"));
                        number = root.getString("phone");
                        logoUrl = root.getString("logoUrl");
                        Glide.with(OrgInfoActivity.this).load(logoUrl)
                                .thumbnail(0.5f)
                                .crossFade()
                                .placeholder(R.mipmap.organisation_placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(iVInfoOrgLogo);

                        createDialog();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                }

                break;

            default:
                break;

        }


    }


}
