package com.mobile.ict.cart.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;



public class ReferralFragment extends Fragment implements GetResponse {

    private DBHelper dbHelper;
    private EditText phoneNumberNew;
    private EditText eReferralCode;
    private ArrayAdapter<String> adapter;
    private List<String> list;
    private int position = 0;
    private JSONObject obj;
    private String Orgabbr;
    private String phoneNumber;
    Button refer,selfRefer,submit,referralCancel;
    private Dialog referralDialogView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);

        getActivity().setTitle(R.string.title_fragment_referral);

        setHasOptionsMenu(true);

      //  AnalyticsFragment();

        View view = inflater.inflate(R.layout.fragment_referral, container, false);
        dbHelper = DBHelper.getInstance(getActivity());


        phoneNumberNew = (EditText) view.findViewById(R.id.phonenumber_et);
         refer = (Button) view.findViewById(R.id.refer_button);
        selfRefer = (Button) view.findViewById(R.id.selfRefer_button);

        Spinner spinner = (Spinner) view.findViewById(R.id.referra_org_spinner);
        ImageView contactPicker = (ImageView) view.findViewById(R.id.contact_picker_iv);

        list = new ArrayList<>();
        list.add(getActivity().getString(R.string.label_loading_org_list));


        adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, list);
        spinner.setAdapter(adapter);


        if (Master.isNetworkAvailable(getActivity()))
        {

            new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                    getString(R.string.pd_chk_internet_connection),ReferralFragment.this,"Organisation_Internet" ).execute();



        } else
        {

           Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
            refer.setEnabled(false);

        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                position = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        contactPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, 1);
            }
        });


        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (phoneNumberNew.getText().toString().trim().equals("")) {

                    //AnalyticsEvent("UtilityError", "ReferralError- phone number not present");
                    Toast.makeText(getActivity(), R.string.toast_please_fill_all_the_details, Toast.LENGTH_LONG).show();
                } else if (phoneNumberNew.getText().toString().trim().length() != 10) {

                   // AnalyticsEvent("UtilityError", "ReferralError - phone number not 10 digits");
                    Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();

                } else {

                    if (!phoneNumberNew.getText().toString().trim().matches("\\d{10}")) {
                        //AnalyticsEvent("UtilityError", "ReferralError - phone number not 10 digits");
                        Toast.makeText(getActivity(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();

                    } else {

                        if (position != 0) {
                            Orgabbr = Organisations.organisationList.get(position - 1).getOrgabbr();
                            phoneNumber = "91" + phoneNumberNew.getText().toString().trim();

                            obj = new JSONObject();
                            sendReferal();

                        } else {
                            Toast.makeText(getActivity(), R.string.toast_please_select_an_organisation, Toast.LENGTH_LONG).show();
                        }

                    }

                }
            }
        });


        selfRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openReferralDialog();
            }
        });





        return view;
    }



    private void openReferralDialog()
    {
        referralDialogView = new Dialog(getActivity());
        referralDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialogView.setContentView(R.layout.dialog_referral_code);
        referralDialogView.setCancelable(false);
        TextInputLayout txtInputMobileNumber = (TextInputLayout) referralDialogView.findViewById(R.id.textInputMobileNumber);
        EditText eReferralMobileNumber = (EditText) referralDialogView.findViewById(R.id.eReferralMobileNumber);

        eReferralMobileNumber.setVisibility(View.GONE);
        txtInputMobileNumber.setVisibility(View.GONE);


        eReferralCode = (EditText) referralDialogView.findViewById(R.id.eReferralCode);
        referralCancel = (Button) referralDialogView.findViewById(R.id.bReferralCancel);
        submit = (Button) referralDialogView.findViewById(R.id.bReferralSubmit);

        referralCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialogView.dismiss();
                referralDialogView=null;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (Master.isNetworkAvailable(getActivity()))
                {

                    new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection),ReferralFragment.this,"CheckReferralCode_Internet" ).execute();


                }
                else
                {
                    Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                }

            }
        });

        referralDialogView.show();
    }










    private void sendReferal() {

        new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                getString(R.string.pd_chk_internet_connection),ReferralFragment.this,"Referral_Internet" ).execute();



    }

    /*private void AnalyticsEvent(String categoryId, String actionId) {
        Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .build());
    }*/


  /*  private void AnalyticsFragment() {
        LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        Tracker mTracker = application.getDefaultTracker();
        String name = "Referral";
        mTracker.setScreenName("Fragment~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case 1:
                    contactPicked(data);
                    break;
            }
        } else {
        }
    }


    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo;
            Uri uri = data.getData();
            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            phoneNo = cursor.getString(phoneIndex);


            phoneNo=phoneNo.trim().replaceAll("[+ ]", "");
            if(phoneNo.length()>10)
            {
                    if(phoneNo.startsWith("91"))
                        phoneNo=phoneNo.substring(2);
                    else if(phoneNo.startsWith("0"))
                        phoneNo=phoneNo.substring(1);

            }
             phoneNumberNew.setText(phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(cursor != null)
                cursor.close();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
      //  AnalyticsFragment();
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();
       // Master.getMemberDetails(getActivity());
    }

    @Override
    public void getData(String response, String tag)
    {
        if (ReferralFragment.this.isAdded())
        {
            switch (tag)
            {

                case "Organisation_Internet":
                    if(response.equals("success"))
                    {

                        try {
                            JSONObject obj = new JSONObject();
                            obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                            obj.put(Master.PASSWORD, MemberDetails.getPassword());
                            new NetworkAsyncTask
                                    (
                                            Master.getLoginURL(),
                                            "getOrganisations",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_fetching_data_from_server),
                                            ReferralFragment.this,
                                            obj,
                                            Master.POST,
                                            Master.AUTH_FALSE,
                                            null,
                                            null
                                    ).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();
                        refer.setEnabled(false);

                    }

                    break;

                case "Referral_Internet":
                    if(response.equals("success"))
                    {
                        try {
                            obj.put(Master.DEFAULT_ORG_ABBR, Orgabbr);
                            obj.put("phonenumber", phoneNumber);
                            obj.put("refemail", MemberDetails.getEmail());

                            new NetworkAsyncTask
                                    (
                                            Master.getSendReferURL(),
                                            "sendReferral",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_referral),
                                            ReferralFragment.this,
                                            obj,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();

                    }
                    break;




                case "getOrganisations":

                    if (response.equals("exception")) {
                        list.clear();
                        Toast.makeText(getActivity(), getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_SHORT).show();

                        adapter.notifyDataSetChanged();
                    } else {
                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                        Organisations.organisationList = new ArrayList<>();
                        Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), SharedPreferenceConnector
                                .readString(getActivity().getApplicationContext(), Master.LOGIN_JSON, Master.DEFAULT_LOGIN_JSON));
                        list.clear();

                        if (Organisations.organisationList.size() > 0) {
                            list.add(getActivity().getString(R.string.label_select_organisation));
                            for (int i = 0; i < Organisations.organisationList.size(); ++i) {
                                list.add(Organisations.organisationList.get(i).getName());
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            list.add(getActivity().getString(R.string.label_error_loading_organisations));
                            adapter.notifyDataSetChanged();
                        }
                    }
                    break;

                case "sendReferral":

                    if (response.equals("exception"))

                    {

                        Toast.makeText(getActivity(), getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_SHORT).show();

                    } else {
                        try {
                            JSONObject responseJson = new JSONObject(response);
                            if (responseJson.has("response")) {
                                String resp = responseJson.getString("response");
                                switch (resp.toLowerCase()) {
                                    case "already a member": {
                                        Toast.makeText(getActivity(), R.string.toast_already_a_member, Toast.LENGTH_SHORT).show();
                                        break;
                                    }
                                    case "success": {
                                        Toast.makeText(getActivity(), R.string.toast_member_referred_successfully, Toast.LENGTH_SHORT).show();
                                        //AnalyticsEvent("Membership", "Referralsuccess");
                                        break;
                                    }
                                    case "failure": {
                                      //  AnalyticsEvent("Membership", "Referralfailure");
                                        if (responseJson.has("reason")) {
                                            Toast.makeText(getActivity(), R.string.toast_error_cannot_refer_admin, Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getActivity(), R.string.toast_error_while_sending_referral, Toast.LENGTH_LONG).show();
                                        }

                                        break;
                                    }


                                }

                                SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, response);
                            }


                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), R.string.toast_error_refer, Toast.LENGTH_LONG).show();

                            e.printStackTrace();
                        }

                    }
                    dbHelper.getProfile();

                    break;




                case "CheckReferralCode_Internet" :

                    if(response.equals("success"))
                    {

                        if (eReferralCode.getText().toString().trim().equals(""))

                            Toast.makeText(getActivity(), R.string.toast_Please_enter_referral_code, Toast.LENGTH_LONG).show();

                        else {

                            eReferralCode.setEnabled(false);

                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("phonenumber", "91"+ MemberDetails.getMobileNumber());
                                jsonObject.put("refCode", eReferralCode.getText().toString().trim());

                                new NetworkAsyncTask
                                        (
                                                Master.getReferCodeURL(),
                                                "referralCode",
                                                Master.DIALOG_TRUE,
                                                getString(R.string.pd_sending_data_to_server),
                                                ReferralFragment.this,
                                                jsonObject,
                                                Master.POST,
                                                Master.AUTH_FALSE,
                                                null,
                                                null
                                        ).execute();

                            } catch (JSONException e)
                            {
                            }
                        }


                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                    }

                    break;



                case "referralCode" :

                    if (response.equals("exception"))
                    {
                        eReferralCode.setEnabled(true);
                        Material.alertDialog(getActivity(), getString(R.string.toast_we_are_facing_some_technical_problems_landing_page), "OK");
                    }
                    else
                    {

                        try
                        {
                            eReferralCode.setEnabled(true);

                            JSONObject  obj = new JSONObject(response);
                            String status = obj.getString("response");

                            switch(status)
                            {
                                case "success" :
                                    referralDialogView.dismiss();

                                    Toast.makeText(getActivity(), R.string.toast_get_sms_referral, Toast.LENGTH_LONG).show();

                                    break;

                                case "Already a member":
                                    Toast.makeText(getActivity(), R.string.toast_already_member_organizations, Toast.LENGTH_LONG).show();

                                    break;


                                case "admin":
                                    Toast.makeText(getActivity(), R.string.toast_error_cannot_refer_admin, Toast.LENGTH_LONG).show();

                                    break;

                                case "No such code":
                                    Toast.makeText(getActivity(), R.string.toast_referral_mismatch, Toast.LENGTH_LONG).show();

                                    break;

                                default: break;
                            }
                        }
                        catch(Exception e)
                        {
                            Toast.makeText(getActivity(), R.string.toast_we_are_facing_some_technical_problems_landing_page, Toast.LENGTH_LONG).show();

                        }

                    }
                    break;


                default:
                    break;
            }


        }


    }

}
