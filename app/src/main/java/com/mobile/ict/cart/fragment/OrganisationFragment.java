package com.mobile.ict.cart.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.OrganisationAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class OrganisationFragment extends Fragment implements View.OnClickListener, GetResponse {

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private Button bChangeOrganisation;
    private Button bLeaveOrganisation;
    private TextView tOrganisationZero;
    private TextView tNoData,tNoInternet,tOops;
    private ImageView ivNoData,ivNoInternet;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private DBHelper dbHelper;
    private Tracker mTracker;
    private final String name = "Contact";
    private int position;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        getActivity().setTitle(R.string.title_fragment_change_organisation);

        setHasOptionsMenu(true);

       // LokacartApplication application = (LokacartApplication) getActivity().getApplication();
       // mTracker = application.getDefaultTracker();
       // mTracker.setScreenName("Fragment~" + name);
       // mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View changeOrganisationFragmentView;
        if (!Master.isNetworkAvailable(getActivity()))
        {
            changeOrganisationFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        }
        else
        {
            changeOrganisationFragmentView = inflater.inflate(R.layout.fragment_organisation, container, false);
            dbHelper = DBHelper.getInstance(getActivity());
            mRecyclerView = (RecyclerView) changeOrganisationFragmentView.findViewById(R.id.organisationRecyclerView);
            tOrganisationZero = (TextView) changeOrganisationFragmentView.findViewById(R.id.tOrganisationZero);
            bChangeOrganisation = (Button) changeOrganisationFragmentView.findViewById(R.id.bChangeOrganisation);
            bLeaveOrganisation = (Button) changeOrganisationFragmentView.findViewById(R.id.bLeaveOrganisation);

            ivNoData = (ImageView) changeOrganisationFragmentView.findViewById(R.id.ivNoData);
            ivNoData.setVisibility(View.GONE);

            tNoData = (TextView) changeOrganisationFragmentView.findViewById(R.id.tNoData);
            tNoData.setVisibility(View.GONE);

            ivNoInternet = (ImageView) changeOrganisationFragmentView.findViewById(R.id.ivNoInternet);
            ivNoInternet.setVisibility(View.GONE);

            tOops = (TextView) changeOrganisationFragmentView.findViewById(R.id.tvOops);
            tOops.setVisibility(View.GONE);

            tNoInternet = (TextView) changeOrganisationFragmentView.findViewById(R.id.tvNoInternet);
            tNoInternet.setVisibility(View.GONE);

            mRecyclerView.setHasFixedSize(true);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

            mRecyclerView.setLayoutManager(mLayoutManager);

            bChangeOrganisation.setOnClickListener(this);
            bLeaveOrganisation.setOnClickListener(this);

            new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE, getString(R.string.pd_chk_internet_connection),OrganisationFragment.this,"Organisation_Internet" ).execute();

        }

        return changeOrganisationFragmentView;
    }


    @Override
    public void onResume() {
        super.onResume();


        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();

       // LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        //mTracker = application.getDefaultTracker();
      //  mTracker.setScreenName("Fragment~" + name);
      //  mTracker.send(new HitBuilders.ScreenViewBuilder().build());


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.bLeaveOrganisation:

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(false);
                builder.setMessage(R.string.alert_Do_you_want_terminate_this_organization_memebership);
                builder.setTitle(Organisations.organisationList.get(OrganisationAdapter.getLastCheckedPos()).getName());

                builder.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                                getString(R.string.pd_chk_internet_connection),OrganisationFragment.this,"LeaveOrg_Internet" ).execute();


                    }
                });
                builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

                break;

            case R.id.bChangeOrganisation:

                for (int i = 0; i < Organisations.organisationList.size(); ++i) {
                    if (Organisations.organisationList.get(i).getIsChecked()) {

                        if (Organisations.organisationList.get(i).getName().equals(MemberDetails.getSelectedOrgName())) {
                            Toast.makeText(getActivity(), R.string.toast_no_changes_to_save, Toast.LENGTH_LONG).show();
                        } else {
                            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                                    Organisations.organisationList.get(i).getName(),
                                    Organisations.organisationList.get(i).getOrgabbr());

                            dbHelper.getProfile();


                            MemberDetails.setSelectedOrgAbbr(Organisations.organisationList.get(i).getOrgabbr());
                            MemberDetails.setSelectedOrgName(Organisations.organisationList.get(i).getName());
                            Master.CART_ITEM_COUNT = dbHelper.getCartItemsCount(MemberDetails.getMobileNumber(),
                                    MemberDetails.getSelectedOrgAbbr());
                            getActivity().invalidateOptionsMenu();
                            Toast.makeText(getActivity(), R.string.toast_organisation_changed_successfully, Toast.LENGTH_LONG).show();
                        }

                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void getData(String object, String tag)
    {
        if (OrganisationFragment.this.isAdded())
        {
            switch (tag)

            {

                case "Organisation_Internet":


                    if(object.equals("success"))
                    {
                        if (bChangeOrganisation != null)
                        {
                            bChangeOrganisation.setVisibility(View.GONE);
                            bLeaveOrganisation.setVisibility(View.GONE);

                            try
                            {
                                JSONObject obj = new JSONObject();
                                obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                                obj.put(Master.PASSWORD, MemberDetails.getPassword());
                                new NetworkAsyncTask
                                        (
                                                Master.getLoginURL(),
                                                "getOrganisations",
                                                Master.DIALOG_TRUE,
                                                getString(R.string.pd_fetching_data_from_server),
                                                OrganisationFragment.this,
                                                obj,
                                                Master.POST,
                                                Master.AUTH_FALSE,
                                                null,
                                                null
                                        ).execute();
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    else
                    {
                        ivNoInternet.setVisibility(View.VISIBLE);
                        tOops.setVisibility(View.VISIBLE);
                        tNoInternet.setVisibility(View.VISIBLE);

                        tNoData.setVisibility(View.GONE);
                        ivNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                        tOrganisationZero.setVisibility(View.GONE);
                        bChangeOrganisation.setVisibility(View.GONE);
                        bLeaveOrganisation.setVisibility(View.GONE);

                    }
                    break;

                case "LeaveOrg_Internet":
                    if(object.equals("success"))
                    {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put(Master.ORG_ABBR, Organisations.organisationList.get(OrganisationAdapter.getLastCheckedPos()).getOrgabbr());
                            jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());

                            position = OrganisationAdapter.getLastCheckedPos();

                            new NetworkAsyncTask
                                    (
                                            Master.getLeaveOrganisationURL(),
                                            "leaveOrganisation",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_terminating_your_membership),
                                            OrganisationFragment.this,
                                            jsonObject,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {

                        ivNoInternet.setVisibility(View.VISIBLE);
                        tOops.setVisibility(View.VISIBLE);
                        tNoInternet.setVisibility(View.VISIBLE);

                        tNoData.setVisibility(View.GONE);
                        ivNoData.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.GONE);
                        tOrganisationZero.setVisibility(View.GONE);
                        bChangeOrganisation.setVisibility(View.GONE);
                        bLeaveOrganisation.setVisibility(View.GONE);
                    }
                    break;


                case "getOrganisations":

                {
                    if (object.equals("exception"))
                    {
                        tNoData.setVisibility(View.VISIBLE);
                        ivNoData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                        tOrganisationZero.setVisibility(View.GONE);
                        bChangeOrganisation.setVisibility(View.GONE);
                        bLeaveOrganisation.setVisibility(View.GONE);
                    }
                    else
                    {
                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, object);
                        Organisations.organisationList = new ArrayList<>();


                        Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(),object);


                        if (Organisations.organisationList.size() == 0)
                        {
                            mRecyclerView.setVisibility(View.GONE);
                            tOrganisationZero.setVisibility(View.VISIBLE);
                            bChangeOrganisation.setVisibility(View.GONE);
                            bLeaveOrganisation.setVisibility(View.GONE);
                            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(), "null", "null");
                        }
                        else
                        {

                            if (Organisations.organisationList.size() == 1)
                            {
                                bLeaveOrganisation.setVisibility(View.GONE);
                                bChangeOrganisation.setVisibility(View.GONE);
                            } else
                            {
                                bLeaveOrganisation.setVisibility(View.VISIBLE);
                                bChangeOrganisation.setVisibility(View.VISIBLE);
                            }
                            String org[];
                            org = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());



                            for (int i = 0; i < Organisations.organisationList.size(); ++i)
                            {
                                if (Organisations.organisationList.get(i).getName().equals(org[1]))
                                {
                                    Organisations.organisationList.get(i).setIsChecked(true);
                                    break;
                                }
                            }

                            setOrg();

                            mAdapter = new OrganisationAdapter(getActivity());
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    }
                }

                break;

                case "leaveOrganisation":

                {
                    try {
                        JSONObject responseObject = new JSONObject(object);
                        if (object.equals("exception")) {
                            Toast.makeText(getActivity(), R.string.alert_unable_to_connect_server, Toast.LENGTH_LONG).show();

                        } else if (responseObject.getString(Master.RESPONSE).equals("Membership removed")
                                || responseObject.getString(Master.RESPONSE).equals("error")) {
                            Toast.makeText(getActivity(), R.string.toast_Membership_terminated_successfully, Toast.LENGTH_LONG).show();

                            dbHelper.deleteCart(MemberDetails.getMobileNumber(), Organisations.organisationList.get(position).getOrgabbr());

                            String org[];
                            org = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

                            if (org[1].equals(Organisations.organisationList.get(position).getName())) {
                                Organisations.organisationList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                Organisations.organisationList.get(0).setIsChecked(true);
                                mAdapter.notifyItemChanged(0);

                                dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                                        Organisations.organisationList.get(0).getName(),
                                        Organisations.organisationList.get(0).getOrgabbr());
                            } else {
                                Organisations.organisationList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                Organisations.organisationList.get(0).setIsChecked(true);
                                mAdapter.notifyItemChanged(0);
                            }

                            setOrg();

                            if (Organisations.organisationList.size() == 1)
                            {
                                bLeaveOrganisation.setVisibility(View.GONE);
                                bChangeOrganisation.setVisibility(View.GONE);
                            } else {
                                bLeaveOrganisation.setVisibility(View.VISIBLE);
                                bChangeOrganisation.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString(Master.RESPONSE).equals("Cannot be deleted")) {
                            Toast.makeText(getActivity(), R.string.toast_Membership_cannot_be_terminated, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.alert_unable_to_connect_server, Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

                default:
                    break;

            }
        }


    }


    private void setOrg() {
        boolean flag = false;
        for (int i = 0; i < Organisations.organisationList.size(); ++i)
        {
            if (Organisations.organisationList.get(i).getIsChecked())
            {
                flag = true;
                break;
            }
        }

        if (!flag)
        {
            dbHelper.setSelectedOrg(MemberDetails.getMobileNumber(),
                    Organisations.organisationList.get(0).getName(),
                    Organisations.organisationList.get(0).getOrgabbr());

            Master.isMember = true;

            Organisations.organisationList.get(0).setIsChecked(true);
        }
    }

}
