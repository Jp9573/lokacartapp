package com.mobile.ict.cart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Orders;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.adapter.PlacedOrderAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.EditDeletePlacedOrderInterface;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DeliveredOrderFragment extends Fragment implements EditDeletePlacedOrderInterface, GetResponse {

    private SwipeRefreshLayout swipeContainer;
    private List<Orders> ordersList;
    private RecyclerView mRecyclerView;
    private PlacedOrderAdapter mAdapter;
    private int orderId;
    private int lastpos = 0;
    private LinearLayout emptyCartLinearLayout;
    private RelativeLayout noDataRelativeLayout,noInternetRelativeLayout;
    private DBHelper dbHelper;

    LayoutInflater _inflater;
    ViewGroup _container;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.title_fragment_delivered_order);

           _inflater=inflater;
           _container=container;
        View deliveredOrderFragmentView;
        if (!Master.isNetworkAvailable(getActivity()))
        {
            deliveredOrderFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
        }
        else
        {
            dbHelper = DBHelper.getInstance(getActivity());

            deliveredOrderFragmentView = inflater.inflate(R.layout.fragment_delivered_order, container, false);
            mRecyclerView = (RecyclerView) deliveredOrderFragmentView.findViewById(R.id.rvOrder);
            emptyCartLinearLayout = (LinearLayout) deliveredOrderFragmentView.findViewById(R.id.cartEmptyLinearLayout);
            noDataRelativeLayout = (RelativeLayout) deliveredOrderFragmentView.findViewById(R.id.noDataRelativeLayout);
            noInternetRelativeLayout = (RelativeLayout) deliveredOrderFragmentView.findViewById(R.id.noInternetRelativeLayout);
            noInternetRelativeLayout.setVisibility(View.GONE);

            swipeContainer = (SwipeRefreshLayout) deliveredOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection),DeliveredOrderFragment.this,"DeliveredOrder_Internet" ).execute();


                }
            });
            swipeContainer.setColorSchemeResources(
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light,
                    android.R.color.holo_blue_bright);
        }

        return deliveredOrderFragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (Master.isNetworkAvailable(getActivity())) {
            if (mRecyclerView != null)
            {

                new NetworkAsyncTask(Master.checkInternetURL,Master.DIALOG_TRUE,
                        getString(R.string.pd_chk_internet_connection),DeliveredOrderFragment.this,"DeliveredOrder_Internet" ).execute();



            }
        }
    }


    private List<Orders> getOrders(JSONArray jsonArray) {

        ordersList = new ArrayList<>();

        int size = jsonArray.length();

        if (size != 0)

        {

            mRecyclerView.setVisibility(View.VISIBLE);
            emptyCartLinearLayout.setVisibility(View.GONE);
            noDataRelativeLayout.setVisibility(View.GONE);


            for (int i = 0; i < size; i++) {

                try {

                    Orders orders = new Orders((JSONObject) jsonArray.get(i), i, Master.DELIVEREDORDER);
                    orders.getDeliveredOrderItemsList(i);
                    ordersList.add(orders);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        } else {

            emptyCartLinearLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            noDataRelativeLayout.setVisibility(View.GONE);

        }

        return ordersList;


    }


    @Override
    public void deletePlacedOrder()

    {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (ordersList != null)
        {

            ordersList.clear();
        }



    }

    @Override
    public void getData(String response, String tag) {
        if (DeliveredOrderFragment.this.isAdded())
        {

            switch(tag)
            {
                case "DeliveredOrder_Internet":
                      if(response.equals("success"))
                      {
                          new NetworkAsyncTask
                                  (
                                          Master.getDeliveredOrderURL(MemberDetails.getSelectedOrgAbbr(), MemberDetails.getMobileNumber()),
                                          "getDeliveredOrders",
                                          Master.DIALOG_TRUE,
                                          getString(R.string.pd_fetching_orders),
                                          DeliveredOrderFragment.this,
                                          null,
                                          Master.GET,
                                          Master.AUTH_TRUE,
                                          MemberDetails.getEmail(),
                                          MemberDetails.getPassword()
                                  ).execute();


                          RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                          mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
                              @Override
                              public boolean onTouch(View v, MotionEvent event) {
                                  v.getParent().requestDisallowInterceptTouchEvent(true);
                                  return false;
                              }
                          });
                          mRecyclerView.setLayoutManager(mLayoutManager);
                      }
                    else
                      {
                          noInternetRelativeLayout.setVisibility(View.VISIBLE);
                          mRecyclerView.setVisibility(View.GONE);
                          noDataRelativeLayout.setVisibility(View.GONE);
                          swipeContainer.setRefreshing(false);
                      }

                     break;

                case "getDeliveredOrders":
                    if (response.equals("exception"))
                    {
                        mRecyclerView.setVisibility(View.GONE);
                        emptyCartLinearLayout.setVisibility(View.GONE);
                        noDataRelativeLayout.setVisibility(View.VISIBLE);
                        swipeContainer.setRefreshing(false);

                    } else
                    {
                        try {


                            JSONObject orderList = new JSONObject(response);
                            JSONArray ordersArray = orderList.getJSONArray("orders");


                            ordersList = getOrders(ordersArray);


                            mAdapter = new PlacedOrderAdapter(getActivity(), ordersList, DeliveredOrderFragment.this, Master.DELIVEREDORDER);
                            mRecyclerView.setHasFixedSize(true);
                            mRecyclerView.setAdapter(mAdapter);

                            mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
                                final List<? extends ParentListItem> parentItemList = mAdapter.getParentItemList();

                                @Override
                                public void onListItemExpanded(int position) {

                                    if (lastpos != position) {
                                        mAdapter.collapseParent(lastpos);
                                    }
                                    mRecyclerView.smoothScrollToPosition(position + parentItemList.get(position).getChildItemList().size());


                                    lastpos = position;
                                    orderId = ordersList.get(position).getOrder_id();

                                }

                                @Override
                                public void onListItemCollapsed(int position) {
                                }
                            });
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block

                            mRecyclerView.setVisibility(View.GONE);
                            emptyCartLinearLayout.setVisibility(View.GONE);
                            noDataRelativeLayout.setVisibility(View.VISIBLE);
                        }
                        swipeContainer.setRefreshing(false);
                    }
                      break;

                default:break;
            }


        }

    }


}
