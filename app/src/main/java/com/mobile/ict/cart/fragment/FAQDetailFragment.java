package com.mobile.ict.cart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import com.mobile.ict.cart.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FAQDetailFragment extends Fragment {

    private int position;
    private View rootView;

    public FAQDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt("position");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_faqdetail, container, false);

        CharSequence[] category = getResources().getTextArray(R.array.category_faqs_titles);

        getActivity().setTitle(category[position]);
        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView tvSubCategory = (TextView) rootView.findViewById(R.id.tvSubCategory);
        tvSubCategory.setMovementMethod(new ScrollingMovementMethod());

        CharSequence[] SubCategory = getResources().getTextArray(R.array.category_faqs_content);

        tvSubCategory.setText(SubCategory[position]);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ShowHamburger showHamburger = (ShowHamburger) getContext();
        showHamburger.showHamburgerIcon();
    }

    public interface ShowHamburger {
        void showHamburgerIcon();
    }
}
