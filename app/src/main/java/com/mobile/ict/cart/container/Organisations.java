package com.mobile.ict.cart.container;

import com.mobile.ict.cart.util.Master;

import org.json.JSONObject;

import java.util.ArrayList;

public class Organisations {

    private String name;
    private Boolean isChecked;
    private String orgabbr;

    public String getContact() {
        return contact;
    }

    private String contact;
    public static ArrayList<Organisations> organisationList;

    public Organisations(JSONObject object) {
        try {
            this.name = object.getString(Master.ORG_NAME);
            this.isChecked = false;
            this.orgabbr = object.getString(Master.ORG_ABBR);
            this.contact = object.getString(Master.ORG_CONTACT);

        } catch (Exception ignored) {
        }
    }

    public String getOrgabbr() {
        return orgabbr;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
