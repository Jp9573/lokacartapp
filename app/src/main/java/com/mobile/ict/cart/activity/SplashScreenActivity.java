package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;


import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;


public class SplashScreenActivity extends Activity implements GetResponse/*,ConnectionService.ConnectionServiceCallback*/ {


    private static int from;
    private static String goTo;

    private DBHelper dbHelper;

    private static JSONObject acceptReferralResponse;

    private boolean showFlag = true;


    Intent intent;
    String referralCode;

    private static boolean isValidEmail(@NonNull CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }

    @Override
    protected void onStart() {
        super.onStart();
        MemberDetails.setMemberDetails();
        dbHelper = DBHelper.getInstance(this);

        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

            SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, -1);

        }

        Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_splash_screen, Toast.LENGTH_LONG);
        toast.show();

        if (Master.isNetworkAvailable(this)) {

            new NetworkAsyncTask
                    (
                            Master.checkInternetURL,
                            Master.DIALOG_FALSE,
                            getString(R.string.pd_chk_internet_connection),
                            SplashScreenActivity.this,
                            "Branch_Internet"
                    ).execute();


        } else {

            if (SharedPreferenceConnector.readInteger(getApplicationContext(), -1) == 2) {
                forceUpdateDialog();
            } else if (SharedPreferenceConnector.readInteger(getApplicationContext(), -1) == 1) {

                showUpdateDialog2();
            } else {

                CheckAllDetailsPresent();


            }
        }


    }


    private void showUpdateDialog2() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                CheckAllDetailsPresent();


            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void CheckAllDetailsPresent() {
        dbHelper.getProfile();
        String fname, lname, address, pincode, mobileNumber;

        address = MemberDetails.getAddress();
        fname = MemberDetails.getFname();
        lname = MemberDetails.getLname();
        pincode = MemberDetails.getPincode();
        mobileNumber = MemberDetails.getMobileNumber();

        if (mobileNumber.equals("null")) {

            openTalkToUs();
        } else {
            if (address.equals("null") || fname.equals("null")
                    || lname.equals("null")
                    || pincode.equals("null")) {
                ShowStepper("profile");

            } else {
                ShowStepper("dashboard");

            }
        }
    }

    private void Init() {

        try {


            Branch branch = Branch.getInstance();

            branch.initSession(new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(JSONObject referringParams, BranchError error) {


                    if (error == null) {

                        try {
                            JSONObject rootJsonObject = new JSONObject(referringParams.toString());

                            boolean clickedBranchLink = rootJsonObject.getBoolean("+clicked_branch_link");

                            if (clickedBranchLink) {


                                String phoneNumber = rootJsonObject.getString("phonenumber").substring(2);
                                String email = rootJsonObject.getString("email");
                                referralCode = rootJsonObject.getString("referralCode");
                                String password = rootJsonObject.getString("token");

                                SharedPreferenceConnector.writeString(getApplicationContext(), Master.ReferralCodePref, referralCode);

                                if (isValidEmail(email) && referralCode != null && !referralCode.equals("")
                                        && password != null && !password.equals("") && phoneNumber != null && !phoneNumber.equals("")) {

                                    storeUserDetailsFromBranchData(new String[]{phoneNumber, email, password});

                                    if (Master.isNetworkAvailable(getApplicationContext())) {

                                        new NetworkAsyncTask(
                                                Master.checkInternetURL,
                                                Master.DIALOG_FALSE,
                                                getString(R.string.pd_chk_internet_connection),
                                                SplashScreenActivity.this,
                                                "AcceptReferral_Internet").execute();


                                    } else {

                                        CheckAllDetailsPresent();
                                    }


                                } else {

                                    CheckAllDetailsPresent();

                                }

                            } else {

                                dbHelper.getProfile();

                                if (Master.isNetworkAvailable(getApplicationContext())) {
                                    new NetworkAsyncTask(Master.checkInternetURL,
                                            Master.DIALOG_FALSE,
                                            getString(R.string.pd_chk_internet_connection),
                                            SplashScreenActivity.this,
                                            "CheckVersion_Internet").execute();


                                } else {

                                    CheckAllDetailsPresent();

                                }


                            }
                        } catch (JSONException e) {

                            CheckAllDetailsPresent();

                            e.printStackTrace();
                        }


                    } else {


                        showAdblockDialog();

                    }
                }
            }, this.getIntent().getData(), this);

        } catch (Exception e) {
            Intent i;
            i = new Intent(SplashScreenActivity.this, SplashScreenActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    private void showAdblockDialog() {
        if (this != null && !this.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_show_add_block_message)
                    .setTitle(R.string.dialog_show_add_block_tittle);
            builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dbHelper.getProfile();

                    if (Master.isNetworkAvailable(getApplicationContext())) {

                        new NetworkAsyncTask(
                                Master.checkInternetURL,
                                Master.DIALOG_FALSE,
                                getString(R.string.pd_chk_internet_connection),
                                SplashScreenActivity.this,
                                "CheckVersion_Internet").execute();


                    } else {

                        CheckAllDetailsPresent();


                    }

                }
            });
            builder.setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });

            builder.setCancelable(false);

            AlertDialog dialog = builder.create();
            dialog.show();

        }


    }

    private void openTalkToUs() {
        ShowStepper("talktous");
    }

    private void ShowStepper(@NonNull String nextPage) {
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

            Intent i = new Intent(getApplicationContext(), StepperActivity.class);
            i.putExtra("redirectto", nextPage);
            switch (nextPage) {
                case "profile": {
                    i.putExtra("showintro", "true");

                    break;
                }
                case "organisation": {
                    i.putExtra("showintro", "true");
                    break;
                }
                case "dashboard": {
                    i.putExtra("showintro", "true");
                    break;
                }
                case "talktous": {

                    break;
                }

                default:
                    break;

            }

            startActivity(i);
            finish();
        } else {
            goToActivity(nextPage);
        }
    }

    private void goToActivity(@NonNull String nextPage) {
        Intent i;
        if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.INTRO, Master.DEFAULT_INTRO)) {
            switch (nextPage) {
                case "profile": {

                    i = new Intent(SplashScreenActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation": {


                    i = new Intent(SplashScreenActivity.this, DemoLists.class);
                    i.putExtra("redirectto", "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard": {
                    i = new Intent(SplashScreenActivity.this, DemoLists.class);
                    i.putExtra("redirectto", "dashboard");
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous": {
                    i = new Intent(SplashScreenActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }

                default:
                    break;
            }

        } else {
            switch (nextPage) {
                case "profile": {
                    i = new Intent(SplashScreenActivity.this, ProfileActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "organisation": {

                    i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.putExtra("redirectto", "organisation");
                    startActivity(i);
                    finish();
                    break;
                }
                case "dashboard": {
                    i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }
                case "talktous": {
                    i = new Intent(SplashScreenActivity.this, LandingActivity.class);
                    startActivity(i);
                    finish();
                    break;
                }

                default:
                    break;
            }
        }

    }

    private void storeUserDetailsFromBranchData(String[] details) {


        dbHelper.getProfile();
        MemberDetails.setEmail(details[1]);
        MemberDetails.setPassword(details[2]);
        MemberDetails.setMobileNumber(details[0]);
        dbHelper.addProfile();


    }


    private void storeUserDetails(JSONObject rootJson) {


        String name, address, lastname, pincode, phonenumber, email, abbr, organisation, profilePicUrl;
        abbr = organisation = profilePicUrl = "null";


        try {

            name = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NAME_TAG);
            address = rootJson.getString(Master.ACCEPT_REFERRAL_USER_ADDRESS_TAG);
            lastname = rootJson.getString(Master.ACCEPT_REFERRAL_USER_LAST_NAME_TAG);
            pincode = rootJson.getString(Master.ACCEPT_REFERRAL_PINCODE_TAG);
            phonenumber = rootJson.getString(Master.ACCEPT_REFERRAL_USER_NUMBER_TAG);
            email = rootJson.getString(Master.ACCEPT_REFERRAL_USER_EMAIL_TAG);

            if (rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG) && rootJson.has(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG)) {
                abbr = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG);
                organisation = rootJson.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
            }

            if (rootJson.has(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL)) {
                profilePicUrl = rootJson.getString(Master.ACCEPT_REFERRAL_USER_PROFILE_PIC_URL);

            }

            MemberDetails.setProfileImageUrl(profilePicUrl);
            MemberDetails.setFname(name);
            MemberDetails.setAddress(address);
            MemberDetails.setLname(lastname);
            MemberDetails.setPincode(pincode);
            MemberDetails.setMobileNumber(phonenumber.substring(2));

            if (email.equals("null"))
                MemberDetails.setEmail(phonenumber + "@gmail.com");
            else
                MemberDetails.setEmail(email);

            MemberDetails.setSelectedOrgAbbr(abbr);
            MemberDetails.setSelectedOrgName(organisation);


            String selectedOrg[] = dbHelper.getSelectedOrg(MemberDetails.getMobileNumber());

            dbHelper.addProfile();


            if (abbr.equals("null") || organisation.equals("null")) {

                boolean orgFound = false;
                if (rootJson.has("memberships") && !selectedOrg[0].equals("null")) {
                    JSONArray memarr = rootJson.getJSONArray("memberships");
                    if (memarr.length() > 0) {
                        for (int i = 0; i < memarr.length(); i++) {

                            JSONObject org = memarr.getJSONObject(i);
                            String orgAbbr = org.getString("abbr");
                            if (orgAbbr.equals(selectedOrg[0])) {
                                orgFound = true;
                                break;
                            }

                        }
                    }
                }


                if (orgFound) {

                    dbHelper.setSelectedOrg(phonenumber.substring(2), selectedOrg[1], selectedOrg[0]);

                } else {


                    String organisation2Name = "";
                    String organisation2Abbr = "";


                    if (rootJson.has("memberships")) {
                        if (rootJson.getJSONArray("memberships").length() > 0) {
                            JSONArray organisationsList = rootJson.getJSONArray("memberships");
                            JSONObject org = organisationsList.getJSONObject(0);
                            organisation2Name = org.getString("organization");
                            organisation2Abbr = org.getString("abbr");

                        } else {
                            showFlag = false;

                            Toast.makeText(getApplicationContext(), R.string.toast_no_member_organizations, Toast.LENGTH_SHORT).show();
                            openTalkToUs();
                        }

                    } else {

                        showFlag = false;


                        Toast.makeText(getApplicationContext(), R.string.toast_no_member_organizations, Toast.LENGTH_SHORT).show();
                        openTalkToUs();
                    }

                    if (organisation2Name.equals("") || organisation2Name.equals("null")
                            || organisation2Abbr.equals("") || organisation2Abbr.equals("null")) {
                        dbHelper.setSelectedOrg(phonenumber.substring(2), organisation, abbr);
                    } else {
                        dbHelper.setSelectedOrg(phonenumber.substring(2), organisation2Name, organisation2Abbr);
                    }


                }


            } else {

                dbHelper.setSelectedOrg(phonenumber.substring(2), organisation, abbr);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void processResponseFromAcceptReferral(JSONObject response) {

        String acceptReferralResponseStatus = "";
        String detailsPresent = "";

        try {
            detailsPresent = response.getString(Master.ACCEPT_REFERRAL_DETAILS_PRESENT);
            acceptReferralResponseStatus = response.getString(Master.ACCEPT_REFERRAL_RESPONSE_TAG);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (acceptReferralResponseStatus) {
            case "success":

                try {
                    String organisationName = response.getString(Master.ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG);
                    Toast.makeText(getApplicationContext(), "You are now a member of " + organisationName + " organisation", Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
            case "Already a member":
                Toast.makeText(getApplicationContext(), R.string.toast_already_member_organizations, Toast.LENGTH_SHORT).show();


                break;
            default:

                break;
        }

        if (detailsPresent.equals("0")) {

            ShowStepper("organisation");

        } else {

            ShowStepper("profile");

        }

    }


    private void showRecommendUpdateDialog(int calledFrom) {
        from = calledFrom;


        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 1);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.recommend_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                if (from == 1) {
                    processResponseFromAcceptReferral(acceptReferralResponse);

                } else {
                    ShowStepper(goTo);
                }


            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void forceUpdateDialog() {


        SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 2);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        builder.setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void getData(String response, String tag) {

        switch (tag) {
            case "Branch_Internet":

                if (response.equals("success")) {
                    Init();
                } else {
                    if (SharedPreferenceConnector.readInteger(getApplicationContext(), -1) == 2) {
                        forceUpdateDialog();
                    } else if (SharedPreferenceConnector.readInteger(getApplicationContext(), -1) == 1) {

                        showUpdateDialog2();
                    } else {

                        CheckAllDetailsPresent();


                    }
                }


                break;

            case "AcceptReferral_Internet":

                if (response.equals("success")) {
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("referralcode", referralCode);
                        obj.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Master.version(getApplicationContext()));
                        new NetworkAsyncTask(
                                Master.getAcceptReferralURL(),
                                "acceptReferral",
                                Master.DIALOG_FALSE,
                                null,
                                SplashScreenActivity.this,
                                obj,
                                Master.POST,
                                Master.AUTH_FALSE,
                                null,
                                null
                        ).execute();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CheckAllDetailsPresent();
                }
                break;

            case "CheckVersion_Internet":
                if (response.equals("success")) {
                    JSONObject obj = new JSONObject();
                    String number = MemberDetails.getMobileNumber();
                    try {
                        if (!number.equals("null")) {
                            obj.put(Master.VERSION_CHECK_NEW_PHONE_NUMBER_TAG, "91" + number);
                        }

                        obj.put(Master.VERSION_CHECK_NEW_VERSION_TAG, Master.version(getApplicationContext()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    new NetworkAsyncTask(
                            Master.getVersionCheckNewURL(),
                            "checkVersion",
                            Master.DIALOG_FALSE,
                            null,
                            SplashScreenActivity.this,
                            obj,
                            Master.POST,
                            Master.AUTH_FALSE,
                            null,
                            null
                    ).execute();
                } else {
                    CheckAllDetailsPresent();

                }
                break;


            case "acceptReferral":

                if (response.equals("exception")) {
                    CheckAllDetailsPresent();
                } else {

                    SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.AcceptReferralFailedPref, 0);

                    try {
                        acceptReferralResponse = new JSONObject(response);
                        String responseValue = acceptReferralResponse.getString("response");

                        try {

                            String verifeidEmail = acceptReferralResponse.getString("emailVerify");
                            SharedPreferenceConnector.writeString(SplashScreenActivity.this.getApplicationContext(), Master.emailVerified, verifeidEmail);

                        } catch (Exception ignored) {

                        }
                        String mandatory = acceptReferralResponse.getString(Master.ACCEPT_REFERRAL_UPDATE_MANDATORY_TAG);


                        if (responseValue.equals("success")) {

                            storeUserDetails(acceptReferralResponse);

                        } else if (responseValue.equals("Already a member")) {

                            storeUserDetails(acceptReferralResponse);

                        }

                        switch (mandatory) {
                            case "2":

                                forceUpdateDialog();

                                break;
                            case "1":
                                showRecommendUpdateDialog(1);
                                break;
                            default:
                                SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 0);

                                processResponseFromAcceptReferral(acceptReferralResponse);

                                break;
                        }


                    } catch (JSONException e) {
                        openTalkToUs();
                        e.printStackTrace();
                    }
                }

                dbHelper.getProfile();
                break;

            case "checkVersion":

                if (response.equals("exception")) {

                    CheckAllDetailsPresent();


                } else {

                    try {
                        JSONObject responseJson = new JSONObject(response);

                        try {


                            String verifeidEmail = String.valueOf(responseJson.getInt("emailVerify"));
                            String image = responseJson.getString("image");
                            String link = responseJson.getString("link");
                            SharedPreferenceConnector.writeString(SplashScreenActivity.this.getApplicationContext(), Master.image, image);
                            SharedPreferenceConnector.writeString(SplashScreenActivity.this.getApplicationContext(), Master.link, link);
                            SharedPreferenceConnector.writeString(SplashScreenActivity.this, "showAdsDialog", "1");

                            SharedPreferenceConnector.writeString(SplashScreenActivity.this.getApplicationContext(), Master.emailVerified, verifeidEmail);

                        } catch (Exception ignored) {

                        }


                        storeUserDetails(responseJson);

                        if (showFlag) {


                            String updateValue = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG);

                            switch (updateValue) {
                                case "0": {

                                    SharedPreferenceConnector.writeInteger(getApplicationContext(), Master.UpdateRequiredPref, 0);


                                    if (responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {


                                        String flag;
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        if (flag.equals("0")) {

                                            ShowStepper("dashboard");
                                        } else {

                                            ShowStepper("profile");

                                        }
                                    } else {

                                        openTalkToUs();


                                    }
                                    break;
                                }
                                case "1": {

                                    if (responseJson.has(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG)) {

                                        String flag;
                                        flag = responseJson.getString(Master.VERSION_CHECK_NEW_RESPONSE_FLAG_TAG);
                                        if (flag.equals("0")) {

                                            goTo = "dashboard";

                                        } else {
                                            goTo = "profile";

                                        }

                                        showRecommendUpdateDialog(2);
                                    } else {
                                        openTalkToUs();

                                    }

                                    break;
                                }
                                case "2": {
                                    forceUpdateDialog();
                                    break;

                                }

                            }

                        }


                    } catch (JSONException e) {

                        CheckAllDetailsPresent();


                        e.printStackTrace();
                    }

                }
                dbHelper.getProfile();

                break;

            default:
                break;


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
    }


}