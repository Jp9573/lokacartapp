package com.mobile.ict.cart.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.interfaces.DeleteProductListener;
import com.mobile.ict.cart.util.Master;

import java.text.DecimalFormat;
import java.util.ArrayList;


@SuppressWarnings("ALL")
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.DataObjectHolder> {
    private Context context;
    DataObjectHolder rcv;
    TextView cartTotal,deliveryCharges,amountPayable;
    double sum = 0.0;
    DeleteProductListener deleteProductListener;


    public CartAdapter(Context context, TextView cartTotal,TextView deliveryCharges,TextView amountPayable) {
        this.context = context;
        this.cartTotal = cartTotal;
        this.deliveryCharges = deliveryCharges;
        this.amountPayable = amountPayable;
        this.deleteProductListener = (DeleteProductListener) context;

    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tProductName, tPrice,tGst, tItemTotal;
        ImageButton bDelete, bPlus, bMinus;
        EditText eQuantity;
        ImageView ivProduct;
        MyCustomEditTextListener myCustomEditTextListener;

        public DataObjectHolder(final View itemView, final Context context, MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);

            tProductName = (TextView) itemView.findViewById(R.id.tCartProductName);
            tPrice = (TextView) itemView.findViewById(R.id.tPrice);
            tGst = (TextView) itemView.findViewById(R.id.tGst);

            tItemTotal = (TextView) itemView.findViewById(R.id.tItemTotal);

            bDelete = (ImageButton) itemView.findViewById(R.id.bDelete);
            bPlus = (ImageButton) itemView.findViewById(R.id.bPlus);
            bMinus = (ImageButton) itemView.findViewById(R.id.bMinus);

            eQuantity = (EditText) itemView.findViewById(R.id.eQuantity);
            this.myCustomEditTextListener = myCustomEditTextListener;
        }
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_cart, parent, false);
        rcv = new DataObjectHolder(cardView, context, new MyCustomEditTextListener());
        return rcv;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.eQuantity.addTextChangedListener(holder.myCustomEditTextListener);
        holder.myCustomEditTextListener.updatePosition(position, holder.tItemTotal, holder.eQuantity, holder.bPlus, holder.bMinus, cartTotal,deliveryCharges,amountPayable);
        holder.tProductName.setText("" + Master.cartList.get(position).getName());
        holder.tPrice.setText("\u20B9" + Master.cartList.get(position).getUnitPrice());
        holder.tGst.setText(context.getResources().getString(R.string.textview_gst)+ " " + Master.cartList.get(position).getGst()+ context.getResources().getString(R.string.textview_percent));
        holder.tItemTotal.setText("\u20B9" + Master.cartList.get(position).getTotal());


        holder.eQuantity.setText("" + Master.cartList.get(position).getQuantity());


        if (Master.cartList.get(position).getQuantity() < 1) {
            holder.bMinus.setEnabled(false);
            holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
        }

        holder.bPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sqty = holder.eQuantity.getText().toString().trim();
                int dqty;
                if (sqty.equals(""))
                    dqty = 0;
                else
                    dqty = Integer.parseInt(sqty);


                dqty++;


                if (dqty >= 999) {
                    holder.eQuantity.setText("999");
                    holder.bPlus.setEnabled(false);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                } else {
                    holder.bPlus.setEnabled(true);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                    holder.eQuantity.setText("" + dqty);
                }

                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);


            }
        });

        holder.bMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.bPlus.setEnabled(true);
                holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);

                String sqty = holder.eQuantity.getText().toString().trim();

                int dqty = Integer.parseInt(sqty);

                if (dqty > 1)
                    holder.eQuantity.setText("" + --dqty);

                else if (dqty == 1) {
                    holder.eQuantity.setText("");
                    holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    holder.bMinus.setEnabled(false);
                }

            }
        });


        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProductListener.deleteProduct(position, Master.cartList.get(position).getID());
            }
        });


        if (Master.cartList.get(position).getImageUrl() == "null") {
            Glide.with(context)
                    .load(Master.cartList.get(position).getImageUrl())
                    .placeholder(R.drawable.placeholder_products)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(holder.ivProduct);
        } else {
            Glide.with(context)
                    .load(Master.cartList.get(position).getImageUrl())
                    .placeholder(R.drawable.placeholder_products)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(holder.ivProduct);
        }

    }

    @Override
    public int getItemCount() {
        return Master.cartList.size();
    }


    public static ArrayList<Product> getList() {


        return Master.cartList;
    }


    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        TextView itotal;
        EditText eqty;
        TextView cartTotal,deliveryCharges,amountPayable;
        ImageButton bPlus, bMinus;

        public void updatePosition(int position, TextView textView, EditText editText, ImageButton bPlus, ImageButton bMinus, TextView cartTotal,TextView deliveryCharges,TextView amountPayable) {
            this.position = position;
            itotal = textView;
            eqty = editText;
            this.bPlus = bPlus;
            this.bMinus = bMinus;
            this.cartTotal = cartTotal;
            this.deliveryCharges = deliveryCharges;
            this.amountPayable = amountPayable;
        }


        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {


        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (!editable.toString().equals("")) {
                String total;
                try {

                    bMinus.setEnabled(true);
                    bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);

                    if (Integer.parseInt(editable.toString()) > 999) {
                        eqty.setText("999");
                        bPlus.setEnabled(false);
                        bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                        total = String.format("%.2f", Master.cartList.get(position).getUnitPrice() * 999);
                        Master.cartList.get(position).setQuantity(999);

                    } else if (Integer.parseInt(editable.toString()) == 0) {
                        bMinus.setEnabled(false);
                        bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                        bPlus.setEnabled(true);
                        bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                        total = String.format("%.2f", Master.cartList.get(position).getUnitPrice() * Integer.parseInt(editable.toString()));
                        Master.cartList.get(position).setQuantity(Integer.parseInt(editable.toString()));
                    } else {
                        bPlus.setEnabled(true);
                        bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                        total = String.format("%.2f", Master.cartList.get(position).getUnitPrice() * Integer.parseInt(editable.toString()));
                        Master.cartList.get(position).setQuantity(Integer.parseInt(editable.toString()));
                    }


                    Master.cartList.get(position).setTotal(Double.parseDouble(total));
                    itotal.setText("\u20B9" + Master.cartList.get(position).getTotal());


                    sum = 0.0;
                    for (int i = 0; i < Master.cartList.size(); i++) {
                        sum = sum + Master.cartList.get(i).getTotal();
                    }

                    DecimalFormat df = new DecimalFormat("0.00");

                    int tot = (int)Math.round(sum);


                    cartTotal.setText("" + df.format(tot));

                    deliveryCharges.setText(""+Master.DELIVERY_CHARGES);

                   // int amountPayable = Master.DELIVERY_CHARGES + df.format(tot);


                    tot = tot+ Master.DELIVERY_CHARGES;


                    amountPayable.setText(""+df.format(tot));

                    //cartTotal.setText("" + String.format("%.2f", sum));


                } catch (NumberFormatException ignored) {
                }
            } else {
                Master.cartList.get(position).setQuantity(0);
                Master.cartList.get(position).setTotal(0.0);
                itotal.setText("\u20B9" + Master.cartList.get(position).getTotal());


                sum = 0.0;

                for (int i = 0; i < Master.cartList.size(); i++) {
                    sum = sum + Master.cartList.get(i).getTotal();
                }

                DecimalFormat df = new DecimalFormat("0.00");

                int tot = (int)Math.round(sum);




                cartTotal.setText("" + df.format(tot));

                deliveryCharges.setText(""+Master.DELIVERY_CHARGES);


                tot = tot+ Master.DELIVERY_CHARGES;


                amountPayable.setText(""+df.format(tot));

                //cartTotal.setText("" + String.format("%.2f", sum));

                bMinus.setEnabled(false);
                bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);

                bPlus.setEnabled(true);
                bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            }

        }
    }

}