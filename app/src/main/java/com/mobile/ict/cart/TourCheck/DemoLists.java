package com.mobile.ict.cart.TourCheck;


import android.content.Intent;

import android.os.Bundle;

import android.support.v4.app.FragmentManager;

import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;

import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.activity.ProfileActivity;
import com.mobile.ict.cart.R;

import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import java.util.ArrayList;

public class DemoLists extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Master.productList = new ArrayList<>();
        Master.getJSON = GetJSON.getInstance();

        setContentView(R.layout.activity_demo_dashboard);


        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.content_frame, new DemoProductFragment(), Master.PRODUCT_TAG).commit();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.skip, menu);


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.skip:
                SharedPreferenceConnector.writeBoolean(this, Master.INTRO, false);


                if (getIntent().hasExtra("redirectto")) {
                    String redirectto = getIntent().getStringExtra("redirectto");
                    Intent i;
                    switch (redirectto) {

                        case "profile": {
                            i = new Intent(this, ProfileActivity.class);
                            startActivity(i);
                            finish();
                            break;
                        }
                        case "organisation": {

                            i = new Intent(this, DashboardActivity.class);
                            i.putExtra("redirectto", "organisation");
                            startActivity(i);
                            finish();
                            break;
                        }
                        case "dashboard": {
                            i = new Intent(this, DashboardActivity.class);
                            i.putExtra("redirectto", redirectto);
                            startActivity(i);
                            finish();
                            break;
                        }
                    }


                } else {

                    startActivity(new Intent(this, DashboardActivity.class));
                    finish();
                }

                return true;
            default:

                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onBackPressed() {

        System.exit(0);


    }

}