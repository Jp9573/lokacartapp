package com.mobile.ict.cart.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.JSONDataHelper;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ALL")
public class FeedbackFragment extends Fragment implements GetResponse {


    final static String Lokacart_Tag = "Lokacart Team";
    int position = 0;
    ArrayAdapter<String> adapter;
    List<String> list;
    EditText feedbackEditText;
    TextView charRemainingTV;
    String name = "Feedback";
    Button submit;
    Spinner spinner;
    String Orgabbr;
    String message;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // LokacartApplication application = (LokacartApplication) getActivity().getApplication();
        // Tracker mTracker = application.getDefaultTracker();
        // mTracker.setScreenName("Fragment~" + name);
        // mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        setHasOptionsMenu(true);

        getActivity().setTitle("Feedback");

        charRemainingTV = (TextView) view.findViewById(R.id.char_remaining_TV);
        submit = (Button) view.findViewById(R.id.submit_button);
        spinner = (Spinner) view.findViewById(R.id.planets_spinner);
        feedbackEditText = (EditText) view.findViewById(R.id.feedback_edittext);

        list = new ArrayList<String>();
        list.add(getActivity().getString(R.string.spinner_feedback_loading_organisations));


        if (Master.isNetworkAvailable(getActivity())) {
            new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                    getString(R.string.pd_chk_internet_connection), FeedbackFragment.this, "Organisation_Internet").execute();


        } else {

            Toast.makeText(getActivity(), getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();
            submit.setEnabled(false);

        }


        feedbackEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        getActivity().getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        feedbackEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != -1)
                    charRemainingTV.setText((500 - s.length()) + " " + getActivity().getString(R.string.pd_submitting_feedback_char_remaining));
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message = feedbackEditText.getText().toString();


                if (message.trim().equals("")
                        || message.trim().isEmpty()) {

                    Toast.makeText(getActivity(), getString(R.string.toast_feedback_enter_feedback_before_submitting), Toast.LENGTH_LONG).show();

                } else if (message.equals(getString(R.string.spinner_feedback_loading_organisations)) || message.equals(getString(R.string.spinner_feedback_error_loading_organisations))) {

                    Toast.makeText(getActivity(), getString(R.string.toast_feedback_error_loading_org), Toast.LENGTH_LONG).show();


                } else if (message.equals(getString(R.string.spinner_feedback_select_organisation))) {
                    Toast.makeText(getActivity(), getString(R.string.toast_feedback_select_organisation), Toast.LENGTH_LONG).show();

                } else if (position == 0) {
                    Toast.makeText(getActivity(), getString(R.string.toast_feedback_select_organisation), Toast.LENGTH_LONG).show();
                } else {


                    if (list.get(position).equals(Lokacart_Tag)) {
                        Orgabbr = "lcart";

                    } else {
                        Orgabbr = Organisations.organisationList.get(position - 1).getOrgabbr();
                    }

                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection), FeedbackFragment.this, "Feedback_Internet").execute();


                }


            }
        });

        @DrawableRes int spinnerbg = R.mipmap.downw_arrow;
        spinner.setBackgroundResource(spinnerbg);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                position = pos;
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.rgb(0, 0, 0));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        @DrawableRes int editTextShape = R.drawable.edittextshape;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {

            spinner.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), editTextShape, null));
            feedbackEditText.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), editTextShape, null));

        } else {
            spinner.setBackground(ResourcesCompat.getDrawable(getResources(), editTextShape, null));
            feedbackEditText.setBackground(ResourcesCompat.getDrawable(getResources(), editTextShape, null));

        }


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, list);

        spinner.setAdapter(adapter);
        return view;

    }

   /* protected void AnalyticsEvent(String categoryId, String actionId) {

        Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .build());
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void getData(String object, String tag) {
        if (FeedbackFragment.this.isAdded()) {
            switch (tag) {

                case "Organisation_Internet":
                    if (object.equals("success")) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                            obj.put(Master.PASSWORD, MemberDetails.getPassword());

                            new NetworkAsyncTask
                                    (
                                            Master.getLoginURL(),
                                            "organisations",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_sending_data_to_server),
                                            FeedbackFragment.this,
                                            obj,
                                            Master.POST,
                                            Master.AUTH_FALSE,
                                            null,
                                            null
                                    ).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();
                        submit.setEnabled(false);
                    }

                    break;

                case "Feedback_Internet":
                    if (object.equals("success")) {

                        try {
                            JSONObject obj = new JSONObject();
                            obj.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                            obj.put(Master.DEFAULT_ORG_ABBR, Orgabbr);
                            obj.put(Master.feedbackText, message);


                            new NetworkAsyncTask
                                    (
                                            Master.getSendCommentURL(),
                                            "feedback",
                                            Master.DIALOG_TRUE,
                                            getString(R.string.pd_submitting_feedback),
                                            FeedbackFragment.this,
                                            obj,
                                            Master.POST,
                                            Master.AUTH_TRUE,
                                            MemberDetails.getEmail(),
                                            MemberDetails.getPassword()
                                    ).execute();


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.toast_Please_check_internet_connection), Toast.LENGTH_LONG).show();
                    }
                    break;

                case "organisations":

                    if (object.equals("exception")) {
                        list.clear();
                        list.add(getString(R.string.spinner_feedback_error_loading_organisations));
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_LONG).show();

                    } else {

                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, object);
                        Organisations.organisationList = new ArrayList<Organisations>();
                        Organisations.organisationList = JSONDataHelper.getOrganisationListFromJson(getActivity(), SharedPreferenceConnector
                                .readString(getActivity().getApplicationContext(), Master.LOGIN_JSON, Master.DEFAULT_LOGIN_JSON));
                        list.clear();

                        if (Organisations.organisationList.size() > 0) {
                            list.add(getString(R.string.spinner_feedback_select_organisation));
                            for (int i = 0; i < Organisations.organisationList.size(); ++i) {

                                list.add(Organisations.organisationList.get(i).getName());

                            }
                            list.add(Lokacart_Tag);
                            adapter.notifyDataSetChanged();
                        } else {
                            list.add(getString(R.string.spinner_feedback_error_loading_organisations));
                            adapter.notifyDataSetChanged();
                        }

                    }
                    break;

                case "feedback":

                    if (object.equals("exception")) {
                        Toast.makeText(getActivity(), getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.pd_submitting_feedback_success), Toast.LENGTH_LONG).show();
                        feedbackEditText.setText("");
                        charRemainingTV.setText(500 + getString(R.string.pd_submitting_feedback_char_remaining));
                        SharedPreferenceConnector.writeString(getActivity().getApplicationContext(), Master.LOGIN_JSON, object);
                        Master.hideSoftKeyboard(getActivity());

                        //DashboardActivity.updateDrawerPosition();
                        UpdateDrawer updateDrawer = (UpdateDrawer) getContext();
                        updateDrawer.updateDrawerPosition();
                    }

                    break;

                default:
                    break;

            }
        }

    }

    public interface UpdateDrawer {
        void updateDrawerPosition();
    }

}