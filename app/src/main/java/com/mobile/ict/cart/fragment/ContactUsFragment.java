package com.mobile.ict.cart.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.Master;


public class ContactUsFragment extends DialogFragment implements View.OnClickListener {

    private long mLastClickTime = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       // LokacartApplication application = (LokacartApplication) getActivity().getApplication();
       // Tracker mTracker = application.getDefaultTracker();
        String name = "Contact";
      //  mTracker.setScreenName("Fragment~" + name);
      //  mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        TextView faqTV = (TextView) view.findViewById(R.id.faq_string_TV);
        ImageView facebookIV = (ImageView) view.findViewById(R.id.facebook_iv);
        ImageView callUsIV = (ImageView) view.findViewById(R.id.call_us_iv);
        ImageView websiteIV = (ImageView) view.findViewById(R.id.website_iv);
        ImageView mailUsIV = (ImageView) view.findViewById(R.id.mail_us_iv);

        facebookIV.setOnClickListener(this);
        callUsIV.setOnClickListener(this);
        websiteIV.setOnClickListener(this);
        mailUsIV.setOnClickListener(this);

        faqTV.setOnClickListener(this);
        Button closeDialogFragmentTV = (Button) view.findViewById(R.id.close_dailof_fragment_tv);

        closeDialogFragmentTV.setOnClickListener(this);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    @Override
    public void onClick(View view) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {

            case R.id.call_us_iv: {

                //AnalyticsEvent("Call");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+912225764974"));
                startActivity(intent);
                break;

            }
            case R.id.mail_us_iv: {

               // AnalyticsEvent("Email");
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "lokacart@cse.iitb.ac.in", null));

                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;

            }
            case R.id.website_iv: {

               // AnalyticsEvent("Website");
                String url = "http://ruralict.cse.iitb.ac.in/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            }
            case R.id.facebook_iv: {

               // AnalyticsEvent("SocialMedia");
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/RuralICT.iitb/"));
                startActivity(i);
                break;

            }

            case R.id.close_dailof_fragment_tv: {
                getDialog().dismiss();
                break;

            }

            case R.id.faq_string_TV: {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FAQFragment(), Master.FAQ_TAG).commit();
                getDialog().dismiss();
                break;

            }
        }
    }

   /* private void AnalyticsEvent(String actionId) {
        Tracker t = ((LokacartApplication) getActivity().getApplication()).getTracker(
                LokacartApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Contact")
                .setAction(actionId)
                .build());
    }
*/

}
