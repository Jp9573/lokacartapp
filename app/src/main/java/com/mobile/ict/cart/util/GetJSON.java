package com.mobile.ict.cart.util;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

import org.json.JSONObject;

import android.util.Base64;


public class GetJSON {
    private static GetJSON getJSON;

    public static GetJSON getInstance() {

        if (getJSON == null) {
            getJSON = new GetJSON();
        }
        return getJSON;
    }

    private GetJSON() {
    }


    public String getJSONFromUrl(String url, JSONObject obj, String method, boolean auth, String emailid, String password) {


        String status;

        InputStream is;
        HttpURLConnection linkConnection;
        try {

            URL linkurl = new URL(url);

            linkConnection = (HttpURLConnection) linkurl.openConnection();

            if (auth) {

                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
            }

            linkConnection.setDefaultUseCaches(false);


            if (method.equals("GET")) {
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "application/json");
                linkConnection.setDoInput(true);
            }

            if (method.equals("POST")) {
                linkConnection.setRequestMethod("POST");
                linkConnection.setRequestProperty("Content-Type", "application/json");
                linkConnection.setRequestProperty("Accept", "application/json");
                linkConnection.setDoOutput(true);
                linkConnection.setDoInput(true);

                String o = obj.toString();


                OutputStreamWriter w = new OutputStreamWriter(linkConnection.getOutputStream());
                w.write(o);
                w.flush();
                w.close();
            }


            status = String.valueOf(linkConnection.getResponseCode());


            if (status.equals("200")) {
                is = linkConnection.getInputStream();
            } else {
                status = "exception";
                return status;
            }


        } catch (Exception e) {
            e.printStackTrace();
            status = "exception";
            return status;
        }


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            status = sb.toString();


            try {
                new JSONObject(status);
                return status;
            } catch (Exception e) {
                status = "exception";
                return status;
            }


        } catch (Exception ignored) {


        } finally {
            if (linkConnection != null) {
                linkConnection.disconnect();
            }
        }

        return status;
    }


    public String getInternetActiveStatus(String url) {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");


            if (ipAddr.equals("")) {
                return "failure";
            } else {
                return "success";
            }

        } catch (Exception e) {
            return "failure";
        }


    }


}
