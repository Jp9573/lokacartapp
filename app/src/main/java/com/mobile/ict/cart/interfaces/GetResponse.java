package com.mobile.ict.cart.interfaces;

public interface GetResponse {
    void getData(String response, String tag);
}
