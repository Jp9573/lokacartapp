package com.mobile.ict.cart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.Toast;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.Validation;
import org.json.JSONException;
import org.json.JSONObject;

public class TalkToUsActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener , GetResponse{

    private EditText eName;
    private EditText eEmail;
    private EditText eMobileNumber;
    private EditText eMessage;
    private EditText eAddress1;
    private EditText eAddress2;
    private static Boolean backPress;
    private BottomSheetBehavior behavior;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.talk_to_us_menu, menu);
        MenuItem infoMenuItem = menu.findItem(R.id.info);


        infoMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                if(behavior.getState() == 4){
                    behavior.setState(3);
                }else{
                    behavior.setState(4);
                }


                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {

                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        backPress = false;
        setContentView(R.layout.activity_talk_to_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(2);
        Button aboutUsButton = (Button) bottomSheet.findViewById(R.id.button_info_about_us);
        Button tncButton = (Button) bottomSheet.findViewById(R.id.button_info_tnc);
        Button fbButton = (Button) bottomSheet.findViewById(R.id.button_info_fb);
        Button faqButton = (Button) bottomSheet.findViewById(R.id.button_info_faq);

        aboutUsButton.setOnClickListener(this);
        tncButton.setOnClickListener(this);
        fbButton.setOnClickListener(this);
        faqButton.setOnClickListener(this);

        setTitle(R.string.title_connect_with_us);

        eName = (EditText) findViewById(R.id.eEnquiryName);
        eName.setOnFocusChangeListener(this);

        eEmail = (EditText) findViewById(R.id.eEnquiryEmail);
        eEmail.setOnFocusChangeListener(this);

        eMobileNumber = (EditText) findViewById(R.id.eEnquiryMobileNumber);
        eMobileNumber.setOnFocusChangeListener(this);

        eAddress1 = (EditText) findViewById(R.id.eEnquiryAddress1);
        eAddress1.setOnFocusChangeListener(this);

        eAddress2 = (EditText) findViewById(R.id.eEnquiryAddress2);
        eAddress2.setOnFocusChangeListener(this);

        eMessage = (EditText) findViewById(R.id.eEnquiryMessage);
        eMessage.setScroller(new Scroller(TalkToUsActivity.this));
        eMessage.setVerticalScrollBarEnabled(true);
        eMessage.setMovementMethod(new ScrollingMovementMethod());
        eMessage.setOnFocusChangeListener(this);

        Button bSave = (Button) findViewById(R.id.bEnquirySave);
        bSave.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        backPress = false;
        behavior.setState(4);
        switch (v.getId())
        {
            case R.id.bEnquirySave:
                save();
                break;

            case R.id.button_info_fb:
                String url = "https://www.facebook.com/RuralICT.iitb/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.button_info_about_us:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "about");
                startActivity(i);
                break;
            case R.id.button_info_tnc:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "tnc");
                startActivity(i);
                break;
            case R.id.button_info_faq:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "faq");
                startActivity(i);
                break;



        }
    }

    private void save()
    {
        if(eName.getText().toString().trim().equals("")
                ||eEmail.getText().toString().trim().equals("")
                ||eMessage.getText().toString().trim().equals("")
                ||eAddress1.getText().toString().trim().equals("")
                ||eMobileNumber.getText().toString().trim().equals(""))
        {
            if(eName.getText().toString().trim().equals(""))
            {
                eName.setError(getString(R.string.error_required));
            }
            if(eEmail.getText().toString().trim().equals(""))
            {
                eEmail.setError(getString(R.string.error_required));
            }
            if(eMobileNumber.getText().toString().trim().equals(""))
            {
                eMobileNumber.setError(getString(R.string.error_required));
            }
            if(eAddress1.getText().toString().trim().equals(""))
            {
                eAddress1.setError(getString(R.string.error_required));
            }
            if(eMessage.getText().toString().trim().equals(""))
            {
                eMessage.setError(getString(R.string.error_required));
            }
        }
        else
        {
            if(eMobileNumber.getText().toString().trim().length()!=10)
            {
                Toast.makeText(getApplicationContext(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
            }
            else if(eMobileNumber.getText().toString().trim().charAt(0) == '0')
            {
                Toast.makeText(getApplicationContext(), R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
            }
            else if(Validation.isValidEmail(eEmail.getText().toString().trim()))
            {
                Toast.makeText(getApplicationContext(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();
            }
            else
            {
                if (Master.isNetworkAvailable(TalkToUsActivity.this))
                {

                    new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                            getString(R.string.pd_chk_internet_connection),this,"TalkToUs_Internet" ).execute();


                }
                else
                {
                    Toast.makeText(TalkToUsActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }

            }


        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus)
            backPress = false;
    }

    @Override
    public void getData(String objects, String tag) {

        switch(tag)
        {
            case "TalkToUs_Internet":
                if(objects.equals("success"))
                {
                    JSONObject enquiryObject = new JSONObject();
                    try
                    {
                        enquiryObject.put(Master.EMAIL, eEmail.getText().toString().trim());
                        enquiryObject.put("name", eName.getText().toString().trim());
                        enquiryObject.put("message",  eMessage.getText().toString().replaceAll("\\n", " "));
                        enquiryObject.put(Master.MOBILENUMBER, "91"+eMobileNumber.getText().toString().trim());
                        enquiryObject.put("address1", eAddress1.getText().toString().replaceAll("\\n", " "));
                        if(!eAddress2.getText().toString().trim().equals(""))
                            enquiryObject.put("address2", eAddress2.getText().toString().replaceAll("\\n", " "));

                        new NetworkAsyncTask
                                (
                                        Master.getEnquiryFormURL(),
                                        "sendDetails",
                                        Master.DIALOG_TRUE,
                                        getString(R.string.pd_sending_data_to_server),
                                        TalkToUsActivity.this,
                                        enquiryObject,
                                        Master.POST,
                                        Master.AUTH_TRUE,
                                        MemberDetails.getEmail(),
                                        MemberDetails.getPassword()
                                ).execute();
                    }
                    catch (JSONException e)
                    {
                    }
                }
                else
                {
                    Toast.makeText(TalkToUsActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }


                 break;

            case "sendDetails":
                if(objects.equals("exception"))
                {
                    Material.alertDialog(TalkToUsActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), "OK");

                }
                else
                {
                    try
                    {
                        JSONObject responseObject = new JSONObject(objects);
                        if (responseObject.get("response").equals("Enquiry sent"))
                        {


                            final AlertDialog.Builder builder = new AlertDialog.Builder(TalkToUsActivity.this);
                            builder.setCancelable(false);
                            builder.setMessage(getString(R.string.toast_the_enquiry_has_been_submitted));
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                    System.exit(0);
                                }
                            });
                            builder.show();
                        }
                        else if(responseObject.get("response").equals("Failed to send enquiry"))
                        {
                            Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_unable_to_record_your_enquiry), Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_LONG).show();

                        e.printStackTrace();
                    }
                }

                  break;

            default:break;

        }


    }


}
