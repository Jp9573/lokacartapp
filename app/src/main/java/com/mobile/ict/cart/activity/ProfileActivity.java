package com.mobile.ict.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.TourCheck.DemoLists;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.interfaces.GetResponse;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.util.NetworkAsyncTask;
import com.mobile.ict.cart.util.Validation;

import org.json.JSONException;
import org.json.JSONObject;


public class ProfileActivity extends Activity implements View.OnClickListener, GetResponse {

    private DBHelper dbHelper;
    private TextInputEditText eFName;
    private TextInputEditText eLName;
    private TextInputEditText eAddress;
    private TextInputEditText ePincode;
    private TextInputEditText eEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setTitle(R.string.title_fragment_profile);

        dbHelper = DBHelper.getInstance(this.getApplicationContext());
        dbHelper.getProfile();

        eEmail = (TextInputEditText) findViewById(R.id.eEditEmail);
        eFName = (TextInputEditText) findViewById(R.id.eEditProfileFName);
        eLName = (TextInputEditText) findViewById(R.id.eEditProfileLName);
        ePincode = (TextInputEditText) findViewById(R.id.eEditProfilePincode);

        eAddress = (TextInputEditText) findViewById(R.id.eEditProfileAddress);

        eAddress.setMovementMethod(new ScrollingMovementMethod());

        ImageView ivDone = (ImageView) findViewById(R.id.ivDone);
        ivDone.setOnClickListener(this);

        init();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();
    }

    @Override
    public void onClick(View v) {
        saveData();
    }

    private void init() {
        if (MemberDetails.getFname().equals("null"))
            eFName.setText("");
        else
            eFName.setText(MemberDetails.getFname());


        if (MemberDetails.getLname().equals("null"))
            eLName.setText("");
        else
            eLName.setText(MemberDetails.getLname());


        if (MemberDetails.getAddress().equals("null"))
            eAddress.setText("");
        else
            eAddress.setText(MemberDetails.getAddress());


        if (MemberDetails.getPincode().equals("null"))
            ePincode.setText("");
        else
            ePincode.setText(MemberDetails.getPincode());

        if (MemberDetails.getEmail().equals("null") || MemberDetails.getEmail().equals("91" + MemberDetails.getMobileNumber() + "@gmail.com"))
            eEmail.setText("");

        else
            eEmail.setText(MemberDetails.getEmail());


    }

    private void saveData() {
        String localFName = eFName.getText().toString().trim();
        String localLName = eLName.getText().toString().trim();
        String localAddress = eAddress.getText().toString().trim();
        String localPincode = ePincode.getText().toString().trim();
        String localEmail = eEmail.getText().toString().trim();

        if (localFName.equals("") || localLName.equals("") || localAddress.equals("") || localPincode.equals("")/*|| localEmail.equals("")*/) {
            if (localFName.equals(""))
                eFName.setError(getString(R.string.error_required));
            if (localLName.equals(""))
                eLName.setError(getString(R.string.error_required));
            if (localAddress.equals(""))
                eAddress.setError(getString(R.string.error_required));
            if (localPincode.equals(""))
                ePincode.setError(getString(R.string.error_required));

        } else if (localPincode.length() != 6)
            Toast.makeText(this, R.string.toast_please_enter_a_valid_six_digit_pincode, Toast.LENGTH_LONG).show();
        else if (localEmail.length() != 0 && Validation.isValidEmail(localEmail)) {
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_LONG).show();
        } else {
            if (Master.isNetworkAvailable(ProfileActivity.this)) {

                new NetworkAsyncTask(Master.checkInternetURL, Master.DIALOG_TRUE,
                        getString(R.string.pd_chk_internet_connection), ProfileActivity.this, "Profile_Internet").execute();


            } else {
                Toast.makeText(ProfileActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

            }

        }
    }

    @Override
    public void getData(String objects, String tag) {

        switch (tag) {
            case "Profile_Internet":

                if (objects.equals("success")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(Master.FNAME, eFName.getText().toString().trim());
                        jsonObject.put(Master.LNAME, eLName.getText().toString().trim());
                        jsonObject.put(Master.ADDRESS, eAddress.getText().toString().trim());
                        jsonObject.put(Master.PINCODE, ePincode.getText().toString().trim());
                        jsonObject.put(Master.MOBILENUMBER, "91" + MemberDetails.getMobileNumber());
                        if (!MemberDetails.getEmail().equals("null")) {
                            jsonObject.put("oldemail", MemberDetails.getEmail());
                        } else {
                            jsonObject.put("oldemail", "91" + MemberDetails.getMobileNumber() + "@gmail.com");
                        }

                        if (eEmail.getText().toString().trim().length() != 0) {
                            jsonObject.put(Master.EMAIL, eEmail.getText().toString().trim());

                        } else {
                            jsonObject.put(Master.EMAIL, "91" + MemberDetails.getMobileNumber() + "@gmail.com");

                        }


                        new NetworkAsyncTask(
                                Master.getEditProfileURL(),
                                "profile",
                                Master.DIALOG_TRUE,
                                getString(R.string.pd_sending_data_to_server),
                                this,
                                jsonObject,
                                Master.POST,
                                Master.AUTH_TRUE,
                                "ruralict.iitb@gmail.com",
                                "password"
                        ).execute();
                    } catch (JSONException e) {
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, R.string.toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();

                }


                break;

            case "profile":
                if (objects.equals("exception")) {
                    Material.alertDialog(ProfileActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), "OK");
                } else {
                    try {
                        JSONObject responseObject = new JSONObject(objects);

                        if (responseObject.getString(Master.RESPONSE).equals("Success")) {
                            Toast.makeText(ProfileActivity.this, R.string.toast_profile_details_updated_successfully, Toast.LENGTH_LONG).show();

                            MemberDetails.setFname(eFName.getText().toString().trim());
                            MemberDetails.setLname(eLName.getText().toString().trim());
                            MemberDetails.setAddress(eAddress.getText().toString().trim());
                            MemberDetails.setPincode(ePincode.getText().toString().trim());

                            if (eEmail.getText().toString().trim().length() != 0)
                                MemberDetails.setEmail(eEmail.getText().toString().trim());

                            else

                                MemberDetails.setEmail("91" + MemberDetails.getMobileNumber() + "@gmail.com");


                            dbHelper.updateProfile(MemberDetails.getMobileNumber());

                            Intent i = new Intent(getApplicationContext(), DemoLists.class);
                            i.putExtra("redirectto", "organisation");
                            i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(i);
                            finish();

                        } else if (responseObject.getString(Master.RESPONSE).equals("The Mail ID is already associated with another account already")) {
                            Toast.makeText(ProfileActivity.this, R.string.toast_email_id_already_exists, Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(ProfileActivity.this, R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ProfileActivity.this, R.string.toast_we_are_facing_some_technical_problems, Toast.LENGTH_LONG).show();

                    }

                }
                break;

            default:
                break;

        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }
}
